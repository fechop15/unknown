<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//rutas

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('blank', 'DashboardController@blank')->name('blank');
    Route::get('historico', 'DashboardController@historico')->name('historico');
    Route::get('perfil/{id}', 'DashboardController@perfil')->name('perfilCliente');
    Route::get('perfil', 'DashboardController@perfil')->name('perfil');
    Route::get('clientes', 'DashboardController@clientes')->name('clientes');


});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'admin']], function () {

    Route::get('empleados/{id}', 'DashboardController@usuarios')->name('empleado');
    Route::get('empleados', 'DashboardController@usuarios')->name('empleados');
    Route::get('servicios', 'DashboardController@historico')->name('servicios');

    Route::get('gastos/fijos', 'DashboardController@gastosFijos')->name('fijos');
    Route::get('gastos/insumos', 'DashboardController@gastosInsumos')->name('insumos');
    Route::get('gastos/otros', 'DashboardController@gastosOtros')->name('otros');

    Route::get('pagos/diarios', 'DashboardController@pagosDoctores')->name('diarios');
    Route::get('pagos/nomina', 'DashboardController@pagosNomina')->name('nomina');
    Route::get('pagos/credito', 'DashboardController@credito')->name('credito');
    Route::get('consignaciones', 'DashboardController@consignaciones')->name('consignaciones');
    Route::get('empresas/{id}', 'DashboardController@empresas');
    Route::get('empresas', 'DashboardController@empresas')->name('empresas');
    Route::get('empresa/{id}', 'DashboardController@empresa');
    Route::get('empresa', 'DashboardController@empresa')->name('empresa');

});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'users']], function () {

    Route::get('producido', 'DashboardController@serviciosEmpleado')->name('producido');

});

Route::get('/', 'Auth\LoginController@showLoginForm')->middleware('guest');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//Fin rutas

//metodos
Route::post('login', 'Auth\LoginController@login');
Route::get('get-usuarios', 'UsuarioController@getUsuarios');
Route::post('get-usuarios-by-rol', 'UsuarioController@getUsuariosByRol');
Route::post('get-usuarios-by-rol-pago', 'UsuarioController@getUsuariosByPago');
Route::post('crear-usuarios', 'UsuarioController@crearUsuario');
Route::post('actualizar-usuarios', 'UsuarioController@actualizarUsuario');
Route::post('buscar-usuarios', 'UsuarioController@buscarUsuario');

Route::get('get-clientes', 'ClientesController@getClientes');
Route::post('crear-cliente', 'ClientesController@crearCliente');
Route::post('actualizar-cliente', 'ClientesController@actualizarCliente');
Route::post('buscar-cliente', 'ClientesController@buscarCliente');

Route::get('get-servicios', 'ServiciosController@getServicios');
Route::post('crear-servicio', 'ServiciosController@crearServicio');
Route::post('actualizar-servicio', 'ServiciosController@actualizarServicio');
Route::post('buscar-servicio', 'ServiciosController@buscarServicio');

Route::get('get-produccion', 'FacturasController@getFacturasDiaUsuario');
Route::get('get-produccion-fecha', 'FacturasController@getFacturasFechaUsuario');
Route::get('get-produccion-mes', 'FacturasController@getFacturasMesUsuario');
Route::post('crear-produccion', 'FacturasController@crearFactura');
Route::post('editar-produccion', 'FacturasController@editarFactura');
Route::post('buscar-produccion', 'FacturasController@getFactura');
Route::post('eliminarProduccion', 'FacturasController@eliminarFactura');

Route::post('get-notas', 'NotasController@getNotas');
Route::post('crear-nota', 'NotasController@crearNota');
Route::post('actualizar-nota', 'NotasController@actualizarNota');
Route::post('buscar-nota', 'NotasController@buscarNota');

Route::post('get-gasto-mes', 'FinanzasController@getGastoMes');
Route::post('get-gasto-dia', 'FinanzasController@getGastoDia');
Route::post('crear-gasto', 'FinanzasController@guardarGasto');

Route::post('crear-consignacion', 'FinanzasController@guardarConsignacion');

Route::get('produccion-mes', 'FinanzasController@getTotalProducidoMes');
Route::get('produccion-dia', 'FinanzasController@getTotalProducidoDia');
Route::get('produccion-tarjeta', 'FinanzasController@getTotalProducidoTarjeta');

Route::post('get-total-pago-mes', 'FinanzasController@getTotalPagoMes');
Route::post('get-nomina', 'FinanzasController@getNomina');
Route::post('crear-pago-nomina', 'FinanzasController@guardarPagoNomina');

Route::post('get-total-pago-dia', 'FinanzasController@getTotalPagoDia');

// fin metodos

/* Actualizacion */
Route::post('cambiar-pass', 'UsuarioController@actualizarPass');
Route::post('cambiar-avatar', 'UsuarioController@update_profile');
Route::post('quitar-avatar', 'UsuarioController@remove_profile');

Route::post('get-credito-empleados', 'CreditosController@getCreditosEmpleados');
Route::post('get-estado-credito-empleado', 'CreditosController@getEstadoCreditoEmpleado');

Route::post('credito-empleado', 'CreditosController@crearCreditoEmpleado');
Route::post('abono', 'CreditosController@crearAbonoEmpleado');

Route::post('get-pagos-empleado', 'FinanzasController@getPagosEmpleado');

Route::post('get-estado-credito-cliente', 'CreditosController@getEstadoCreditoCliente');
Route::post('credito-cliente', 'CreditosController@crearCreditoCliente');
Route::get('total-creditos-clientes', 'CreditosController@getEstadoCreditoClientesFecha');

Route::post('get-empresas', 'EmpresasController@getEmpresas');
Route::post('crear-empresa', 'EmpresasController@createEmpresa');
Route::post('cambir-empresa-logo', 'EmpresasController@updateEmpresaLogo');
Route::post('cambir-empresa-color', 'EmpresasController@updateEmpresaColor');



