<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Egreso extends Model
{
    //
    protected $fillable = [
        'tipo', 'concepto', 'valor', 'fecha', 'empleado_id', 'user_id','empresa_id'
    ];

    public function empleado()
    {
        return $this->belongsTo(User::class, 'empleado_id');
    }
}
