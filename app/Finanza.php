<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finanza extends Model
{
    //
    protected $fillable = [
        'razon', 'descripcion', 'valor', 'tipo', 'user_id','empresa_id'
    ];

}
