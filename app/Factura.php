<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    //
    protected $fillable = [
        'precio', 'servicio', 'metodo_pago', 'empleado_id', 'user_id', 'cliente_id','empresa_id'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function empleado()
    {
        return $this->belongsTo(User::class, 'empleado_id');
    }

    public function servicio_facturas()
    {
        return $this->hasMany(Servicio_factura::class);
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

}
