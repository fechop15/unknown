<?php

namespace App\Http\Controllers;

use App\Empresas;
use App\Pagos;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresasController extends Controller
{
    //

    function getEmpresas()
    {
        $empresas = Empresas::all();
        $response = array(
            'status' => 'success',
            'msg' => $empresas,
        );
        return response()->json($response);
    }

    function createEmpresa(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|string',
        ]);
        $empresa = Empresas::create([
            'nombre' => $request->nombre,
        ]);
        $response = array(
            'status' => 'success',
            'msg' => $empresa,
        );
        return response()->json($response);

    }

    function updateEmpresaColor(Request $request)
    {
        $empresa = Empresas::find(auth()->user()->empresa_id);
        $empresa->color = $request->color;
        $empresa->save();
        $response = array(
            'status' => 'success',
            'msg' => "Color Actualizado con exito",
            'color' => $request->color,
        );
        return response()->json($response);
    }

    function updateEmpresaLogo(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $path = "images/empresas/";
        $empresa = Empresas::find(auth()->user()->empresa_id);

        $filename = $empresa->nombre . '_' . time() . '.' . $request->avatar->getClientOriginalExtension();
        move_uploaded_file($request->file('logo'), $path . $filename);

        $empresa->logo = $filename;
        $empresa->save();

        $response = array(
            'status' => 'success',
            'imagen' => $filename,
            'msg' => "El Logo Se Cargo Correctamente",
        );
        return response()->json($response);
    }

    function updateEmpresa(Request $request)
    {

    }

    function findEmpresa(Request $request)
    {

    }

    function selectEmpresa(Request $request)
    {
        if (auth()->user()->id == 1) {
            $usuario = User::find(1);
            $usuario->empresa_id = $request->id;
            $usuario->save();
            $response = array(
                'status' => 'success',
            );
        } else {
            $response = array(
                'status' => 'error',
                'msg' => "No tienes privilegios para esta accion",
            );
        }
        return response()->json($response);
    }

    function pagos()
    {
        $empresa = Empresas::find(auth()->user()->empresa_id);
        $response = array(
            'status' => 'success',
            'msg' => $empresa->pagos,
        );
        return response()->json($response);
    }

    function createPago(Request $request)
    {
        $this->validate($request, [
            'valor' => 'required',
            'fecha' => 'required',
        ]);
        $pago = Pagos::create([
            'empresa_id' => auth()->user()->empresa_id,
            'valor' => $request->valor,
            'fecha' => $request->fecha,
        ]);
        $response = array(
            'status' => 'success',
            'msg' => $pago,
        );
        return response()->json($response);

    }
}
