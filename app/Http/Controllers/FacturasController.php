<?php

namespace App\Http\Controllers;

use App\Credito;
use App\Factura;
use App\Finanza;
use App\Servicio;
use App\Servicio_factura;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class FacturasController extends Controller
{
    //

    function getFacturasDiaUsuario()
    {
        //$facturas = Factura::all();
        if (auth()->user()->rol_id == 1) {
            $facturas = Factura::whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))
                ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
        } else {
            $facturas = Factura::where("empleado_id", auth()->user()->id)
                ->whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))
                ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
        }

        $array = array();

        //obtener el nombre de los servicios
        foreach ($facturas as $factura) {
            $factura->servicio_facturas->toArray();
            $servicio = "";
            foreach ($factura->servicio_facturas as $producto) {
                $servicio == "" ?: $servicio .= ', ';
                $servicio .= $producto->servicio->nombre;
            }

            $datos = [
                'id' => $factura->id,
                'nombre' => $factura->cliente->nombre,
                'servicio' => $factura->servicio,
                'atendido' => $factura->empleado->nombre,
                'precio' => $factura->precio,
                'metodoP' => $factura->metodo_pago,
                'fecha' => date("d/m/Y h:i A", strtotime($factura->created_at))
            ];

            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'fecha' => Carbon::now()->format('Y-m-d'),
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getFacturasFechaUsuario(Request $request)
    {
        //$facturas = Factura::all();
        if (auth()->user()->rol_id == 1 && $request->id == null) {
            $facturas = Factura::whereDate('created_at', 'like', $request->fecha . '%')
                ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
        } else {
            if ($request->id != null) {
                $facturas = Factura::where("empleado_id", "=", $request->id)
                    ->whereDate('created_at', 'like', $request->fecha . '%')
                    ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
            } else {
                $facturas = Factura::where("empleado_id", "=", auth()->user()->id)
                    ->whereDate('created_at', 'like', $request->fecha . '%')
                    ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
            }

        }

        $array = array();

        //obtener el nombre de los servicios
        foreach ($facturas as $factura) {
            $factura->servicio_facturas->toArray();
            $servicio = "";
            foreach ($factura->servicio_facturas as $producto) {
                $servicio == "" ?: $servicio .= ', ';
                $servicio .= $producto->servicio->nombre;
            }

            $datos = [
                'id' => $factura->id,
                'nombre' => $factura->cliente->nombre,
                'servicio' => $factura->servicio,
                'atendido' => $factura->empleado->nombre,
                'precio' => $factura->precio,
                'metodoP' => $factura->metodo_pago,
                'fecha' => date("d/m/Y h:i A", strtotime($factura->created_at))
            ];

            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'fecha' => Carbon::now()->format('Y-m-d'),
            'msg' => $array,
            'id' => $request->id,
        );

        return response()->json($response);
    }

    function getFacturasMesUsuario(Request $request)
    {
        //$facturas = Factura::all();
        if (auth()->user()->rol_id == 1 && $request->id == null) {
            $facturas = Factura::where('created_at', 'like', $request->fecha . '%')
                ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
        } else {
            if ($request->id != null) {
                $facturas = Factura::where("empleado_id", '=', $request->id)
                    ->whereDate('created_at', 'like', $request->fecha . '%')
                    ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
            } else {
                $facturas = Factura::where("empleado_id", "=", auth()->user()->id)
                    ->whereDate('created_at', 'like', $request->fecha . '%')
                    ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
            }
        }

        $array = array();

        //obtener el nombre de los servicios
        foreach ($facturas as $factura) {
            $factura->servicio_facturas->toArray();
            $servicio = "";
            foreach ($factura->servicio_facturas as $producto) {
                $servicio == "" ?: $servicio .= ', ';
                $servicio .= $producto->servicio->nombre;
            }

            $datos = [
                'id' => $factura->id,
                'nombre' => $factura->cliente->nombre,
                'servicio' => $factura->servicio,
                'atendido' => $factura->empleado->nombre,
                'precio' => $factura->precio,
                'metodoP' => $factura->metodo_pago,
                'fecha' => date("d/m/Y h:i A", strtotime($factura->created_at))
            ];

            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'fecha' => Carbon::now()->format('Y-m'),
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearFactura(Request $request)
    {

        $this->validate($request, [
            'cliente_id' => 'required|numeric',
            'servicio' => 'required|string',
            'empleado_id' => 'required|numeric',
            'precio' => 'required|numeric',
            'metodo_pago' => 'required|numeric',
        ]);

//        $total = 0;
//        foreach ($request->servicios as $servicio_id) {
//            $servicio = Servicio::findOrFail($servicio_id);
//            $total += $servicio->precio;
//        }
        if ($request->metodo_pago == 1) {
            $metodoPago = "Efectivo";

        } elseif ($request->metodo_pago == 2) {
            $metodoPago = "Tarjeta";

        } else {
            $metodoPago = "Mi Saldo";
            $creditos = Credito::where('cliente_id', '=', $request->cliente_id)->where('empresa_id', auth()->user()->empresa_id)->get();
            $recarga = 0;
            $pago = 0;

            foreach ($creditos as $credito) {
                if ($credito->tipo == 'Recarga') {
                    $recarga += $credito->valor;
                } elseif ($credito->tipo == 'Pago') {
                    $pago += $credito->valor;
                }
            }
            $total = $recarga - $pago;
            if ($request->precio > $total) {
                $response = array(
                    'status' => 'error',
                    'msg' => "No tiene fondos sufucientes para pagar este servicio",
                );

                return response()->json($response);
            }

        }

        $factura = Factura::create([
            'precio' => $request->precio,
            'servicio' => $request->servicio,
            'metodo_pago' => $metodoPago,
            'empleado_id' => $request->empleado_id,
            'cliente_id' => $request->cliente_id,
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        if ($metodoPago == "Mi Saldo") {
            //descontar de credito al cliente
            $credito = Credito::create([
                'tipo' => "Pago",
                'descripcion' => "Pago Servicio: " . $request->servicio,
                'valor' => $request->precio,
                'cliente_id' => $request->cliente_id,
                'user_id' => auth()->user()->id,
                'empresa_id' => auth()->user()->empresa_id,
            ]);
        }

//        foreach ($request->servicios as $servicio_id) {
//            $servicio_factura = Servicio_factura::create([
//                    'servicio_id' => $servicio_id,
//                    'factura_id' => $factura->id
//                ]
//            );
//        }
//
//        $factura->servicio_facturas->toArray();
//        $servicio = "";
//        foreach ($factura->servicio_facturas as $producto) {
//            $servicio == "" ?: $servicio .= ', ';
//            $servicio .= $producto->servicio->nombre;
//        }

        $datos = [
            'id' => $factura->id,
            'nombre' => $factura->cliente->nombre,
            'servicio' => $factura->servicio,
            'atendido' => $factura->empleado->nombre,
            'precio' => $factura->precio,
            'metodoP' => $factura->metodo_pago,
            'fecha' => date("d/m/Y h:i A", strtotime($factura->created_at))
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);
    }

    function editarFactura(Request $request)
    {

        $this->validate($request, [
            'id' => 'required|numeric',
            'cliente_id' => 'required|numeric',
            'servicio' => 'required|string',
            'empleado_id' => 'required|numeric',
            'precio' => 'required|numeric',
            'metodo_pago' => 'required|numeric',
        ]);

        $factura = Factura::find($request->id);
        if ($request->metodo_pago == 1) {
            $metodoPago = "Efectivo";

        } elseif ($request->metodo_pago == 2) {
            $metodoPago = "Tarjeta";

        } else {
            $metodoPago = "Mi Saldo";
            $creditos = Credito::where('cliente_id', '=', $request->cliente_id)->where('empresa_id', auth()->user()->empresa_id)->get();
            $recarga = 0;
            $pago = 0;

            foreach ($creditos as $credito) {
                if ($credito->tipo == 'Recarga') {
                    $recarga += $credito->valor;
                } elseif ($credito->tipo == 'Pago') {
                    $pago += $credito->valor;
                }
            }
            $total = $recarga - $pago;
            if ($request->precio > $total) {
                $response = array(
                    'status' => 'error',
                    'msg' => "No tiene fondos sufucientes para pagar este servicio",
                );

                return response()->json($response);
            }

        }


        if ($metodoPago == "Mi Saldo" and $factura->metodo_pago != "Mi Saldo") {
            //descontar de credito al cliente
            $credito = Credito::create([
                'tipo' => "Pago",
                'descripcion' => "Pago Servicio: " . $request->servicio,
                'valor' => $request->precio,
                'cliente_id' => $request->cliente_id,
                'user_id' => auth()->user()->id,
            ]);
        }

        $factura->update([
            'precio' => $request->precio,
            'servicio' => $request->servicio,
            'metodo_pago' => $metodoPago,
            'empleado_id' => $request->empleado_id,
            'cliente_id' => $request->cliente_id,
            'user_id' => auth()->user()->id
        ]);

        $response = array(
            'status' => 'success',
            'msg' => 'Registro Actualizado con Exito',
        );

        return response()->json($response);
    }

    function getFactura(Request $request)
    {
        $factura = Factura::find($request->id);
        $response = array(
            'status' => 'success',
            'msg' => $factura,
        );

        return response()->json($response);
    }

    function eliminarFactura(Request $request)
    {
        $factura = Factura::find($request->id);
        $factura->delete();
        $response = array(
            'status' => 'success',
            'msg' => 'Registro Eliminado con Exito',
        );

        return response()->json($response);
    }

}
