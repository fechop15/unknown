<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Credito;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    //
    function getClientes()
    {
        $clientes = Cliente::where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($clientes as $cliente) {

            $recarga = 0;
            $pago = 0;

            foreach ($cliente->credito as $credito) {
                if ($credito->tipo == 'Recarga') {
                    $recarga += $credito->valor;
                } elseif ($credito->tipo == 'Pago') {
                    $pago += $credito->valor;
                }
            }

            $datos = [
                'id' => $cliente->id,
                'nombre' => $cliente->nombre,
                'cedula' => $cliente->cedula,
                'telefono' => $cliente->telefono,
                'direccion' => $cliente->direccion,
                'ocupacion' => $cliente->ocupacion,
                'ciudad' => $cliente->ciudad,
                'estado_civil' => $cliente->estado_civil,
                'edad' => $cliente->edad,
                'credito' => $recarga - $pago,
            ];

            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearCliente(Request $request)
    {

        $this->validate($request, [
            'cedula' => 'required|numeric|unique:clientes,cedula',
            'ciudad' => 'required|string',
            'direccion' => 'required|string',
            'ocupacion' => 'required|string',
            'nombre' => 'required|string',
            'estado_civil' => 'required|string',
            'telefono' => 'required|numeric',
            'edad' => 'required|numeric',
        ]);


        $cliente = Cliente::create([
            'nombre' => $request->nombre,
            'cedula' => $request->cedula,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
            'ocupacion' => $request->ocupacion,
            'ciudad' => $request->ciudad,
            'estado_civil' => $request->estado_civil,
            'edad' => $request->edad,
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $cliente,
        );

        return response()->json($response);

    }

    function actualizarCliente(Request $request)
    {

        $cliente = Cliente::findOrFail($request->id);

        $this->validate($request, [
            'cedula' => 'required|numeric|unique:clientes,cedula,' . $request->id,
            'ciudad' => 'required|string',
            'direccion' => 'required|string',
            'ocupacion' => 'required|string',
            'nombre' => 'required|string',
            'estado_civil' => 'required|string',
            'telefono' => 'required|numeric',
            'edad' => 'required|numeric',
        ]);

        $cliente->update($request->all());

        $datos = [
            'id' => $cliente->id,
            'nombre' => $cliente->nombre,
            'cedula' => $cliente->cedula,
            'telefono' => $cliente->telefono,
            'direccion' => $cliente->direccion,
            'ocupacion' => $cliente->ocupacion,
            'ciudad' => $cliente->ciudad,
            'estado_civil' => $cliente->estado_civil,
            'edad' => $cliente->edad,
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);

    }

    function buscarCliente(Request $request)
    {
        $cliente = Cliente::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $cliente,
        );

        return response()->json($response);
    }

}
