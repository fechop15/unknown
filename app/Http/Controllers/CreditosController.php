<?php

namespace App\Http\Controllers;

use App\Credito;
use Illuminate\Http\Request;

class CreditosController extends Controller
{
    //
    function getCreditosEmpleados(Request $request)
    {
        $creditos = Credito::where('empleado_id', '!=', null)
            ->where('created_at', 'like', $request->fecha . '%')->where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            $datos = [
                'id' => $credito->id,
                'tipo' => $credito->tipo,
                'descripcion' => $credito->descripcion,
                'valor' => $credito->valor,
                'empleado' => $credito->empleado->nombre,
                'fecha' => date("d/m/Y h:i A", strtotime($credito->created_at)),
            ];

            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'fecha' => $request->fecha,
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearCreditoEmpleado(Request $request)
    {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
        $this->validate($request, [
            'descripcion' => 'required|string',
            'valor' => 'required|numeric|min:1',
            'empleado' => 'required|numeric',
            'tipo' => 'required|string',
        ]);

        $credito = Credito::create([
            'tipo' => $request->tipo,
            'descripcion' => $request->descripcion,
            'valor' => $request->valor,
            'empleado_id' => $request->empleado,
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $credito,
        );

        return response()->json($response);

    }

    function crearCreditoCliente(Request $request)
    {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
        $this->validate($request, [
            'valor' => 'required|numeric|min:1',
            'cliente' => 'required|numeric',
            'tipo' => 'required|string',
        ]);

        $credito = Credito::create([
            'tipo' => $request->tipo,
            'descripcion' => "Recarga Directa al Cliente de " . $request->valor,
            'valor' => $request->valor,
            'cliente_id' => $request->cliente,
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $credito,
        );

        return response()->json($response);

    }

    function getEstadoCreditoEmpleado(Request $request)
    {
        $creditos = Credito::where('empleado_id', '=', $request->empleado)->where('empresa_id', auth()->user()->empresa_id)->get();

        $array = array();
        $avance = 0;
        $abono = 0;
        $descuento = 0;

        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            if ($credito->tipo == 'Avance') {
                $avance += $credito->valor;
            } else if ($credito->tipo == 'Descuento') {
                $descuento += $credito->valor;
            } else {
                $abono += $credito->valor;
            }

            $datos = [
                'id' => $credito->id,
                'tipo' => $credito->tipo,
                'descripcion' => $credito->descripcion,
                'valor' => $credito->valor,
                'empleado' => $credito->empleado->nombre,
                'fecha' => date("d/m/Y h:i A", strtotime($credito->created_at)),
            ];
            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'avance' => $avance,
            'abono' => $abono,
            'descuento' => $descuento,
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getEstadoCreditoCliente(Request $request)
    {
        $creditos = Credito::where('cliente_id', '=', $request->cliente)->
        where('created_at', 'like', $request->fecha . '%')->where('empresa_id', auth()->user()->empresa_id)->get();


        $array = array();
        $recarga = 0;
        $pago = 0;

        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            if ($credito->tipo == 'Recarga') {
                $recarga += $credito->valor;
            } elseif ($credito->tipo == 'Pago') {
                $pago += $credito->valor;
            }

            $datos = [
                'id' => $credito->id,
                'tipo' => $credito->tipo,
                'descripcion' => $credito->descripcion,
                'valor' => $credito->valor,
                'fecha' => date("d/m/Y h:i A", strtotime($credito->created_at)),
            ];
            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'recarga' => $recarga,
            'pago' => $pago,
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getEstadoCreditoClientesFecha(Request $request)
    {

        $recarga = 0;
        $recargaGlobal = 0;
        $pago = 0;
        $pagoGlobal = 0;

        $creditos = Credito::where('cliente_id', '!=', null)->
        where('created_at', 'like', $request->fecha . '%')->where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            if ($credito->tipo == 'Recarga') {
                $recarga += $credito->valor;
            } elseif ($credito->tipo == 'Pago') {
                $pago += $credito->valor;
            }

        }
        $creditos = Credito::where('cliente_id', '!=', null)->where('empresa_id', auth()->user()->empresa_id)->get();
        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            if ($credito->tipo == 'Recarga') {
                $recargaGlobal += $credito->valor;
            } elseif ($credito->tipo == 'Pago') {
                $pagoGlobal += $credito->valor;
            }

        }

        $response = array(
            'status' => 'success',
            'recarga' => $recarga,
            'pago' => $pago,
            'msg' => $recargaGlobal - $pagoGlobal,
        );

        return response()->json($response);

    }
}
