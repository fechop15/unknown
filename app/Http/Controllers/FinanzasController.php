<?php

namespace App\Http\Controllers;

use App\Credito;
use App\Factura;
use App\Finanza;
use App\Nomina;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class FinanzasController extends Controller
{
    //
    function getGastoDia(Request $request)
    {
        //$facturas = Factura::all();
        $finanzas = Finanza::whereDate('created_at', '=', $request->fecha)
            ->where('razon', '=', $request->razon)
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($finanzas as $finanza) {
            array_push($array, $this->datoGasto($finanza));
        }
        $response = array(
            'status' => 'success',
            'fecha' => $request->fecha,
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getGastoMes(Request $request)
    {
        $finanzas = Finanza::where('created_at', 'like', $request->fecha . '%')
            ->where('razon', '=', $request->razon)
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($finanzas as $finanza) {

            array_push($array, $this->datoGasto($finanza));
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function guardarGasto(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|string',
            'razon' => 'required|string',
            'valor' => 'required|numeric',
        ]);

        $gasto = Finanza::create([

            'razon' => $request->razon,
            'descripcion' => $request->descripcion,
            'valor' => $request->valor,
            'tipo' => "Salida",
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $this->datoGasto($gasto),
        );

        return response()->json($response);
    }

    function guardarConsignacion(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|string',
            'razon' => 'required|string',
            'valor' => 'required|numeric',
        ]);

        $gasto = Finanza::create([

            'razon' => $request->razon,
            'descripcion' => $request->descripcion,
            'valor' => $request->valor,
            'tipo' => $request->tipo,
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,

        ]);

        $response = array(
            'status' => 'success',
            'msg' => $this->datoGasto($gasto),
        );

        return response()->json($response);
    }

    function getTotalProducidoMes()
    {
        $facturas = Factura::where('empresa_id', auth()->user()->empresa_id)->get();

        $totalEfectivo = 0;
        $totalTarjeta = 0;
        $totalMiSaldo = 0;
        $totalGastos = 0;
        //obtener el nombre de los servicios

        foreach ($facturas as $factura) {
            if ($factura->metodo_pago == "Efectivo") {
                $totalEfectivo += $factura->precio;
            } else if ($factura->metodo_pago == "Mi Saldo") {
                $totalMiSaldo += $factura->precio;
            } else {
                $totalTarjeta += $factura->precio;
            }
        }

        $finanzas = Finanza::where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($finanzas as $finanza) {
            if ($finanza->tipo == "Salida") {

                if ($finanza->razon == "Consignacion") {
                    $totalEfectivo += $finanza->valor;
                    $totalTarjeta -= $finanza->valor;
                } else {
                    $totalGastos += $finanza->valor;
                }

            } else {
                $totalTarjeta += $finanza->valor;
                $totalEfectivo -= $finanza->valor;
            }
        }

        $nominas = Nomina::where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($nominas as $nomina) {
            $totalGastos += $nomina->valor;
        }

        $creditos = Credito::where('empresa_id', auth()->user()->empresa_id)->get();

        $avance = 0;
        $abono = 0;
        $recarga = 0;
        $pago = 0;
        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            if ($credito->tipo == 'Avance') {
                $avance += $credito->valor;
            } else if ($credito->tipo == 'Descuento') {

            } else if ($credito->tipo == 'Recarga') {
                $recarga += $credito->valor;
            } else if ($credito->tipo == 'Pago') {
                $pago += $credito->valor;
            } else {
                $abono += $credito->valor;
            }

        }

        $datos = [
            'totalEfectivo' => $totalEfectivo + $totalMiSaldo + $abono - $avance + ($recarga - $pago),
            'recarga' => $recarga,
            'pago' => $pago,
            'totalTarjeta' => $totalTarjeta,
            'totalTGastos' => $totalGastos,
            'totalMiSaldo' => $totalMiSaldo,
            'fecha' => Carbon::now()->format('Y-m')
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);
    }

    function getTotalProducidoDia()
    {
        $facturas = Factura::whereDate('created_at', '=', Carbon::now()->format('Y-m-d'))
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        $totalEfectivo = 0;
        $totalTarjeta = 0;
        $totalGastos = 0;
        $totalMiSaldo = 0;

        //obtener el nombre de los servicios

        foreach ($facturas as $factura) {
            if ($factura->metodo_pago == "Efectivo") {
                $totalEfectivo += $factura->precio;
            } else if ($factura->metodo_pago == "Mi Saldo") {
                $totalMiSaldo += $factura->precio;
            } else {
                $totalTarjeta += $factura->precio;
            }
        }

        $finanzas = Finanza::where('created_at', 'like', Carbon::now()->format('Y-m-d') . '%')
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($finanzas as $finanza) {
            if ($finanza->tipo == "Salida") {

                if ($finanza->razon == "Consignacion") {
                    $totalEfectivo += $finanza->valor;
                    $totalTarjeta -= $finanza->valor;
                } else {
                    $totalGastos += $finanza->valor;
                }

            } else {
                $totalTarjeta += $finanza->valor;
                //$totalEfectivo -= $finanza->valor;
            }
        }

        $nominas = Nomina::where('created_at', 'like', Carbon::now()->format('Y-m-d') . '%')
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($nominas as $nomina) {
            $totalGastos += $nomina->valor;
        }

        $creditos = Credito::where('empleado_id', '!=', null)
            ->where('created_at', 'like', Carbon::now()->format('Y-m') . '%')
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        $avance = 0;
        $abono = 0;

        foreach ($creditos as $credito) {
//        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id'
            if ($credito->tipo == 'Avance') {
                $avance += $credito->valor;
            } else if ($credito->tipo == 'Descuento') {

            } else {
                $abono += $credito->valor;
            }
        }

        $datos = [
            'totalEfectivo' => $totalEfectivo + $abono - $avance,
            'totalTarjeta' => $totalTarjeta,
            'totalTGastos' => $totalGastos,
            'totalMiSaldo' => $totalMiSaldo,
            'fecha' => Carbon::now()->format('Y-m-d')
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);
    }

    function getTotalProducidoTarjeta()
    {
        $facturas = Factura::where('metodo_pago', '=', 'Tarjeta')
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        $totalTarjeta = 0;
        $totalConsignacionesEntrada = 0;
        $totalConsignacionesSalida = 0;
        //obtener el nombre de los servicios

        foreach ($facturas as $factura) {
            $totalTarjeta += $factura->precio;
        }

        $finanzas = Finanza::where('razon', '=', 'Consignacion')
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($finanzas as $finanza) {
            if ($finanza->tipo == "Entrada") {
                $totalConsignacionesEntrada += $finanza->valor;
            } else {
                $totalConsignacionesSalida += $finanza->valor;
            }
        }

        $datos = [
            'totalTarjeta' => $totalTarjeta,
            'totalTarjetaEntrada' => $totalConsignacionesEntrada,
            'totalTarjetaSalida' => $totalConsignacionesSalida,
            'fecha' => Carbon::now()->format('Y-m-d')
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);
    }

    function getTotalPagoMes(Request $request)
    {
        $facturas = Factura::where("empleado_id", $request->empleado)
            ->where('created_at', 'like', $request->fecha . '%')
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        $usuario = User::findOrFail($request->empleado);

        $nomina = Nomina::where("empleado_id", $request->empleado)
            ->where('fecha', '=', $request->fecha)->where('empresa_id', auth()->user()->empresa_id)->get();

        if (count($nomina) >= 1) {
            $datos = [
                'totalGanancia' => 0,
                'sueldoFijo' => 0,
                'porcentajeGanancia' => $usuario->ganancia,
                'fecha' => $request->fecha
            ];
        } else {
            $total = 0;
            //obtener el nombre de los servicios
            if ($usuario->ganancia > 0) {
                foreach ($facturas as $factura) {
                    $total += $factura->precio * ($usuario->ganancia / 100);
                }
            }

            $datos = [
                'totalGanancia' => $total,
                'sueldoFijo' => $usuario->sueldo,
                'porcentajeGanancia' => $usuario->ganancia,
                'fecha' => $request->fecha
            ];
        }
        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);
    }

    function getPagosEmpleado(Request $request)
    {

        $nominas = Nomina::where("empleado_id", $request->empleado)
            ->where('fecha', 'like', $request->fecha . '%')->where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($nominas as $nomina) {
            $datos = [
                'descripcion' => $nomina->descripcion,
                'valor' => $nomina->valor,
                'tipo' => 'Nomina',
                'mes' => $nomina->fecha,
                'fecha' => date("d/m/Y h:i A", strtotime($nomina->created_at))
            ];
            array_push($array, $datos);

        }
        $creditos = Credito::where('empleado_id', '=', $request->empleado)
            ->where('created_at', 'like', $request->fecha . '%')->where('empresa_id', auth()->user()->empresa_id)->get();

        foreach ($creditos as $credito) {
            $datos = [
                'descripcion' => $credito->descripcion,
                'valor' => $credito->valor,
                'tipo' => $credito->tipo,
                'mes' => $request->fecha,
                'fecha' => date("d/m/Y h:i A", strtotime($credito->created_at))
            ];
            array_push($array, $datos);
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function guardarPagoNomina(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|string',
            'fecha' => 'required|string',
            'valor' => 'required|numeric',
            'empleado' => 'required|numeric',
        ]);
        $avance = 0;
        $abono = 0;
        if ($request->pagarDeuda) {
            $creditos = Credito::where('empleado_id', '=', $request->empleado)->where('empresa_id', auth()->user()->empresa_id)->get();


            foreach ($creditos as $credito) {
                if ($credito->tipo == 'Avance') {
                    $avance += $credito->valor;
                } else {
                    $abono += $credito->valor;
                }
            }
            if (($avance - $abono) > 0) {
                $credito = Credito::create([
                    'tipo' => 'Descuento',
                    'descripcion' => 'Descuento Directo de Nomina',
                    'valor' => $avance - $abono,
                    'empleado_id' => $request->empleado,
                    'user_id' => auth()->user()->id,
                    'empresa_id' => auth()->user()->empresa_id,
                ]);
            }

        }

        $nomina = Nomina::create([
            'fecha' => $request->fecha,
            'descripcion' => $request->descripcion,
            'valor' => ($avance - $abono) > 0 ? $request->valor - $credito->valor : $request->valor,
            'empleado_id' => $request->empleado,
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'deuda' => $request->pagarDeuda,
            'msg' => $this->datoNomina($nomina),
        );

        return response()->json($response);
    }

    function getNomina(Request $request)
    {

        $nominas = Nomina::where('fecha', 'like', $request->fecha)
            ->orderBy('created_at', 'asc')->where('empresa_id', auth()->user()->empresa_id)->get();

        $array = array();
        $array2 = array();

        foreach ($nominas as $nomina) {
            if (strlen($nomina->fecha) > 7) {
                array_push($array, $this->datoNomina($nomina));
            } else {
                array_push($array2, $this->datoNomina($nomina));
            }


        }

        $response = array(
            'status' => 'success',
            'msg' => ($request->tipo == "Diario" ? $array : $array2),
        );

        return response()->json($response);
    }

    public function datoGasto($data)
    {
        $datos = [
            'id' => $data->id,
            'descripcion' => $data->descripcion,
            'valor' => $data->valor,
            'tipo' => $data->tipo,
            'fecha' => date("d/m/Y h:i A", strtotime($data->created_at)),
        ];
        return $datos;
    }

    public function datoNomina($data)
    {
        $datos = [
            'id' => $data->id,
            'descripcion' => $data->descripcion,
            'valor' => $data->valor,
            'fecha' => $data->fecha,
            'fechaPago' => date("d/m/Y h:i A", strtotime($data->created_at)),
            'empleado' => $data->empleado->nombre,
        ];
        return $datos;
    }

}
