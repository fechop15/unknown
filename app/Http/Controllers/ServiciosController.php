<?php

namespace App\Http\Controllers;

use App\Servicio;
use Illuminate\Http\Request;

class ServiciosController extends Controller
{
    //
    function getServicios()
    {
        $servicios = Servicio::where('empresa_id', auth()->user()->empresa_id)->get();

        $response = array(
            'status' => 'success',
            'msg' => $servicios,
        );

        return response()->json($response);
    }

    function crearServicio(Request $request)
    {

        $this->validate($request, [
            'nombre' => 'required|string|unique:servicios,nombre',
            'precio' => 'required|numeric',
        ]);

        $servicio = Servicio::create([
            'nombre' => $request->nombre,
            'precio' => $request->precio,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $servicio,
        );

        return response()->json($response);

    }

    function actualizarServicio(Request $request)
    {

        $servicio = Servicio::findOrFail($request->id);

        $this->validate($request, [
            'nombre' => 'required|string',
            'precio' => 'required|numeric',
            'estado' => 'required|numeric',
        ]);

        $servicio->update($request->all());


        $response = array(
            'status' => 'success',
            'msg' => $servicio,
        );

        return response()->json($response);

    }

    function buscarServicio(Request $request)
    {
        $servicio = Servicio::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $servicio,
        );

        return response()->json($response);
    }
}
