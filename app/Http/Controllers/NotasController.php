<?php

namespace App\Http\Controllers;

use App\Nota;
use Illuminate\Http\Request;

class NotasController extends Controller
{
    //


    function getNotas(Request $request)
    {
        $notas = Nota::where("cliente_id", "=", $request->cliente)->where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();
        foreach ($notas as $nota) {
            array_push($array, $this->datos($nota));
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearNota(Request $request)
    {

        $this->validate($request, [
            'titulo' => 'required|string',
            'descripcion' => 'required|string',
            'cliente_id' => 'required',
        ]);

        $data = $request;

        $nota = Nota::create([
            'titulo' => $data['titulo'],
            'descripcion' => $data['descripcion'],
            'cliente_id' => $data['cliente_id'],
            'user_id' => auth()->user()->id,
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($nota),
        );

        return response()->json($response);

    }

    function actualizarNota(Request $request)
    {

        $nota = Nota::findOrFail($request->id);
        $request->cliente_id = auth()->user()->id;
        $this->validate($request, [
            'titulo' => 'required|string',
            'descripcion' => 'required|string',
        ]);

        $nota->update($request->all());

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($nota),
        );

        return response()->json($response);

    }

    function buscarNota(Request $request)
    {
        $nota = Nota::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $this->datos($nota),
        );

        return response()->json($response);
    }

    public function datos($nota)
    {
        $datos = [
            'id' => $nota->id,
            'titulo' => $nota->titulo,
            'descripcion' => $nota->descripcion,
            'fecha' => date("d/m/Y h:i A", strtotime($nota->updated_at)),
        ];
        return $datos;
    }
}
