<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    function getUsuarios()
    {
        $usuarios = User::where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($usuarios as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->nombre,
                'cedula' => $usuario->cedula,
                'email' => $usuario->email,
                'ganancia' => $usuario->ganancia,
                'rol' => $usuario->rol->nombre,
                'telefono' => $usuario->telefono,
                'estado' => $usuario->estado,
                'tipoPago' => $usuario->tipoPago,
                'sueldo' => $usuario->sueldo,
                'imagen' => $usuario->imagen
            ];

            array_push($array, $datos);

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function crearUsuario(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6',
            'rol_id' => 'required',
            'nombre' => 'required|string',
            'cedula' => 'required|numeric',
            'telefono' => 'required|numeric',
            'sueldo' => 'required|numeric',
            'tipoPago' => 'required|string',
            'ganancia' => 'required|numeric',
        ]);

        $data = $request;

        $usuario = user::create([

            'nombre' => $data['nombre'],
            'cedula' => $data['cedula'],
            'email' => $data['email'],
            'telefono' => $data['telefono'],
            'password' => bcrypt($data['password']),
            'rol_id' => $data['rol_id'],
            'tipoPago' => $data['tipoPago'],
            'sueldo' => $data['sueldo'],
            'ganancia' => $data['ganancia'],
            'empresa_id' => auth()->user()->empresa_id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => $usuario,
        );

        return response()->json($response);

    }

    function actualizarUsuario(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $request->id,
            'rol_id' => 'required',
            'nombre' => 'required|string',
            'cedula' => 'required|numeric',
            'telefono' => 'required|numeric',
        ]);

        $usuario->update($request->all());

        $datos = [
            'id' => $usuario->id,
            'nombre' => $usuario->nombre,
            'cedula' => $usuario->cedula,
            'email' => $usuario->email,
            'ganancia' => $usuario->ganancia,
            'rol' => $usuario->rol->nombre,
            'telefono' => $usuario->telefono,
            'estado' => $usuario->estado,
            'tipoPago' => $usuario->tipoPago,
            'sueldo' => $usuario->sueldo,
        ];

        $response = array(
            'status' => 'success',
            'msg' => $datos,
        );

        return response()->json($response);

    }

    function buscarUsuario(Request $request)
    {
        $usuario = User::findOrFail($request->id);

        $response = array(
            'status' => 'success',
            'msg' => $usuario,
        );

        return response()->json($response);
    }

    function getUsuariosByRol(Request $request)
    {

        $usuarios = User::where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($usuarios as $usuario) {
            if ($usuario->rol->nombre == $request->rol && $usuario->estado == true) {

                $datos = [
                    'id' => $usuario->id,
                    'nombre' => $usuario->nombre,
                    'cedula' => $usuario->cedula,
                    'email' => $usuario->email,
                    'ganancia' => $usuario->ganancia,
                    'rol' => $usuario->rol->nombre,
                    'telefono' => $usuario->telefono,
                    'estado' => $usuario->estado,
                    'tipoPago' => $usuario->tipoPago,
                    'sueldo' => $usuario->sueldo,
                ];

                array_push($array, $datos);
            }

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getUsuariosByPago(Request $request)
    {

        $usuarios = User::where('empresa_id', auth()->user()->empresa_id)->get();
        $array = array();

        foreach ($usuarios as $usuario) {
            if ($usuario->rol->nombre == $request->rol && $usuario->tipoPago == $request->pago) {

                $datos = [
                    'id' => $usuario->id,
                    'nombre' => $usuario->nombre,
                    'cedula' => $usuario->cedula,
                    'email' => $usuario->email,
                    'ganancia' => $usuario->ganancia,
                    'rol' => $usuario->rol->nombre,
                    'telefono' => $usuario->telefono,
                    'estado' => $usuario->estado,
                    'tipoPago' => $usuario->tipoPago,
                    'sueldo' => $usuario->sueldo,
                ];

                array_push($array, $datos);
            }

        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    /*Actulizacion*/
    function actualizarPass(Request $request)
    {

        if ($request->id) {
            $usuario = User::findOrFail($request->id);
            $this->validate($request, [
                'pass' => 'required|string|min:6',
            ]);

            if (auth()->user()->rol_id != 1 && ($usuario->rol_id == 1 || $usuario->rol_id == 2 || $usuario->rol_id == 3)) {
                $response = array(
                    'status' => 'Error',
                    'msg' => "Error, Falta de privilegios",
                );
                return response()->json($response);

            } else {
                $usuario->password = bcrypt($request->pass);
                $usuario->save();
                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );
                return response()->json($response);

            }

        } else {

            $usuario = User::findOrFail(auth()->user()->id);
            $this->validate($request, [
                'passOld' => 'required|string',
                'pass' => 'required|string|min:6',
            ]);

            if (Hash::check($request->passOld, $usuario->password)) {

                $usuario->password = bcrypt($request->pass);
                $usuario->save();

                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );

            } else {
                $response = array(
                    'status' => 'Error',
                    'status2' => Hash::check($request->passOld, $usuario->password),
                    'msg' => "la Contraseña actual no es correcta",
                );
            }

            return response()->json($response);

        }

    }

    function update_profile(Request $request)
    {

        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $path = "images/perfil/";

        $filename = Auth::id() . '_' . time() . '.' . $request->avatar->getClientOriginalExtension();
        move_uploaded_file($request->file('avatar'), $path . $filename);

        $user = Auth::user();
        $user->imagen = $filename;
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $filename,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }

    function remove_profile()
    {

        $user = Auth::user();
        $user->imagen = 'avatar-1.png';
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $user->imagen,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }
}
