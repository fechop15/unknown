<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuarios = User::where('empresa_id', auth()->user()->empresa_id)->get();
        $clientes = User::where('empresa_id', auth()->user()->empresa_id)->get();

        return view('nueva.page.inicio', compact('usuarios', 'clientes'));

    }

    public function usuarios($id = null)
    {
        if ($id == null)
            return view('nueva.page.usuarios');
        else {
            $empleado = User::findOrFail($id);
            return view('nueva.page.empleado', compact('empleado'));
        }

    }

    public function blank()
    {
        return view('page.blank');
    }

//eliminar
    public function servicios()
    {
        return view('page.servicios');
    }

//
    public function serviciosEmpleado()
    {
        return view('nueva.page.servicioEmpleado');
    }

    public function historico()
    {
        return view('nueva.page.historico');
    }

    public function perfil($id = null)
    {
        if ($id == null) {
            return view('nueva.page.perfil');
        } else {
            $cliente = Cliente::findOrFail($id);
            return view('nueva.page.cliente', compact('cliente'));
        }
    }

    public function clientes()
    {
        return view('nueva.page.clientes');
    }

    public function gastosFijos()
    {
        return view('nueva.page.gastos.fijos');
    }

    public function gastosInsumos()
    {
        return view('nueva.page.gastos.insumos');
    }

    public function gastosOtros()
    {
        return view('nueva.page.gastos.otros');
    }

    public function pagosDoctores()
    {
        return view('nueva.page.pagos.doctores');
    }

    public function pagosNomina()
    {
        return view('nueva.page.pagos.nomina');
    }

    public function consignaciones()
    {
        return view('nueva.page.consignaciones');
    }

    public function credito()
    {
        return view('nueva.page.pagos.credito');
    }

    public function empresas()
    {
        return view('nueva.page.empresas');
    }

    public function empresa($id = null)
    {

        if (auth()->user()->id == 1) {
            if ($id == null) {
                return view('nueva.page.empresas.empresa');
            } else {
                $usuario = User::find(1);
                $usuario->empresa_id = $id;
                $usuario->save();
                return redirect()->route('servicios');

            }

        } else {
            $response = array(
                'status' => 'error',
                'msg' => "No tienes privilegios para esta accion",
            );
        }
        return response()->json($response);
    }
}
