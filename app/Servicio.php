<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    //
    protected $fillable = [
        'nombre', 'precio', 'estado','empresa_id'
    ];

}
