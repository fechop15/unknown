<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    //

    protected $fillable = [
        'tipo', 'descripcion', 'valor', 'empleado_id', 'user_id', 'cliente_id','empresa_id'
    ];

    public function empleado()
    {
        return $this->belongsTo(User::class, 'empleado_id');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }
}
