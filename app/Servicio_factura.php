<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio_factura extends Model
{
    //
    protected $fillable = [
        'servicio_id', 'factura_id','empresa_id'
    ];


    public function servicio()
    {
        return $this->belongsTo(Servicio::class);
    }

    public function factura()
    {
        return $this->belongsTo(Factura::class);
    }
}
