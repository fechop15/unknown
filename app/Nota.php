<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    //
    protected $fillable = [
        'titulo', 'descripcion', 'user_id', 'cliente_id','empresa_id'
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }


}
