<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $fillable = [
        'nombre', 'cedula', 'telefono', 'direccion', 'ciudad', 'ocupacion', 'estado_civil', 'user_id', 'edad','empresa_id'
    ];

    public function facturas()
    {
        return $this->hasMany(Factura::class);
    }

    public function notas()
    {
        return $this->hasMany(Nota::class);
    }

    public function credito()
    {
        return $this->hasMany(Credito::class);
    }
}
