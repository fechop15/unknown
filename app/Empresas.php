<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    //
    protected $fillable = [
        'id', 'nombre', 'representante', 'nit', 'contacto', 'cuota', 'color', 'logo', 'lema', 'estado'
    ];

    public function pagos()
    {
        return $this->hasMany(Pagos::class);
    }
}
