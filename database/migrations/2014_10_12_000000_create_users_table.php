<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('cedula');
            $table->string('email')->unique();
            $table->string('telefono');
            $table->string('password');
            $table->integer('sueldo')->default("0");
            $table->string('tipoPago');
            $table->string('ganancia')->default("0");
            $table->boolean('estado')->default(true);
            $table->rememberToken();
            $table->integer('rol_id')->unsigned();
            $table->foreign('rol_id')->references('id')->on('rols');
            $table->dateTime('last_login')->nullable();
            $table->string('imagen')->default('avatar-1.png');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
