<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('servicios')->insert([
            'nombre' => 'Corte Adulto',
            'precio' => 5000,
            'empresa_id' => 1
        ]);

        DB::table('servicios')->insert([
            'nombre' => 'Corte niño',
            'precio' => 3000,
            'empresa_id' => 1
        ]);


    }
}
