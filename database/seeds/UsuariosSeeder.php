<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'nombre' => 'Admin',
            'email' => 'admin@gmail.com',
            'cedula' => 00000000,
            'telefono' => 00000000,
            'password' => bcrypt('123456'),
            'tipoPago' => 'Mensual',
            'rol_id' => 1,
            'empresa_id' => 1
        ]);

        DB::table('users')->insert([
            'nombre' => 'Luis Avila',
            'email' => 'luisg.avilaa@gmail.com',
            'cedula' => 1102856657,
            'telefono' => 3116614896,
            'password' => bcrypt('123456'),
            'tipoPago' => 'Mensual',
            'rol_id' => 2,
            'empresa_id' => 1
        ]);

    }
}
