<!doctype html>
<html lang="en">
<head>
    @include('include.head')
</head>
<body class="sidebar-mini fixed">


<div class="wrapper">
    <div class="loader-bg">
        <div class="loader-bar">
        </div>
    </div>

    @include('include.navbar')

    <div class="main-container">
        @include('include.sidebar')
        @include('include.sidebarChat')

        @yield('page')
    </div>

</div>
@include('include.script')
@yield('js')
@yield('modaljs')
</body>
</html>

