<div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro De Empleado</h5>
            </div>

            <div class="modal-body">
                <form id="empleados">

                    <label class="form-control-label">Nombre </label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre De Usuario">
                    </div>

                    <label class="form-control-label">Identificacion </label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                        <input type="text" class="form-control" id="cedula" placeholder="Cedula De Ciudadania">
                    </div>

                    <label class="form-control-label">Correo</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                        <input type="email" class="form-control" id="email" placeholder="Correo Electronico">
                    </div>

                    <label class="form-control-label">Telefono </label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                        <input type="number" class="form-control" id="telefono" placeholder="Telefono">
                    </div>

                    <label class="form-control-label">Contraseña </label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                        <input type="text" class="form-control" id="password" placeholder="Contraseña">
                    </div>

                    <label class="form-control-label">Sueldo Fijo</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                        <input type="number" class="form-control" id="sueldo" placeholder="Sueldo Fijo a Pagar">
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label" for="single-select">Tipo Pago</label>
                        <select id="tipoPago" class="form-control">
                            <option value="Diario">Diario</option>
                            <option value="Mensual">Mensual</option>
                        </select>
                    </div>

                    <label class="form-control-label">Ganancia</label>
                    <div class="input-group">
                        <input type="number" id="ganancia" class="form-control" placeholder="% de Ganancia"
                               aria-describedby="basic-addon2">
                        <span class="input-group-addon" id="basic-addon2">%</span>
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label" for="single-select">Rol</label>
                        <select id="rol" class="form-control">
                            <option value="1">Administrador</option>
                            <option value="2">Empleado</option>
                        </select>
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label" for="single-select">Estado</label>
                        <select id="estado" class="form-control">
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                    <label id="error" class="has-error"></label>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Empleado</h5>
            </div>

            <div class="modal-body">
                <form id="empleados-actualizar">
                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label">Nombre</label>
                        <input type="name" id="Anombre" class="form-control">
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label">Cedula</label>
                        <input type="number" id="Acedula" class="form-control">
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label">Email</label>
                        <input type="email" id="Aemail" class="form-control">
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label">Telefono</label>
                        <input type="number" id="Atelefono" class="form-control">
                    </div>

                    <div class="input-group">
                        <label class="form-control-label">Sueldo Fijo</label>
                        <input type="number" class="form-control" id="Asueldo">
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label" for="single-select">Tipo Pago</label>
                        <select id="AtipoPago" class="form-control">
                            <option value="Diario">Diario</option>
                            <option value="Mensual">Mensual</option>
                        </select>
                    </div>

                    <label class="form-control-label">Ganancia</label>
                    <div class="input-group">
                        <input type="number" id="Aganancia" class="form-control" placeholder="Percentage"
                               aria-describedby="basic-addon2">
                        <span class="input-group-addon" id="basic-addon2">%</span>
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label" for="single-select">Rol</label>
                        <select id="Arol" class="form-control">
                            <option value="1">Administrador</option>
                            <option value="2">Usuario</option>
                        </select>
                    </div>

                    <div class="form-group" style="margin-bottom: 0px;">
                        <label class="form-control-label" for="single-select">Estado</label>
                        <select id="Aestado" class="form-control">
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Cambio de contraseña</h5>
            </div>

            <div class="modal-body">

                <form id="form-password">

                    <div class="form-group">
                        <label class="col-form-label">Contraseña nueva:</label>
                        <input type="text" class="form-control" id="pass">
                    </div>


                    <div class="form-group">
                        <div class="alert alert-danger" id="Perror" style="display: none">
                        </div>
                    </div>
                </form>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="cambiarPass()">Cambiar
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>


    </script>

@endsection