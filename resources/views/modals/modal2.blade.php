<div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">Servicio Nuevo</h5>
            </div>

            <div class="modal-body">
                <label class="form-control-label">Nombre </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="nombre" placeholder="Nombre Del Servicio">
                </div>

                <label class="form-control-label">Precio </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="number" class="form-control" id="precio" placeholder="Precio Del Servicio">
                </div>

                <label id="error" class="has-error"></label>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardarServicio()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-2-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Servicio</h5>
            </div>

            <div class="modal-body">

                <label class="form-control-label">Nombre </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="Anombre" placeholder="Nombre Del Servicio">
                </div>

                <label class="form-control-label">Precio </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="number" class="form-control" id="Aprecio" placeholder="Precio Del Servicio">
                </div>

                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="form-control-label">Estado </label>
                    <select id="Aestado" class="form-control">
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>
                </div>

                <label id="error" class="has-error"></label>

            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizarServicio()">
                    Actualizar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-2-produccion" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-purple">
                <h5 class="modal-title h4" id="myLargeModalLabel" style="color: white;">Registrar Produccion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-produccion">
                    <div class="row">

                        <div class="col-md-12">
                            <label class="text-c-purple">Cliente: </label>
                            <select class="js-example-basic-single select2-hidden-accessible" id="select-cliente"
                                    name="cliente" style="width: 100%;">
                                <option value="" selected="selected" disabled> Buscar Cliente</option>
                            </select>
                            <label style="color: gray;" id="notaSaldo"> </label>
                        </div>

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Servicio:</label>
                            <input type="text" class="form-control" id="Pservicio" placeholder="Servicio Prestado">
                        </div>

                        <div class="col-md-12">
                            <label class="text-c-purple">Atendido Por:</label>
                            <select class="js-example-basic-single select2-hidden-accessible" id="select-usuarios"
                                    name="empleado" style="width: 100%">
                                <option value="" selected="selected" disabled> Buscar Empleado</option>
                            </select>
                        </div>

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Valor:</label>
                            <input type="number" class="form-control" id="Pprecio" placeholder="Valor del Servicio"
                                   onkeyup="calcularToral(this)">
                        </div>

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Metodo De Pago:</label>
                            <select id="PmetodoPago" class="form-control" style="width: 100%">
                                <option disabled>Seleccione un metodo</option>
                                <option value="1">Efectivo</option>
                                <option value="3">Mi Saldo</option>
                                <option value="2">Tarjeta</option>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert" id="produccion-error" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <h4 class="modal-title text-center" style="color: #4680ff">Total: $<strong id="totalRegistroProduccion">0</strong>
                </h4>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" id="produccion"
                        onclick="guardarProduccion()">
                    Guardar
                </button>
                <button type="button" class="btn btn-primary waves-effect waves-light"
                        onclick="actualizarProduccion()">
                    Actualizar
                </button>
            </div>
        </div>
    </div>
</div>
