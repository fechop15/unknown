<div class="modal fade" id="modal-8" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registrar Movimientos Tarjeta</h5>
            </div>

            <div class="modal-body p-5">
                <label class="form-control-label">Descripcion</label>
                <div class="input-group">
                    <textarea class="form-control" id="descripcion" rows="4"></textarea>
                </div>

                <label class="form-control-label">Valor </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-money"></i></span>
                    <input type="number" class="form-control" id="valor" placeholder="valor"
                           onkeyup="disponibilidad(this)">
                </div>

                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="form-control-label" for="single-select">Tipo de Movimiento</label>
                    <select id="tipo" class="form-control" onchange="verificar()">
                        <option value="Salida">Salida</option>
                        <option value="Entrada">Entrada</option>
                    </select>
                </div>

                <label id="error" class="has-error"></label>

            </div>

            <div class="modal-footer border-0">
                <h4 class="modal-title text-center">Total: $<strong id="totalConsignacion">0</strong></h4>

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()" id="guardar"
                        style="display: none">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>