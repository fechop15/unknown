<div class="modal fade" id="modal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Registro De Cliente</h5>
            </div>

            <div class="modal-body">

                <label class="form-control-label">Nombre </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="nombre" placeholder="Nombre Del Cliente">
                </div>

                <label class="form-control-label">Identificacion </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="cedula" placeholder="Cedula De Ciudadania">
                </div>

                <label class="form-control-label">Edad </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="edad" placeholder="Años de edad">
                </div>

                <label class="form-control-label">Telefono </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="number" class="form-control" id="telefono" placeholder="Telefono">
                </div>

                <label class="form-control-label">Direccion</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" id="direccion" class="form-control" placeholder="Direccion de residencia">
                </div>

                <label class="form-control-label">Ocupacion</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" id="ocupacion" class="form-control" placeholder="Ocupacion">
                </div>

                <label class="form-control-label">Ciudad</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" id="ciudad" class="form-control" placeholder="Ciudad">
                </div>

                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="form-control-label" for="single-select">Estado Civil</label>
                    <select id="estado_civil" class="form-control">
                        <option value="Casado(a)">Casado(a)</option>
                        <option value="Soltero(a)">Soltero(a)</option>
                        <option value="Union Libre">Union Libre</option>
                    </select>
                </div>

                <label id="error" class="has-error"></label>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardarPaciente()">Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-actualizar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Actualizar Cliente</h5>
            </div>

            <div class="modal-body">

                <label class="form-control-label">Nombre </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="Anombre" placeholder="Nombre Del Cliente">
                </div>

                <label class="form-control-label">Identificacion </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="Acedula" placeholder="Cedula De Ciudadania">
                </div>

                <label class="form-control-label">Edad </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" class="form-control" id="Aedad" placeholder="Años de edad">
                </div>

                <label class="form-control-label">Telefono </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="number" class="form-control" id="Atelefono" placeholder="Telefono">
                </div>

                <label class="form-control-label">Direccion</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" id="Adireccion" class="form-control" placeholder="Direccion de Residencia">
                </div>

                <label class="form-control-label">Ocupacion</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" id="Aocupacion" class="form-control" placeholder="Ocupacion">
                </div>

                <label class="form-control-label">Ciudad</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                    <input type="text" id="Aciudad" class="form-control" placeholder="Ciudad">
                </div>

                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="form-control-label" for="single-select">Estado Civil</label>
                    <select id="Aestado_civil" class="form-control">
                        <option value="Casado(a)">Casado(a)</option>
                        <option value="Soltero(a)">Soltero(a)</option>
                        <option value="Union Libre">Union Libre</option>
                    </select>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="actualizar()">Actualizar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Ingresar Efectivo</h5>
            </div>

            <div class="modal-body">

                <label class="form-control-label">Dinero a Ingresar:</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-cur-dollar"></i></span>
                    <input type="number" class="form-control" id="abono">
                </div>

                <label class="form-control-label">Descripcion</label>
                <div class="input-group">
                    <textarea class="form-control" id="descripcion" rows="4"></textarea>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="abonar()">
                    Abonar
                </button>
            </div>
        </div>
    </div>
</div>

@section('modaljs')
    <script>


    </script>

@endsection