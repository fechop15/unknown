<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Avance de pago</h5>
            </div>
            <div class="modal-body p-5">
                <label class="form-control-label">Empleado</label>
                <div class="input-group">
                    <select class="js-example-basic-single select2-hidden-accessible" id="select-usuarios"
                            name="empleado" style="width: 100%">
                        <option value="" selected="selected" disabled> Buscar Empleado</option>

                    </select>
                </div>

                <div class="form-group" style="margin-bottom: 0px;">
                    <label class="form-control-label">Tipo Credito</label>
                    <select id="tipo" class="form-control">
                        <option disabled>Seleccionar</option>
                        <option value="Avance">Avance</option>
                        <option value="Abono">Abono</option>
                    </select>
                </div>

                <label class="form-control-label">Valor </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icofont icofont-money"></i></span>
                    <input type="number" class="form-control" id="valor" placeholder="valor"
                           onkeyup="disponibilidad(this)">
                </div>

                <label class="form-control-label">Descripcion</label>
                <div class="input-group">
                    <textarea class="form-control" id="descripcion" rows="4"></textarea>
                </div>

                <label class="form-control-label" id="nota"></label>

                <label id="error" class="has-error"></label>

            </div>

            <div class="modal-footer">
                <h4 class="modal-title text-center">Total: $<strong id="totalAPagar">0</strong></h4>

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()" id="guardar"
                        style="display: none;">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>
