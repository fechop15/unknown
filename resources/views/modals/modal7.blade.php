<div class="modal fade" id="modal-7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title">Pago Doctor</h5>
            </div>
            <div class="modal-body p-5">
                <label class="form-control-label">Doctor</label>
                <div class="input-group">
                    <select class="js-example-basic-single select2-hidden-accessible" id="select-usuarios"
                            name="empleado" style="width: 100%">
                        <option value="" selected="selected" disabled> Buscar Doctor</option>

                    </select>
                </div>

                <label class="form-control-label">Dia a Pagar</label>
                <div id="fechaDiaNomina"></div>

                <label class="form-control-label">Descripcion</label>
                <div class="input-group">
                    <textarea class="form-control" id="descripcion" rows="4"></textarea>
                </div>


                <label id="error" class="has-error"></label>

            </div>

            <div class="modal-footer">
                <h4 class="modal-title text-center">Total: $<strong id="totalAPagar">0</strong></h4>

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="guardar()" id="guardar" style="display: none">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>