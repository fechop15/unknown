@extends('nueva.layout.Dashboard')
@section('page')

    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Historial</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Historico</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row animated fadeIn">
                <div class="col-sm-12">
                    <div class="row no-gutters">
                        <div class="col-md-4 col-xl-2">
                            <div class="card">
                                <div class="card-body">
                                    <span>Total Recarga:</span>
                                    <h6 class="float-right text-c-green d-inline-block m-0"
                                        id="totalCreditoCliente">
                                        0</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xl-2">
                            <div class="card">
                                <div class="card-body">
                                    <span>Total:</span>
                                    <h6 class="float-right text-c-green d-inline-block m-0" id="total">0</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xl-2">
                            <div class="card">
                                <div class="card-body">
                                    <span>Mi Saldo:</span>
                                    <h6 class="float-right text-c-green d-inline-block m-0" id="totalM">0</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xl-2">
                            <div class="card">
                                <div class="card-body">
                                    <span>Tarjeta:</span>
                                    <h6 class="float-right text-c-green d-inline-block m-0" id="totalT">0</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xl-2">
                            <div class="card">
                                <div class="card-body">
                                    <span>Efectivo:</span>
                                    <h6 class="float-right text-c-green d-inline-block m-0" id="totalE">0</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xl-2">
                            <div class="card">
                                <div class="card-body" style="padding-top: 9px; padding-bottom: 9px;">
                                    <button type="button" class="btn btn-primary btn-block" style="float: right"
                                            onclick="registrarProduccion()">
                                        <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Produccion</h5>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover" id="tablaProduccion" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Servicio</th>
                                        <th>Atendido Por</th>
                                        <th>Precio</th>
                                        <th>Metodo De Pago</th>
                                        <th>Fecha Creacion</th>
                                        @if(auth()->user()->id==1)
                                            <th>Opciones</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="col-md-12">
                        <div class="card quater-card">
                            <div class="card-body">
                                <h5 class="mb-3">Produccion</h5>

                                <h6 class="text-muted m-b-15">Recaudado Virtual</h6>
                                <h4 id="producidoT">$0</h4>

                                <h6 class="text-muted m-b-15">Recaudado Efectivo del mes</h6>
                                <h4 id="producido">$0</h4>

                                <h6 class="text-muted m-b-15">Saldo Cliente</h6>
                                <h4 id="totalSC">$0</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                            <div class="card-block">
                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                           id="select_dia">Dia</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                           id="select_mes">Mes</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaDia"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="profile3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaMes"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="messages3" role="tabpanel">
                                        <div class="container">
                                            <br>
                                            <div class='col-md-12'>
                                                <div class="form-group">
                                                    <label for="">Desde</label>
                                                    <div class="input-group date input-group-date-custom">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-addon bg-primary">
                                                            <i class="icofont icofont-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='col-md-12'>
                                                <div class="form-group">
                                                    <label for="">Hasta</label>
                                                    <div class="input-group date input-group-date-custom">
                                                        <input type="text" class="form-control">
                                                        <span class="input-group-addon bg-primary">
                                                            <i class="icofont icofont-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-block waves-effect"
                                                data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title=".btn-success .btn-block">Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.produccion')

@endsection

@section('js')

    <script>

        var opciones = [];
        var REGISTROS = [];
        var USUARIOS = [];
        var CLIENTES = [];
        var FECHA = [];
        var TABLA = null;
        var totalCreditoCliente = 0;

        $(document).ready(function () {
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Empleado'}
            ).done(function (data) {
                //console.log(data);

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal-2-produccion"),
                    minimumResultsForSearch: 5,
                    placeholder: "Buscar Empleado",
                    allowClear: true,
                    data: USUARIOS
                });

            });
            $.get(
                "/get-clientes"
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre + " - " + data.msg[item].cedula
                    };
                    CLIENTES.push(itemSelect2)
                }
                $('#select-cliente').select2({
                    dropdownParent: $("#modal-2-produccion"),
                    minimumResultsForSearch: 5,
                    placeholder: "Buscar Cliente",
                    allowClear: true,
                    data: CLIENTES
                });

            });

            $('#select-servicios').on('select2:select', function (e) {
                calcularToral($('#select-servicios').select2("val"));
            });

            $('#select-cliente').on('change', function (e) {
                consultarSaldoCuenta($('#select-cliente').val());
            });
            getProduccion();

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });
            //rango//
            $('#datetimepicker6').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true
            });

            $('#datetimepicker7').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                FECHA = $("#fechaDia").data("date");
                TABLA.ajax.reload();
                //gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
                //gerRegistrosMes($("#fechaMes").data("date"));
            });

            FECHA = $("#fechaDia").data("date");

            TABLA = $('#tablaProduccion').DataTable({
                "ajax": {
                    "url": "/get-produccion-fecha",
                    "data": function (d) {
                        d.fecha = FECHA;
                    },
                    "type": "get",
                    "dataSrc": function (data) {
                        var json = [];
                        REGISTROS = [];
                        console.log(data);
                        for (var item in data.msg) {
                            REGISTROS.push(data.msg[item]);
                            var pago = '';
                            var editar = true;
                            if (data.msg[item].metodoP == 'Efectivo') {
                                pago = '<span class="badge badge-success">Efectivo</span>';
                            } else if (data.msg[item].metodoP == 'Tarjeta') {
                                pago = '<span class="badge badge-info">Tarjeta</span>';
                            } else {
                                pago = '<span class="badge badge-warning">Mi Saldo</span>';
                                editar = false;
                            }
                            var itemJson = {
                                Id: data.msg[item].id,
                                Nombre: data.msg[item].nombre,
                                Servicio: data.msg[item].servicio,
                                Atendido: data.msg[item].atendido,
                                Precio: '$' + parseInt(data.msg[item].precio).toLocaleString(),
                                MetodoPago: pago,
                                Fecha: data.msg[item].fecha,
                                @if(auth()->user()->id==1)
                                Opciones: pago == 'Mi Saldo' ? '' : opciones(editar),
                                @endif
                            };
                            json.push(itemJson)
                        }
                        creditosCliente(FECHA);
                        getProduccion();
                        return json;
                    }
                },
                columns: [
                    {data: "Nombre"},
                    {data: "Servicio"},
                    {data: "Atendido"},
                    {data: "Precio"},
                    {data: "MetodoPago"},
                    {data: "Fecha"},
                        @if(auth()->user()->id==1)
                    {
                        data: "Opciones"
                    },
                    @endif
                ],
            });

            @if(auth()->user()->id==1)
            function opciones(editar) {
                if (editar) {
                    var opciones = '' +
                        '<button type="button" class="btn btn-primary btn-sm waves-effect waves-light actualizar" ' +
                        '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit">' +
                        '           <i class="fa fa-edit"></i>' +
                        '</button>' +
                        '<button type="button" class="btn btn-danger btn-sm waves-effect waves-light eliminar" ' +
                        '           data-toggle="tooltip" data-placement="top" title="Ver perfil" data-original-title="Edit">' +
                        '           <i class="fa fa-trash"></i>' +
                        '</button>';

                    return opciones;
                } else {
                    return "";
                }
            }

            TABLA.on('click', '.actualizar', function () {
                $tr = $(this).closest('tr');
                var data = TABLA.row($tr).data();
                $('#id').val(data.Id);

                $("#form-produccion")[0].reset();
                $('#guardarProduccion').hide();
                $('#actualizarProduccion').show();
                $('#modal-2-produccion').modal('show');

                cargar(true, '#actualizarProduccion');
                $.ajax({
                        url: '/buscar-produccion',
                        type: 'POST',
                        data: {
                            id: data.Id,
                            _token: $('meta[name="csrf-token"]').attr('content')
                        },

                    }
                ).done(function (response) {
                    //console.log(response);
                    console.log(response);

                    $('#select-cliente').val(response.msg.cliente_id).trigger('change');
                    $('#Pservicio').val(response.msg.servicio);
                    $('#select-usuarios').val(response.msg.empleado_id).trigger('change');
                    $('#Pprecio').val(response.msg.precio);
                    var metodoPago = 0;
                    if (response.msg.metodo_pago === "Efectivo") {
                        metodoPago = 1;
                    } else if (response.msg.metodo_pago === "Tarjeta") {
                        metodoPago = 2;
                    } else {
                        metodoPago = 3;
                    }
                    $('#PmetodoPago').val(metodoPago);

                    cargar(false, '#actualizarProduccion');

                    return response;

                }).fail(function (error) {
                    console.log(error);
                    cargar(false, '#actualizarProduccion');
                });

            });

            TABLA.on('click', '.eliminar', function () {
                $tr = $(this).closest('tr');
                var data = TABLA.row($tr).data();
                let opcion = "";

                swal({
                    title: '¿Estas seguro?',
                    text: "¿Vas a Eliminar este Registro? ",
                    icon: 'warning',
                    buttons: {
                        confirm: {
                            text: 'Si, Eliminar ',
                            className: 'btn btn-success'
                        },
                        cancel: {
                            text: 'Cancelar',
                            visible: true,
                            className: 'btn btn-danger'
                        }
                    }
                }).then((Delete) => {
                    if (Delete) {

                        $.ajax({
                                url: '/eliminarProduccion',
                                type: 'POST',
                                data: {
                                    id: data.Id,
                                    _token: $('meta[name="csrf-token"]').attr('content')
                                },

                            }
                        ).done(function (response) {
                            console.log(response);
                            if (response.status == "Error") {
                                //error
                            } else {

                                TABLA.ajax.reload();
                                swal({
                                    title: 'Ok!',
                                    text: 'El Registro Ha Sido Eliminado',
                                    icon: 'success',
                                    buttons: {
                                        confirm: {
                                            className: 'btn btn-success'
                                        }
                                    }
                                });
                            }
                            //return response;
                        }).fail(function (error) {

                            console.log(error);

                        });


                    } else {
                        swal.close();
                    }
                });

            });

            @endif
            function consultarSaldoCuenta(IDCLIENTE) {
                $.post(
                    "/get-estado-credito-cliente", {
                        cliente: IDCLIENTE,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    var valor = data.recarga - data.pago;
                    SALDO = valor;
                    if (valor >= 0) {
                        $("#notaSaldo").html("Saldo Actual: $" + valor.toLocaleString());
                    } else {
                        $("#notaSaldo").html("Actualmente Debe: $0");
                    }
                });
            }

            function creditosCliente(fecha) {
                $.get(
                    "/total-creditos-clientes", {
                        fecha: fecha,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#totalCreditoCliente').html(parseInt(data.recarga).toLocaleString());
                    totalCreditoCliente = data.recarga;
                    calcularToral2();
                    $("#totalSC").html("$" + parseInt(data.msg).toLocaleString());
                });
            }

            function calcularToral2() {
                var total = 0;
                var totalE = 0;
                var totalT = 0;
                var totalM = 0;
                var totalMM = 0;

                for (var item in REGISTROS) {
                    totalMM += parseInt(REGISTROS[item].precio);

                    if (REGISTROS[item].metodoP == "Efectivo") {
                        totalE += parseInt(REGISTROS[item].precio);
                        total += parseInt(REGISTROS[item].precio);
                    } else if (REGISTROS[item].metodoP == "Tarjeta") {
                        totalT += parseInt(REGISTROS[item].precio);
                        total += parseInt(REGISTROS[item].precio);
                    } else {
                        totalM += parseInt(REGISTROS[item].precio);
                        console.log(REGISTROS[item].precio)
                    }
                }
                total = totalCreditoCliente + total;
                $('#total').html(total.toLocaleString());
                $('#totalE').html(totalE.toLocaleString());
                $('#totalT').html(totalT.toLocaleString());
                $('#totalM').html(totalM.toLocaleString());
                $('#totalMM').html(totalMM.toLocaleString());
            }

            $("#select_dia").click(function () {
                FECHA = $("#fechaDia").data("date");
                TABLA.ajax.reload();
                // gerRegistros($("#fechaDia").data("date"));
            });

            $("#select_mes").click(function () {
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
                //gerRegistrosMes($("#fechaMes").data("date"));
            });
        });

        function calcularToral(total) {
            var valor = parseInt(total.value);
            $('#totalRegistroProduccion').html(valor.toLocaleString());
        }

        $('#guardarProduccion').on('click', function () {
            $("#produccion-error").html("");
            $("#produccion-error").hide();
            cargar(true, '#guardarProduccion');
            $.ajax({
                url: '/crear-produccion',
                type: 'POST',
                data: {
                    cliente_id: $('#select-cliente').val(),
                    servicio: $('#Pservicio').val(),
                    empleado_id: $('#select-usuarios').val(),
                    precio: $('#Pprecio').val(),
                    metodo_pago: $('#PmetodoPago').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                console.log("respuesta" + response);
                if (response.status == 'error') {
                    $("#produccion-error").html(response.msg);
                    $("#produccion-error").show();
                    cargar(false, '#guardarProduccion');
                    return;
                }
                notify('Produccion Registrada con Exito', 'success');
                $('#modal-2-produccion').modal('hide');
                $("#notaSaldo").html("");
                $("#produccion-error").html("");
                $('#form-produccion')[0].reset();
                TABLA.ajax.reload();
                cargar(false, '#guardarProduccion');
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#produccion-error").html(value[0]);
                    $("#produccion-error").show();
                    cargar(false, '#guardarProduccion');
                });
                cargar(false, '#guardarProduccion');
            });

        });

        function getProduccion() {
            $.get(
                "/produccion-tarjeta"
            ).done(function (data) {
                console.log(data);
                calcularToralTarjeta(data.msg);
            });

            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToralEfectivo(data.msg);
            });
        }

        function calcularToralEfectivo(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())
        }

        function calcularToralTarjeta(data) {
            var total = data.totalTarjeta + data.totalTarjetaEntrada - data.totalTarjetaSalida;
            TOTALDISPONIBLE = total;
            $('#producidoT').html("$" + total.toLocaleString())
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        function registrarProduccion() {
            $('#form-produccion')[0].reset();
            $('#guardarProduccion').show();
            $('#actualizarProduccion').hide();
            $('#select-usuarios').val("").trigger('change');
            $('#select-cliente').val("").trigger('change');
            $('#modal-2-produccion').modal('show');
        }

        $('#actualizarProduccion').on('click', function () {
            cargar(true, '#actualizarProduccion');
            $.ajax({
                url: '/editar-produccion',
                type: 'POST',
                data: {
                    id: $('#id').val(),
                    cliente_id: $('#select-cliente').val(),
                    servicio: $('#Pservicio').val(),
                    empleado_id: $('#select-usuarios').val(),
                    precio: $('#Pprecio').val(),
                    metodo_pago: $('#PmetodoPago').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                console.log("respuesta" + response);
                if (response.status == 'error') {
                    $("#produccion-error").html(response.msg);
                    $("#produccion-error").show();
                    cargar(false, '#actualizarProduccion');
                    return;
                }
                notify('Produccion Actualizada con Exito', 'success');
                $('#modal-2-produccion').modal('hide');
                $("#notaSaldo").html("");
                $("#produccion-error").html("");
                $('#form-produccion')[0].reset();
                TABLA.ajax.reload();
                cargar(false, '#actualizarProduccion');
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#produccion-error").html(value[0]);
                    $("#produccion-error").show();
                    cargar(false, '#actualizarProduccion');
                });
                cargar(false, '#actualizarProduccion');
            });

        });

        $("#actualizarProduccion").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        $("#guardarProduccion").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });
    </script>



@endsection
