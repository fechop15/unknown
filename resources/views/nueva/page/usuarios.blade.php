@extends('nueva.layout.Dashboard')
@section('page')

    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Empleados</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                            class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Empleados DentalStettic</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">

                    <div class="card user-profile-list animated fadeIn" id="vistaEmpleados">
                        <div class="card-header">
                            <h5>Empleados DentalStetic</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary" id="nuevoEmpleado">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-hover" id="tablaEmpleados" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Cedula</th>
                                        <th>Rol</th>
                                        <th>Telefono</th>
                                        <th>% Ganancia</th>
                                        <th>Sueldo</th>
                                        <th>Pago</th>
                                        <th>Estado</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaRegistroEmpleado" style="display: none">
                        <div class="card-header">
                            <h5>Registro De Empleado</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarUsuario">

                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-control-label">Nombre </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="text" class="form-control" id="nombre"
                                                   placeholder="Nombre De Usuario">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Identificacion </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="text" class="form-control" id="cedula"
                                                   placeholder="Cedula De Ciudadania">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Correo</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="email" class="form-control" id="email"
                                                   placeholder="Correo Electronico">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Telefono </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="number" class="form-control" id="telefono"
                                                   placeholder="Telefono">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Contraseña </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="text" class="form-control" id="password"
                                                   placeholder="Contraseña">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Sueldo Fijo</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="number" class="form-control" id="sueldo"
                                                   placeholder="Sueldo Fijo a Pagar">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label" for="single-select">Tipo Pago</label>
                                            <select id="tipoPago" class="form-control">
                                                <option value="Diario">Diario</option>
                                                <option value="Mensual">Mensual</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-control-label">Ganancia</label>
                                        <div class="input-group">
                                            <input type="number" id="ganancia" class="form-control"
                                                   placeholder="% de Ganancia"
                                                   aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label" for="single-select">Rol</label>
                                            <select id="rol" class="form-control">
                                                <option value="1">Administrador</option>
                                                <option value="2">Empleado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label" for="single-select">Estado</label>
                                            <select id="estado" class="form-control">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">
                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaActualizarEmpleado" style="display: none">
                        <div class="card-header">
                            <h5>Actualizar De Empleado</h5>
                        </div>
                        <div class="card-body">
                            <form id="actualizarUsuario">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label">Nombre</label>
                                            <input type="name" id="Anombre" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label">Cedula</label>
                                            <input type="number" id="Acedula" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label">Email</label>
                                            <input type="email" id="Aemail" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label">Telefono</label>
                                            <input type="number" id="Atelefono" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label">Sueldo Fijo</label>
                                            <input type="number" class="form-control" id="Asueldo">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label" for="single-select">Tipo Pago</label>
                                            <select id="AtipoPago" class="form-control">
                                                <option value="Diario">Diario</option>
                                                <option value="Mensual">Mensual</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-control-label">Ganancia</label>
                                        <div class="input-group">
                                            <input type="number" id="Aganancia" class="form-control"
                                                   placeholder="Percentage"
                                                   aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2">%</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label" for="single-select">Rol</label>
                                            <select id="Arol" class="form-control">
                                                <option value="1">Administrador</option>
                                                <option value="2">Empleado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-bottom: 0px;">
                                            <label class="form-control-label" for="single-select">Estado</label>
                                            <select id="Aestado" class="form-control">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="Aerror" style="display:none">
                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    <!-- [ Main Content ] end -->

    @include('nueva.modal.cambioPassword')
@endsection
@section('js')

    <script>
        var IDUSUARIO = 0;
        var STATES = [];

        var TABLA = $('#tablaEmpleados').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            "ajax": {
                "url": "/get-usuarios",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>');

                        var estadoYOpciones = estado + '<div class="overlay-edit">' + opciones(data.msg[item].id) + '</div>';

                        var usuario = '<div class="d-inline-block align-middle">' +
                            '<img src="/images/perfil/' + data.msg[item].imagen + '" alt="user image" class="img-radius align-top m-r-15" style="width:40px;">' +
                            '<div class="d-inline-block">' +
                            '<h6 class="m-b-0">' + data.msg[item].nombre + '</h6>' +
                            '<p class="m-b-0">' + data.msg[item].email + '</p>' +
                            '</div>' +
                            '</div>';
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombres: usuario,
                            Cedula: data.msg[item].cedula,
                            Correo: data.msg[item].email,
                            Rol: data.msg[item].rol,
                            Telefono: data.msg[item].telefono,
                            Ganancia: data.msg[item].ganancia,
                            Sueldo: data.msg[item].sueldo.toLocaleString(),
                            TipoPago: data.msg[item].tipoPago,
                            Estado: estadoYOpciones,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombres"},
                {data: "Cedula"},
                {data: "Rol"},
                {data: "Telefono"},
                {data: "Ganancia"},
                {data: "Sueldo"},
                {data: "TipoPago"},
                {data: "Estado"},
            ]
        });

        $('#nuevoEmpleado').on('click', function () {
            $('#vistaEmpleados').hide();
            $('#vistaRegistroEmpleado').show();
        });

        function cancelar() {
            $('#vistaEmpleados').show();
            $('#vistaRegistroEmpleado').hide();
            $('#vistaActualizarEmpleado').hide();
        }

        function opciones() {
            return '' +
                '<button type="button" class="btn btn-icon btn-warning actualizar">' +
                '        <i class="feather icon-edit"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-icon btn-info perfil">' +
                '        <i class="feather icon-eye"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-icon btn-success password">' +
                '        <i class="fas fa-key"></i>' +
                '</button>';
        }

        $('#guardar').on('click', function () {
            cargar(true, this);
            $.ajax({
                url: '/crear-usuarios',
                type: 'POST',
                data: {
                    nombre: $('#nombre').val(),
                    email: $('#email').val(),
                    cedula: $('#cedula').val(),
                    telefono: $('#telefono').val(),
                    password: $('#password').val(),
                    ganancia: $('#ganancia').val(),
                    sueldo: $('#sueldo').val(),
                    tipoPago: $('#tipoPago').val(),
                    rol_id: $('#rol').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {

                    TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                    notify('Empleado regustrado con exito', 'success');
                    cancelar();
                }
                cargar(false, '#guardarUsuario');

                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false, '#guardarUsuario');
                });

            });
        });

        $('#actualizar').on('click', function () {
            cargar(true, this);
            $("#Aerror").hide();
            console.log(IDUSUARIO);
            $.ajax({
                    url: '/actualizar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        nombre: $('#Anombre').val(),
                        email: $('#Aemail').val(),
                        cedula: $('#Acedula').val(),
                        telefono: $('#Atelefono').val(),
                        password: $('#Apassword').val(),
                        rol_id: $('#Arol').val(),
                        estado: $('#Aestado').val(),
                        ganancia: $('#Aganancia').val(),
                        sueldo: $('#Asueldo').val(),
                        tipoPago: $('#AtipoPago').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Empleado actualizado con exito', 'success');
                    cancelar();
                    $("#actualizarUsuario")[0].reset();
                    $("#Aerror").hide();
                }
                cargar(false, '#actualizarUsuario');
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();
                });
                cargar(false, '#actualizarUsuario');

            });
        });

        function cambiarPass() {
            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    id: IDUSUARIO,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else {
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

        TABLA.on('click', '.actualizar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDUSUARIO = data.Id;
            $("#actualizarUsuario")[0].reset();
            $('#vistaEmpleados').hide();
            $('#vistaActualizarEmpleado').show();
            cargar(true, '#actualizarUsuario');

            $.ajax({
                    url: '/buscar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Acedula').val(response.msg.cedula);
                $('#Aemail').val(response.msg.email);
                $('#Atelefono').val(response.msg.telefono);
                $('#Apassword').val(response.msg.password);
                $('#Arol').val(response.msg.rol_id);
                $('#Aganancia').val(response.msg.ganancia);
                $('#Aestado').val(response.msg.estado);
                $('#Asueldo').val(response.msg.sueldo);
                $('#AtipoPago').val(response.msg.tipoPago);
                cargar(false, '#actualizarUsuario');

                //$('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);
                cargar(false, '#actualizarUsuario');

            });
        });

        TABLA.on('click', '.perfil', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            window.location.href = "/dashboard/empleados/" + data.Id;
            //alert("Perfil");
        });

        TABLA.on('click', '.password', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDUSUARIO = data.Id;
            $('#modal-pass').modal();
            $("#form-password")[0].reset();
            $("#Perror").hide();
        });

    </script>


@endsection
