@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>{{$empleado->nombre}}</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('empleados')}}">Empleado</a></li>
                                <li class="breadcrumb-item"><a href="#!">Perfil</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-pills bg-white" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active text-uppercase" id="user1-tab" data-toggle="tab"
                                       href="#user1" role="tab" aria-controls="user1"
                                       aria-selected="true">Informacion</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user2-tab" data-toggle="tab" href="#user2"
                                       role="tab" aria-controls="user2" aria-selected="false">Historial</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user3-tab" data-toggle="tab" href="#user3"
                                       role="tab" aria-controls="user3" aria-selected="false">Pagos Realizados</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <!-- [ user card1 ] start -->
                        <div class="tab-pane fade show active" id="user1" role="tabpanel" aria-labelledby="user1-tab">
                            <div class="row mb-n4">
                                <div class="col-md-12">
                                    <div class="card bg-light">
                                        <div class="card-header">
                                            <h5>Informacion Personal</h5>

                                        </div>
                                        <div class="card-body">
                                            <div class="view-info">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="general-info">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-xl-6">
                                                                    <table class="table m-0">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th scope="row">Nombres Completos</th>
                                                                            <td>{{ $empleado->nombre}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Identificación</th>
                                                                            <td>{{ $empleado->cedula}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Telefono</th>
                                                                            <td>{{ $empleado->telefono}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Correo</th>
                                                                            <td>{{ $empleado->email}}</td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->

                                                                <div class="col-lg-12 col-xl-6">
                                                                    <table class="table">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th scope="row">Ganancia</th>
                                                                            <td>{{$empleado->ganancia}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Sueldo</th>
                                                                            <td> {{$empleado->sueldo}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th scope="row">Tipo de Pago</th>
                                                                            <td>{{$empleado->tipoPago}}</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <th scope="row">Estado</th>
                                                                            <td>{{$empleado->estado?'Activo':'Inactivo'}}</td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!-- end of table col-lg-6 -->
                                                            </div>
                                                            <!-- end of row -->
                                                        </div>
                                                        <!-- end of general info -->
                                                    </div>
                                                    <!-- end of col-lg-12 -->
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [ user card1 ] end -->
                        <!-- varient [ 2 ][ cover shape ] card Start -->
                        <div class="tab-pane fade" id="user2" role="tabpanel" aria-labelledby="user2-tab">
                            <div class="row mb-n4">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Produccion</h5>
                                            <a style="float: right;padding-right: 30px">Total Produccion: $
                                                <strong id="total">0</strong>
                                            </a>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablaHistorial"
                                                       style="width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Nombre Paciente</th>
                                                        <th>Servicio</th>
                                                        <th>Precio</th>
                                                        <th>Ganancia</th>
                                                        <th>Fecha Registro</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_dia">Dia</a>
                                                    <div class="slide"></div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#profile3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                                {{--<li class="nav-item">--}}
                                                {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                                {{--<div class="slide"></div>--}}
                                                {{--</li>--}}
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaDia"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMes"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                    <div class="container">
                                                        <br>
                                                        <div class='col-md-12'>
                                                            <div class="form-group">
                                                                <label for="">Desde</label>
                                                                <div class="input-group date input-group-date-custom">
                                                                    <input type="text" class="form-control">
                                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-12'>
                                                            <div class="form-group">
                                                                <label for="">Hasta</label>
                                                                <div class="input-group date input-group-date-custom">
                                                                    <input type="text" class="form-control">
                                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block waves-effect"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title=".btn-success .btn-block">Buscar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- varient [ 2 ][ cover shape ] card end -->
                        <!-- varient [ footer color ] card Start -->
                        <div class="tab-pane fade user-card" id="user3" role="tabpanel" aria-labelledby="user3-tab">
                            <div class="row mb-n4">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Movimientos de Pagos</h5>
                                            <a style="float: right;padding-right: 30px">
                                                <strong id="nota"></strong>
                                            </a>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablapagos" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Descripcion</th>
                                                        <th>Valor</th>
                                                        <th>Tipo</th>
                                                        <th>Mes Pagado</th>
                                                        <th>Fecha</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMesPagos"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- varient [ footer color ] card end -->
                        <!-- varient [ Profile ] card Start -->
                        <div class="tab-pane fade" id="user4" role="tabpanel" aria-labelledby="user4-tab">
                            <div class="row mb-n4">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Cursos Disponibles</h5>
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablaCursos"
                                                       width="100%">
                                                    <thead>

                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Id</th>
                                                        <th class="text-center txt-primary">Curso</th>
                                                        <th class="text-center txt-primary">Cupo</th>
                                                        <th class="text-center txt-primary">Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="text-center">

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- varient [ Profile ] card End -->
                    </div>
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>


@endsection
@section('js')

    <script>
        var IDCLIENTE = '{!! $empleado->id !!}';
        var FECHA = null;
        var FECHAH = null;
        var TABLA = null;
        var TABLA2 = null;
        var COMISION = '{!! $empleado->ganancia !!}';
        estadoCredito();

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-notas", {_token: $('meta[name="csrf-token"]').attr('content'), cliente: IDCLIENTE}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    // (data.msg[item], 'agregar');
                }

            });

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaMesPagos').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                FECHAH = $("#fechaDia").data("date");
                TABLA2.ajax.reload();

                //gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHAH = $("#fechaMes").data("date");
                TABLA2.ajax.reload();

                //gerRegistrosMes($("#fechaMes").data("date"));
            });

            $('#fechaMesPagos').on('dp.change', function (e) {
                FECHA = $("#fechaMesPagos").data("date");
                TABLA.ajax.reload();
                console.log($("#fechaMesPagos").data("date"));

            });

            FECHA = $("#fechaMesPagos").data("date");

            FECHAH = $("#fechaDia").data("date");

            TABLA = $('#tablapagos').DataTable({
                "ajax": {
                    "url": "/get-pagos-empleado",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.empleado = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Mes: data.msg[item].mes,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Mes"},
                    {data: "Fecha"},
                ],
            });

            TABLA2 = $('#tablaHistorial').DataTable({
                "ajax": {
                    "url": "/get-produccion-mes",
                    "data": function (d) {
                        d.fecha = FECHAH;
                        d.id = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "GET",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        var total = 0;
                        for (var item in data.msg) {
                            var ganancia = data.msg[item].precio * (COMISION / 100);
                            total += ganancia;
                            var itemJson = {
                                nombre: data.msg[item].nombre,
                                servicio: data.msg[item].servicio,
                                precio: '$' + parseInt(data.msg[item].precio).toLocaleString(),
                                ganancia: '$' + ganancia.toLocaleString(),
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson);
                        }
                        $('#total').html(total.toLocaleString())

                        return json;
                    }
                },
                columns: [
                    {data: "nombre"},
                    {data: "servicio"},
                    {data: "precio"},
                    {data: "ganancia"},
                    {data: "Fecha"},
                ],
            });

            function calcularToral() {
                var total = 0;

                for (var item in REGISTROS) {
                    total += parseInt(REGISTROS[item].precio);
                }

                total = total * (COMISION / 100);
                $('#total').html(total.toLocaleString())

            }

            $("#select_dia").click(function () {
                //gerRegistros($("#fechaDia").data("date"));
                FECHAH = $("#fechaDia").data("date");
                TABLA2.ajax.reload();
                console.log($("#fechaMesPagos").data("date"));
                // calcularToral();

            });

            $("#select_mes").click(function () {
                //gerRegistrosMes($("#fechaMes").data("date"));
                FECHAH = $("#fechaMes").data("date");
                TABLA2.ajax.reload();
                console.log($("#fechaMes").data("date"));
                //calcularToral();

            });
        });

        function estadoCredito() {
            $.post(
                "/get-estado-credito-empleado", {
                    empleado: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                var valor = data.avance - data.abono - data.descuento;
                if (valor > 0) {
                    $("#nota").html("Actualmente Debe: $" + valor.toLocaleString());
                } else {
                    $("#nota").html("Actualmente Debe: $0");
                }
            });
        }
    </script>


@endsection
