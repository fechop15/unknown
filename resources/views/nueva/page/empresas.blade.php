@extends('nueva.layout.Dashboard')
@section('page')

    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Empresas</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                            class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Empresas Mi Negocio</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">

                    <div class="card animated fadeIn" id="vistaEmpresa">
                        <div class="card-header">
                            <h5>Empresas DenTool</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary" id="nuevoEmpresa">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-hover" id="tablaEmpresa" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Cedula</th>
                                        <th>Estado</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaRegistroEmpleado" style="display: none">
                        <div class="card-header">
                            <h5>Registro Empresas Mi Negocio</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarUsuario">

                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-control-label">Nombre </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="text" class="form-control" id="nombre"
                                                   placeholder="Nombre De La Empresa">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-control-label">Representante </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="text" class="form-control" id="representante"
                                                   placeholder="Representante De La Empresa">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-control-label">NIT </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="text" class="form-control" id="nit"
                                                   placeholder="NIT De La Empresa">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-control-label">Numero de contacto </label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="number" class="form-control" id="contacto"
                                                   placeholder="Numero de Contacto De La Empresa">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-control-label">Cuota Mensual</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <input type="number" class="form-control" id="cuota"
                                                   placeholder="$0">
                                        </div>
                                    </div>

                                    <label id="error" class="has-error"></label>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    <!-- [ Main Content ] end -->

@endsection
@section('js')
    <style>
        .card .card-header .card-header-right {
            display: inline-block !important;
        }
    </style>
    <script>

        var IDUSUARIO = 0;
        var STATES = [];

        var TABLA = $('#tablaEmpresa').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            "ajax": {
                "url": "/get-empresas",
                "data": function (d) {
                    d._token = $('meta[name="csrf-token"]').attr('content');
                },
                "type": "POST",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].nombre,
                            Estado: data.msg[item].estado,
                            Pago: "",
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Pago"},
                {data: "Estado"},
            ],
        });

        $('#nuevoEmpresa').on('click', function () {
            $('#vistaEmpresa').hide();
            $('#vistaRegistroEmpleado').show();
            $('#vistaRegistroEmpleado').show();
        });

        function cancelar() {
            $('#vistaEmpresa').show();
            $('#vistaRegistroEmpleado').hide();
            $('.guardar').show();
            $('.actualizar').hide();
        }

        function opciones() {
            return '' +
                '<button type="button" class="btn btn-icon btn-warning actualizar">' +
                '        <i class="feather icon-edit"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-icon btn-info perfil">' +
                '        <i class="feather icon-eye"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-icon btn-success password">' +
                '        <i class="fas fa-key"></i>' +
                '</button>';
        }

        $('#guardar').on('click', function () {
            cargar(true, this);
            $.ajax({
                url: '/crear-empresa',
                type: 'POST',
                data: {
                    nombre: $('#nombre').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {

                    TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                    notify('Empresa Registrada con exito', 'success');
                    cancelar();
                }
                cargar(false, '#guardarUsuario');

                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false, '#guardarUsuario');
                });

            });
        });

        $('#actualizar').on('click', function () {
            cargar(true, this);
            $("#Aerror").hide();
            console.log(IDUSUARIO);
            $.ajax({
                    url: '/actualizar-empresa',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        nombre: $('#Anombre').val(),
                        email: $('#Aemail').val(),
                        cedula: $('#Acedula').val(),
                        telefono: $('#Atelefono').val(),
                        password: $('#Apassword').val(),
                        rol_id: $('#Arol').val(),
                        estado: $('#Aestado').val(),
                        ganancia: $('#Aganancia').val(),
                        sueldo: $('#Asueldo').val(),
                        tipoPago: $('#AtipoPago').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Empleado actualizado con exito', 'success');
                    cancelar();
                    $("#actualizarUsuario")[0].reset();
                    $("#Aerror").hide();
                }
                cargar(false, '#actualizarUsuario');
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();
                });
                cargar(false, '#actualizarUsuario');

            });
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else {
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

        TABLA.on('click', '.actualizar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDUSUARIO = data.Id;
            $("#actualizarUsuario")[0].reset();
            $('#vistaEmpresa').hide();
            $('#vistaActualizarEmpleado').show();
            cargar(true, '#actualizarUsuario');

            $.ajax({
                    url: '/buscar-empresa',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Acedula').val(response.msg.cedula);
                $('#Aemail').val(response.msg.email);
                $('#Atelefono').val(response.msg.telefono);
                $('#Apassword').val(response.msg.password);
                $('#Arol').val(response.msg.rol_id);
                $('#Aganancia').val(response.msg.ganancia);
                $('#Aestado').val(response.msg.estado);
                $('#Asueldo').val(response.msg.sueldo);
                $('#AtipoPago').val(response.msg.tipoPago);
                cargar(false, '#actualizarUsuario');

                //$('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error);
                cargar(false, '#actualizarUsuario');

            });
        });

        TABLA.on('click', '.perfil', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            window.location.href = "/dashboard/empleados/" + data.Id;
            //alert("Perfil");
        });

        TABLA.on('click', '.password', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDUSUARIO = data.Id;
            $('#modal-pass').modal();
            $("#form-password")[0].reset();
            $("#Perror").hide();
        });

    </script>


@endsection
