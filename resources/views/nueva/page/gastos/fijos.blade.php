@extends('nueva.layout.Dashboard')
@section('page')
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Gastos Fijos</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a>Gastos</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#"> Fijos (Mensuales)</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Gastos Fijos</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="abrirModal()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                            <a style="float: right;padding-right: 30px">Total: $
                                <strong id="total">0</strong>
                            </a>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover" id="tablaGastosFijos">
                                    <thead>
                                    <tr>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Fecha Creacion</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        <div class="card quater-card">
                            <div class="card-body">
                                <h5 class="mb-3">Produccion</h5>

                                <h6 class="text-muted m-b-15">Recaudado Virtual</h6>
                                <h4 id="producidoT">$0</h4>

                                <h6 class="text-muted m-b-15">Recaudado Efectivo del mes</h6>
                                <h4 id="producido">$0</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                            <div class="card-block">
                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                           id="select_mes">Mes</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaMes"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.gastos')

@endsection

@section('js')

    <script>
        var TOTALDISPONIBLE = 0;
        var opciones = [];
        var REGISTROS = [];
        var total = 0;
        var TABLA = 0;
        var FECHA;

        function abrirModal() {
            $("#tituloModal").html("Registrar Gasto Fijo");
            $('#modal-5').modal('show');
            $('#error').hide();
            $('#form-gastos')[0].reset();
            $("#guardarGasto").show();
            $('#totalAPagar').html(0);
        }

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                // gerRegistrosMes($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });

            //gerRegistrosMes($("#fechaMes").data("date"));
            FECHA = $("#fechaMes").data("date");
            TABLA = $('#tablaGastosFijos').DataTable({
                "ajax": {
                    "url": "/get-gasto-mes",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.razon = "Gasto Fijo";
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        REGISTROS = [];
                        total = 0;
                        console.log(data);
                        for (var item in data.msg) {
                            REGISTROS.push(data.msg[item]);
                            var itemJson = {
                                Id: data.msg[item].id,
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Fecha: data.msg[item].fecha,
                            };
                            total += parseInt(data.msg[item].valor);

                            json.push(itemJson)
                        }
                        getProduccion();
                        $('#total').html(total.toLocaleString());
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Fecha"},
                ],
            });

            $("#select_mes").click(function () {
                console.log($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });

        });

        function disponibilidad(total) {
            var valor = parseInt(total.value == '' ? 0 : total.value);

            $('#totalAPagar').html(valor.toLocaleString());
            if (TOTALDISPONIBLE >= total.value) {
                $("#guardarGasto").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardarGasto").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }
        }

        function calcularToral(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())
        }

        function getProduccion() {
            $.get(
                "/produccion-tarjeta"
            ).done(function (data) {
                console.log(data);
                calcularToralTarjeta(data.msg);
            });

            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });
        }

        function calcularToralTarjeta(data) {
            var total = data.totalTarjeta + data.totalTarjetaEntrada - data.totalTarjetaSalida;
            TOTALDISPONIBLE = total;
            $('#producidoT').html("$" + total.toLocaleString())
        }

        $('#guardarGasto').on('click', function () {
            cargar(true, '#guardarGasto');
            $("#error").hide();

            $.ajax({
                url: '/crear-gasto',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    valor: $('#valor').val(),
                    razon: "Gasto Fijo",
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                $('#descripcion').val("");
                $('#valor').val("")
                $('#modal-5').modal('hide');
                getProduccion();
                cargar(false, '#guardarGasto');
                notify('Gasto Registrado con Exito', 'success');

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false, '#guardarGasto');

            });
        });

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        $("#guardarGasto").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }
    </script>


@endsection
