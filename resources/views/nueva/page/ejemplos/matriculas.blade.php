@extends('nueva.layout.Dashboard')
@section('page')
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Matriculacion</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                                class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Matriculas de estudiantes</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">
                    <div class="card" id="vistaMatricula">
                        <div class="card-header">
                            <h5>Matriculacion</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary"
                                            data-toggle="modal"
                                            data-target="#modal-1">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Nueva
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-striped table-bordered nowrap" id="tablaInscripciones" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Cedula</th>
                                        <th>Ref</th>
                                        <th>Curso</th>
                                        <th>Pagado</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.matricula')

@endsection

@section('js')

    <script>

        var USUARIOS = [];
        var CURSOS = [];
        var IDINSCRIPCION = 0;

        var TABLA = $('#tablaInscripciones').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            "ajax": {
                "url": "/get-inscripciones",
                "type": "GET",
                "dataSrc": function (data) {
                    console.log(data);
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>');
                        var pago = (data.msg[item].pago == 0 ? '<span class="badge badge-danger">Pendiente</span>' : '<span class="badge badge-success">Pago</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Cedula: data.msg[item].persona.documento,
                            RefCurso: data.msg[item].curso.id,
                            Curso: data.msg[item].curso.nombre + "</br> " + data.msg[item].curso.nivel + "</br>" +
                                data.msg[item].curso.fecha_inicio_curso + " a " + data.msg[item].curso.fecha_fin_curso,
                            Pagado: pago,
                            Estado: estado,
                            Fecha: data.msg[item].fecha,
                            Opciones: opciones(data.msg[item].id, data.msg[item].estado, data.msg[item].curso.id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            order: [[0, "desc"]],
            columns: [
                {data: "Id"},
                {data: "Cedula"},
                {data: "RefCurso"},
                {data: "Curso"},
                {data: "Pagado"},
                {data: "Estado"},
                {data: "Fecha"},
                {data: "Opciones"}
            ]
        });

        function opciones(id, estado, curso_id) {
            var opciones = '';
            if ('{!! auth()->user()->rol->nombre !!}' == "Administrador") {
                opciones += '<button type="button" class="btn btn-sm btn-primary actualizar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                    '           onclick="actualizarModal(' + id + ')">\n' +
                    '           <i class="feather icon-edit"></i>\n' +
                    ' </button>';
            }
            opciones += '<button type="button" class="btn btn-sm btn-success" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver curso" data-original-title="Edit"' +
                '           onclick="verCurso(' + curso_id + ')">\n' +
                '           <i class="feather icon-eye"></i>\n' +
                '</button>';

            return opciones;
        }

        cursos();


        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Estudiante'}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].persona.id,
                        text: data.msg[item].cedula + " - " + data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-persona').select2({
                    dropdownParent: $("#modal-1"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Estudiante",
                    data: USUARIOS
                });
                $("#select-persona").val("").trigger('change');
            });


        });

        function cursos() {
            CURSOS = [];
            $.post(
                "/get-cursos-matricula", {_token: $('meta[name="csrf-token"]').attr('content')}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].matriculados + "/" + data.msg[item].cupos +
                            " - " + data.msg[item].nombre + " " + data.msg[item].nivel + " - " +
                            data.msg[item].fecha_inicio_curso + " a " + data.msg[item].fecha_fin_curso,
                    };
                    CURSOS.push(itemSelect2)
                }
                $('#select-curso').select2({
                    dropdownParent: $("#modal-1"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Cursos Disponibles",
                    data: CURSOS
                });
                $("#select-curso").val("").trigger('change');
            });
        }

        function actualizarModal(id) {
            $('#modal-actualizar').modal();
            $('#cargaActualizar').show();
            $('#actualizarUsuario').hide();

            IDINSCRIPCION = id;
            $.ajax({
                    url: '/buscar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Adocumento').val(response.msg.documento);
                $('#Atelefono').val(response.msg.telefono);
                $('#Acorreo').val(response.msg.correo);
                $('#AfechaInicio').val(response.msg.fecha_inicio);
                $('#AfechaFin').val(response.msg.fecha_fin);
                $('#Acurso').val(response.msg.curso);
                $('#Adocumentado').val(response.msg.documentado);
                $('#Apago').val(response.msg.pago);
                $('#estado').val(response.msg.estado);

                if (response.msg.documentado == 1) {
                    $("#Adocumentado").attr("disabled", true);
                } else {
                    $("#Adocumentado").attr("disabled", false);
                }

                if (response.msg.pago == 1) {
                    $("#Apago").attr("disabled", true);
                } else {
                    $("#Apago").attr("disabled", false);
                }

                $('#cargaActualizar').hide();
                $('#actualizarUsuario').show();

                return response;

            }).fail(function (error) {

                console.log(error);
                notify('Error del servidor','danger');
                $('#modal-actualizar').modal('hide');
                $('#cargaActualizar').hide();
                //cargar(false,'#actualizarUsuario');

            });
        }

        function informacionModal(id) {

            $.ajax({
                    url: '/buscar-inscripcion',
                    type: 'POST',
                    data: {
                        id: id,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Inombre').val(response.msg.nombre);
                $('#Idocumento').val(response.msg.documento);
                $('#Itelefono').val(response.msg.telefono);
                $('#Icorreo').val(response.msg.correo);
                $('#IfechaInicio').val(response.msg.fecha_inicio);
                $('#IfechaFin').val(response.msg.fecha_fin);
                $('#Icurso').val(response.msg.curso);
                $('#Idocumentado').val(response.msg.documentado);
                $('#Ipago').val(response.msg.pago);
                $('#Iestado').val(response.msg.estado);

                $('#modal-informacion').modal();
                return response;

            }).fail(function (error) {
                console.log(error);
            });

        }

        function verCurso(id) {
            window.location.href = "/dashboard/curso/" + id;
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        $(".btn-guardar").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        $(".btn-actualizar").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        $(".btn-guardar").on("click", function () {
            var d = $(this);
            d.children(".spinner-border").show();
            d.children(".spinner-grow").show();
            d.children(".load-text").show();
            d.children(".btn-text").hide();
            d.attr("disabled", "true");
            $.ajax({
                url: '/crear-inscripcion',
                type: 'POST',
                data: {
                    documentado: $('#documentado').val(),
                    pago: $('#pago').val(),
                    persona_id: $('#select-persona').val(),
                    curso_id: $('#select-curso').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    $('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#Aerror").hide();
                    cursos();
                    notify('Matricula creada con exito','success');
                }
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled")

                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();
                });
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled")
            });

        });

        $(".btn-actualizar").on("click", function () {
            var d = $(this);
            d.children(".spinner-border").show();
            d.children(".spinner-grow").show();
            d.children(".load-text").show();
            d.children(".btn-text").hide();
            d.attr("disabled", "true");
            $.ajax({
                    url: '/actualizar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        documentado: $('#Adocumentado').val(),
                        pago: $('#Apago').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                TABLA.ajax.reload();
                $('#modal-actualizar').modal('hide');
                $("#actualizarUsuario")[0].reset();
                cursos();

                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
                notify('Matricula actualizada con exito', 'success');
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled")
            });
        });
    </script>


@endsection