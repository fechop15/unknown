<!DOCTYPE html>
<html lang="es">

<head>

    <title>Gospel App</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content=""/>
    <meta name="keywords" content="">
    <meta name="author" content="GatewayTI"/>
    <!-- Favicon icon -->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="/assets/css/style.css">


</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
    <div class="auth-content">
        <div class="card">
            <div class="row align-items-center text-center">
                <div class="col-md-12">
                    <div class="card-body">
                        <img src="/images/logo-bajo.png" alt="" class="img-fluid mb-4">
                        <h4 class="mb-3 f-w-400">Consulta de certificados</h4>
                        @if(isset($inscripcion))
                            <div class="form-group" id="confirmacion">
                                <h6 id="nombre">Nombre: {{$inscripcion->persona->nombres}} {{$inscripcion->persona->apellidos}}
                                    <br>
                                    Documento: {{$inscripcion->persona->documento}}
                                </h6>
                                <div style="color:black">
                                    <table class="table table-striped">
                                        <thead class="alert-default">
                                        <tr>
                                            <th scope="col">Curso</th>
                                            <th scope="col">Expedicion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{$inscripcion->curso->nombre}} {{$inscripcion->curso->nivel}}</td>
                                            <td>{{$inscripcion->fecha_certificacion}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <form action="/consulta_externa">
                                <input type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20"
                                       value="Regresar" />
                            </form>
                        @else
                            <div class="form-group" id="confirmacion" style="display:none;">
                                <h6 id="nombre"></h6>
                                <div style="color:black">
                                    <table class="table table-striped">
                                        <thead class="alert-default">
                                        <tr>
                                            <th scope="col">Curso</th>
                                            <th scope="col">Expedicion</th>
                                            <th scope="col">Cod</th>
                                        </tr>
                                        </thead>
                                        <tbody id="cursos">
                                        <tr>
                                            <th>Ninguno</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" onclick="regresar()"
                                        class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                    Regresar
                                </button>
                            </div>
                            <form class="md-float-material" id="form-preinscripcion">

                                <div class="form-group">
                                    <label class="col-form-label">Numero Documento:</label>
                                    <input type="number" class="form-control" id="documento" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <div class="alert alert-danger" id="error" style="display: none">
                                    </div>
                                </div>

                                <!-- <div class="card-footer"> -->
                                <!-- </div> -->
                            </form>
                            <div class="row">
                                <div class="col-xs-10 offset-xs-1">
                                    <h6>Se encuentran disponibles los certificados emitidos a partir del 22 de Marzo de 2019</h6>

                                    <br>

                                    <button id="btn-registrar" type="button" onclick="consultar()"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                        Consultar
                                    </button>

                                </div>
                            </div>
                            <!-- end of form -->
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="/assets/js/vendor-all.min.js"></script>
<script src="/assets/js/plugins/bootstrap.min.js"></script>
<script src="/assets/js/ripple.js"></script>
<script src="/assets/js/pcoded.min.js"></script>
<script>

    function consultar() {

        $("#error").hide();
        $("#confirmacion").hide();

        $.ajax({
            url: '/consultar',
            type: 'POST',
            data: {
                documento: $('#documento').val(),
                _token: $('meta[name="csrf-token"]').attr('content')
            },

        }).done(function (response) {
            console.log(response);
            if (response.status == "Error") {
                $("#error").html(response.msg);
                $("#error").show();
            } else {
                $("#form-preinscripcion").hide();
                $("#confirmacion").show();
                $('#cursos').html("");

                $("#nombre").html("Nombre: " + response.nombre + "<br>Documento: " + $('#documento').val());
                if (response.msg==''){
                    $('#cursos').append('' +
                        '<tr>' +
                        '      <td colspan="3" valign="middle">Ningun Certificado</td>' +
                        '</tr>');
                }
                else{
                    jQuery.each(response.msg, function (i, val) {
                        $('#cursos').append('' +
                            '<tr>' +
                            '      <td>' + val.curso + '</td>' +
                            '      <td>' + val.fecha_certificacion + '</td>' +
                            '      <td>' + val.cod + '</td>' +
                            '</tr>');
                    });
                }

                $("#btn-registrar").hide();
                $("#error").hide();
            }
            //return response;
        }).fail(function (error) {

            console.log(error);
            var obj = error.responseJSON.errors;
            Object.entries(obj).forEach(([key, value]) => {
                $("#error").html(value[0]);
                $("#error").show();
            });

        });

    }

    function regresar() {
        $("#form-preinscripcion").show();
        $("#confirmacion").hide();
        $("#error").hide();
        $("#btn-registrar").show();

    }
</script>

</body>

</html>
