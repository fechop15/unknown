@extends('nueva.layout.Dashboard')
@section('page')
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">{{$curso->id}} - {{$curso->nombre}} - {{$curso->nivel}}</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="{{route('cursos')}}">Cursos</a></li>
                                <li class="breadcrumb-item"><a href="#!">{{$curso->nivel}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- statustic with progressbar  start -->
                <div class="col-xl-3 col-md-6">
                    <div class="card statustic-progress-card">
                        <div class="card-body">
                            <h5 class="mb-3">Finalizados</h5>
                            <div class="row align-items-center">
                                <div class="col">
                                    <label class="badge badge-light-success">
                                        {{round($curso->matriculados->where('estado',true)->where('fecha_certificacion','!=',null)->count()*100/$curso->matriculados->where('estado',true)->count())}}%<i
                                                class="m-l-10 feather icon-arrow-up"></i></label>
                                </div>
                                <div class="col text-right">
                                    <h5 class="">{{$curso->matriculados->where('estado',true)->where('fecha_certificacion','!=',null)->count()}} / {{$curso->matriculados->where('estado',true)->count()}}</h5>
                                </div>
                            </div>
                            <div class="progress m-t-5">
                                <div class="progress-bar bg-c-green" style="width:{{round($curso->matriculados->where('estado',true)->where('fecha_certificacion','!=',null)->count()*100/$curso->matriculados->where('estado',true)->count())}}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card statustic-progress-card">
                        <div class="card-body">
                            <h5 class="mb-3">Reportados al Minist.</h5>
                            <div class="row align-items-center">
                                <div class="col">
                                    <label class="badge badge-light-danger">
                                        {{round($curso->matriculados->where('estado',true)->where('fecha_reporte_ministerio','!=',null)->count()*100/$curso->matriculados->where('estado',true)->count())}}%
                                        <i class="m-l-10 feather icon-arrow-up"></i></label>
                                </div>
                                <div class="col text-right">
                                    <h5 class="">{{$curso->matriculados->where('estado',true)->where('fecha_reporte_ministerio','!=',null)->count()}} / {{$curso->matriculados->where('estado',true)->count()}}</h5>
                                </div>
                            </div>
                            <div class="progress m-t-5">
                                <div class="progress-bar bg-c-red" style="width:{{round($curso->matriculados->where('estado',true)->where('fecha_reporte_ministerio','!=',null)->count()*100/$curso->matriculados->where('estado',true)->count())}}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card statustic-progress-card">
                        <div class="card-body">
                            <h5 class="mb-3">Pagos</h5>
                            <div class="row align-items-center">
                                <div class="col">
                                    <label class="badge badge-light-primary">
                                        {{round($curso->matriculados->where('estado',true)->where('pago',true)->count()*100/$curso->matriculados->where('estado',true)->count())}}%
                                        <i class="m-l-10 feather icon-arrow-up"></i></label>
                                </div>
                                <div class="col text-right">
                                    <h5 class="">{{$curso->matriculados->where('estado',true)->where('pago',true)->count()}} / {{$curso->matriculados->where('estado',true)->count()}}</h5>
                                </div>
                            </div>
                            <div class="progress m-t-5">
                                <div class="progress-bar bg-c-blue" style="width:{{round($curso->matriculados->where('estado',true)->where('pago',true)->count()*100/$curso->matriculados->where('estado',true)->count())}}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card statustic-progress-card">
                        <div class="card-body">
                            <h5 class="mb-3">Documentados</h5>
                            <div class="row align-items-center">
                                <div class="col">
                                    <label class="badge badge-light-warning">
                                        {{round($curso->matriculados->where('estado',true)->where('documentado',true)->count()*100/$curso->matriculados->where('estado',true)->count())}}%
                                        <i class="m-l-10 feather icon-arrow-up"></i></label>
                                </div>
                                <div class="col text-right">
                                    <h5 class="">{{$curso->matriculados->where('estado',true)->where('documentado',true)->count()}} / {{$curso->matriculados->where('estado',true)->count()}}</h5>
                                </div>
                            </div>
                            <div class="progress m-t-5">
                                <div class="progress-bar bg-c-yellow" style="width:{{round($curso->matriculados->where('estado',true)->where('documentado',true)->count()*100/$curso->matriculados->where('estado',true)->count())}}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- statustic with progressbar  end -->

                <!-- progressbar static data start -->
                <div class="col-md-12">
                    <div class="card card table-card">
                        <div class="card-body">
                            <ul class="nav nav-tabs px-3 pt-3" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active text-uppercase" id="home-tab" data-toggle="tab"
                                       href="#home" role="tab" aria-controls="home" aria-selected="true">Informacion</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="profile-tab" data-toggle="tab"
                                       href="#profile" role="tab" aria-controls="profile" aria-selected="false">Matriculados</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th>Nombre</th>
                                                                    <td>{{$curso->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nivel</th>
                                                                    <td>{{$curso->nivel}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Intensidad</th>
                                                                    <td>{{$curso->intensidad}} Horas</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Cupos</th>
                                                                    <td>{{$curso->matriculados->where("estado","=",true)->count()}}
                                                                        /{{$curso->cupos}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Entrenador</th>
                                                                    <td>{{$curso->entrenador->nombres }} {{$curso->entrenador->apellidos }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Dias</th>
                                                                    <td>{{$curso->dias}}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th>Fecha Fin Inscripcion</th>
                                                                    <td>{{$curso->fecha_fin_inscripciones}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Inicio del Curso</th>
                                                                    <td> {{$curso->fecha_inicio_curso}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Fin del Curso</th>
                                                                    <td>{{$curso->fecha_fin_curso}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Estado</th>
                                                                    <td>{{($curso->estado ?"Activo":"Inactivo") }}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive" style="padding-left: 10px; padding-right: 10px;">
                                                <table class="table" id="tablaEstudiantesCurso">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center txt-primary">Nombres</th>
                                                        <th class="text-center txt-primary">Documento</th>
                                                        <th class="text-center txt-primary">Telefono</th>
                                                        <th class="text-center txt-primary">Nota</th>
                                                        <th class="text-center txt-primary">Pago</th>
                                                        <th class="text-center txt-primary">Documentado</th>
                                                        <th class="text-center txt-primary">Opciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($curso->matriculados->reverse() as $matricula)
                                                        @if($matricula->estado)
                                                            <tr>
                                                                <td>{{$matricula->persona->nombres}} {{$matricula->persona->apellidos}}</td>
                                                                <td class="text-center">{{$matricula->persona->documento}}</td>
                                                                <td class="text-center">{{$matricula->persona->telefono}}</td>
                                                                <td class="text-center">{{($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6)}}</td>
                                                                <td class="text-center" id="pagos_{{$matricula->id}}">
                                                                    @if($matricula->pago)
                                                                        <span class="badge badge-success">Pagado</span>
                                                                    @else
                                                                        <span class="badge badge-danger">Pendiente</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center"
                                                                    id="documentos_{{$matricula->id}}">
                                                                    @if($matricula->documentado)
                                                                        <span class="badge badge-success">Si</span>
                                                                    @else
                                                                        <span class="badge badge-danger">Pendiente</span>
                                                                    @endif
                                                                </td>
                                                                <td class="text-center" id="op_button_{{$matricula->id}}">
                                                                    @if($matricula->asistencia_validada==0 && (auth()->user()->rol_id<=3))
                                                                        <button id="asistencia_{{$matricula->id}}"
                                                                                type="button"
                                                                                class="btn btn-success btn-sm"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Confirmar Asistencia"
                                                                                data-original-title="Edit"
                                                                                onclick="asistencia('{{$matricula->id}}')">
                                                                            <i class="feather icon-check-circle"></i>
                                                                        </button>
                                                                    @endif
                                                                    @if(($matricula->nota_teorica==null || $matricula->nota_practica==null) && (auth()->user()->rol_id<=3))
                                                                        <button id="calificacion_{{$matricula->id}}"
                                                                                type="button"
                                                                                class="btn btn-info btn-sm"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Ingresar Calificaciones"
                                                                                data-original-title="Edit"
                                                                                onclick="notaCertificado('{{$matricula->id}}')">
                                                                            <i class="feather icon-box"></i>
                                                                        </button>
                                                                    @endif
                                                                    @if(
                                                                    (($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6))>=70 &&
                                                                    ($matricula->asistencia_validada!=0)
                                                                    )
                                                                        @if($matricula->fecha_certificacion!=null)
                                                                            <button type="button"
                                                                                    class="btn btn-info btn-sm"
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    title="Descargar Certificados"
                                                                                    data-original-title="Edit"
                                                                                    onclick="descargarCertificado('{{$matricula->id}}')">
                                                                                <i class="feather icon-download"></i>
                                                                            </button>
                                                                            @if($matricula->fecha_reporte_ministerio==null)
                                                                                <button type="button"
                                                                                        class="btn btn-danger btn-sm"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="top"
                                                                                        title="Reportar Al Ministerio"
                                                                                        data-original-title="Edit"
                                                                                        onclick="reporarMinisterio('{{$matricula->id}}')">
                                                                                    <i class="fas fa-certificate"></i>
                                                                                </button>
                                                                            @endif
                                                                        @else
                                                                            @if(auth()->user()->rol_id==1)
                                                                                <button id="reportar_{{$matricula->id}}"
                                                                                        type="button"
                                                                                        class="btn btn-warning btn-sm"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="top"
                                                                                        title="Generar Certificado"
                                                                                        data-original-title="Edit"
                                                                                        onclick="generarCertificado('{{$matricula->id}}')">
                                                                                    <i class="feather icon-external-link"></i>
                                                                                </button>
                                                                            @endif
                                                                        @endif

                                                                    @endif

                                                                    @if(
                                                                    (($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6))<70 &&
                                                                    ($matricula->nota_teorica*0.4)+($matricula->nota_practica*0.6)!=0)

                                                                        <span class="badge badge-danger">Curso No Superado</span>

                                                                        <button id="calificacion_{{$matricula->id}}"
                                                                                type="button"
                                                                                class="btn btn-info btn-sm"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Modificar Nota"
                                                                                data-original-title="Edit"
                                                                                onclick="actualizarNotas('{{$matricula->id}}','{{$matricula->nota_teorica}}', '{{$matricula->nota_practica}}')">
                                                                            <i class="feather icon-box"></i>
                                                                        </button>

                                                                    @endif
                                                                    @if(auth()->user()->rol_id==1||auth()->user()->rol_id==2)

                                                                        <button type="button"
                                                                                class="btn btn-primary btn-sm actualizar"
                                                                                data-toggle="tooltip"
                                                                                data-placement="top"
                                                                                title="Editar Estudiante"
                                                                                data-original-title="Edit"
                                                                                onclick="actualizarModal('{{$matricula->id}}')">
                                                                            <i class="feather icon-edit-2"></i>
                                                                        </button>

                                                                    @endif

                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- progressbar static data end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.curso_gestion')

@endsection

@section('js')

    <script>
        var IDCURSO = '{!! $curso->id !!}';
        var IDMATRICULA = 0;
        var TABLA = $('#tablaEstudiantesCurso').DataTable();

        /---------------------------------/

        function generarCertificado(id) {
            IDMATRICULA = id;
            $('#modal-certificado').modal();
        }

        function reporarMinisterio(id) {
            IDMATRICULA = id;
            $('#modal-reportar').modal();
        }

        function notaCertificado(id) {
            $('#modal-calificacion').modal();
            $("#calificacion")[0].reset();
            $('#notaFinal').html("0");
            IDMATRICULA = id;
        }

        function actualizarNotas(id, teorica, practica) {

            $('#modal-calificacion').modal();
            $("#calificacion")[0].reset();
            $('#notaFinal').html("0");
            $('#nota_teorica').val(teorica).trigger('keyup');
            $('#nota_practica').val(practica).trigger('keyup');
            IDMATRICULA = id;
        }

        function asistencia(id) {
            $('#modal-asistencia').modal();
            $("#asistencia")[0].reset();
            IDMATRICULA = id;
        }

        function actualizarModal(id) {
            $('#modal-actualizar').modal();
            $('#cargaActualizar').show();
            $('#actualizarUsuario').hide();

            IDINSCRIPCION = id;
            $.ajax({
                    url: '/buscar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Adocumento').val(response.msg.documento);
                $('#Atelefono').val(response.msg.telefono);
                $('#Acorreo').val(response.msg.correo);
                $('#AfechaInicio').val(response.msg.fecha_inicio);
                $('#AfechaFin').val(response.msg.fecha_fin);
                $('#Acurso').val(response.msg.curso);
                $('#Adocumentado').val(response.msg.documentado);
                $('#Apago').val(response.msg.pago);
                $('#estado').val(response.msg.estado);

                if (response.msg.documentado == 1) {
                    $("#Adocumentado").attr("disabled", true);
                } else {
                    $("#Adocumentado").attr("disabled", false);
                }

                if (response.msg.pago == 1) {
                    $("#Apago").attr("disabled", true);
                } else {
                    $("#Apago").attr("disabled", false);
                }

                $('#cargaActualizar').hide();
                $('#actualizarUsuario').show();

                return response;

            }).fail(function (error) {

                console.log(error);
                notify('Error del servidor','danger');
                $('#modal-actualizar').modal('hide');
                $('#cargaActualizar').hide();
                //cargar(false,'#actualizarUsuario');

            });
        }

        /---------------------------------/

        function calcularNota() {
            var nota1 = (parseInt($('#nota_teorica').val()) * 0.4);
            var nota2 = (parseInt($('#nota_practica').val()) * 0.6);
            $('#notaFinal').html(nota1 + nota2);
        }

        function calificar() {
            $.ajax({
                    url: '/calificar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        nota_practica: $('#nota_practica').val(),
                        nota_teorica: $('#nota_teorica').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Cerror").html(response.msg);
                    $("#Cerror").show();
                } else {
                    $("#calificacion_" + IDMATRICULA).remove();
                    $('#modal-calificacion').modal('hide');
                    $("#Cerror").hide();
                    location.reload();
                }
//return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Cerror").html(value[0]);
                    $("#Cerror").show();

                });
            });
        }

        function certificar() {
            $.ajax({
                    url: '/certificar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Ceerror").html(response.msg);
                    $("#Ceerror").show();
                } else {
                    $("#certificado_" + IDMATRICULA).remove();
                    $("#op_button_"+IDMATRICULA).html("<span class='label label-success m-t-20'>Completado</span>");
                    $('#modal-certificado').modal('hide');
                    $("#Ceerror").hide();
                }

//return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Ceerror").html(value[0]);
                    $("#Ceerror").show();

                });
            });
        }

        function descargarCertificado(id) {

            window.open("/descargar_certificado/" + id);

        }

        function confirmarAsistencia() {
            $.ajax({
                    url: '/confirmarAsistencia',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        asistencia_validada: $('#asistio').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    $("#asistencia_" + IDMATRICULA).remove();
                    $('#modal-asistencia').modal('hide');
                    $("#Aerror").hide();
                }

//return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();

                });
            });
        }

        function reportar() {
            $.ajax({
                    url: '/reportar',
                    type: 'POST',
                    data: {
                        id: IDMATRICULA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Merror").html(response.msg);
                    $("#Merror").show();
                } else {
                    $("#reportar_" + IDMATRICULA).remove();
                    $('#modal-reportar').modal('hide');
                    $("#Merror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Merror").html(value[0]);
                    $("#Merror").show();

                });
            });
        }

        function actualizar() {
            var d = $('#actualizarUsuario');
            d.children(".spinner-border").show();
            d.children(".spinner-grow").show();
            d.children(".load-text").show();
            d.children(".btn-text").hide();
            d.attr("disabled", "true");
            $.ajax({
                    url: '/actualizar-inscripcion',
                    type: 'POST',
                    data: {
                        id: IDINSCRIPCION,
                        documentado: $('#Adocumentado').val(),
                        pago: $('#Apago').val(),
                        estado: $('#estado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                //TABLA.ajax.reload();
                $('#modal-actualizar').modal('hide');
                $("#actualizarUsuario")[0].reset();
                if ($('#Apago').val() == 1) {
                    $("#pago_" + IDINSCRIPCION).html('<span class="badge badge-success">Pagado</span>');
                } else {
                    $("#pago_" + IDINSCRIPCION).html('<span class="badge badge-danger">Pendiente</span>\n');
                }

                if ($('#Adocumentado').val() == 1) {
                    $("#documentos_" + IDINSCRIPCION).html('<span class="badge badge-success">Si</span>');
                } else {
                    $("#documentos_" + IDINSCRIPCION).html('<span class="badge badge-danger">Pendiente</span>\n');
                }
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
                //cursos();
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled")
            });
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        $(".btn-actualizar").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });
    </script>


@endsection
