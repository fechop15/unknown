@extends('nueva.layout.Dashboard')
@section('page')
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Noticias</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                                class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Historial de noticias</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">
                    <div class="card" id="vistaNoticia">
                        <div class="card-header">
                            <h5>Noticias</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary" onclick="modalGuardar()">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-hover" id="tablaNoticias"
                                       style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>Titulo</th>
                                        <th>Tipo</th>
                                        <th>Autor</th>
                                        <th>Fecha</th>
                                        <th>Leidos</th>
                                        <th>Like</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card" id="noticia" style="display: none">
                        <div class="card-header">
                            <h5>Registro De Noticia</h5>
                        </div>
                        <div class="card-body" style="padding-top: 0px;">
                            <form id="guardarUsuario">
                                <input type="hidden" id="idNoticia">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group fill">
                                            <label>Titulo</label>
                                            <input type="text" id="titulo" class="form-control" placeholder="titulo">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label for="tipoNoticia">Tipo</label>
                                            <select class="form-control" id="tipoNoticia">
                                                <option value="Predica">Predica</option>
                                                <option value="Evento">Evento</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label for="tipoNoticia">Estado</label>
                                            <select class="form-control" id="estado">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="padding-top: 10px;">
                                        <div class="form-group fill" id="editor">
                                            <textarea id='edit' style="margin-top: 30px;"
                                                      placeholder="Contenido de la noticia"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:flex">

                                        </div>
                                    </div>
                                </div>
                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    <!-- [ Main Content ] end -->

@endsection
@section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <style>
        table {

            overflow-x:auto;
        }
        table td {
            word-wrap: break-word;
            max-width: 400px;
        }
        #grid td {
            white-space:inherit;
        }
    </style>
    <script>

        var IDUSUARIO = 0;
        var evento = "guardar";
        var TABLA = $('#tablaNoticias').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            "ajax": {
                "url": "/get-noticias",
                "type": "GET",
                "dataSrc": function (data) {
                    console.log(data.noticias);
                    var json = [];
                    for (var item in data.noticias) {
                        var estado = (data.noticias[item].estado === 0 ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>');

                        var itemJson = {
                            Id: data.noticias[item].id,
                            Titulo: data.noticias[item].titulo,
                            Tipo: data.noticias[item].tipo,
                            Autor: data.noticias[item].autor.nombres,
                            Fecha: data.noticias[item].fecha,
                            Leidos: data.noticias[item].vistos,
                            Gustados: data.noticias[item].like,
                            Contenido: data.noticias[item].contenido,
                            estado: data.noticias[item].estado,
                            Estado: estado,
                            Opciones: opciones()
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Titulo"},
                {data: "Tipo"},
                {data: "Autor"},
                {data: "Fecha"},
                {data: "Leidos"},
                {data: "Gustados"},
                {data: "Estado"},
                {data: "Opciones"}
            ]
        });

        $("#error").hide();

        function opciones() {
            return '' +
                '<button type="button" class="btn btn-sm btn-primary editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit">' +
                '           <i class="feather icon-edit"></i>\n' +
                ' </button>'+
                '<button type="button" class="btn btn-sm btn-danger eliminar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                '           <i class="feather icon-trash-2"></i>\n' +
                ' </button>';
        }

        function modalGuardar() {
            evento = 'guardar';
            $('#guardarUsuario')[0].reset();
            $('#actualizar').hide();
            $('#guardar').show();
            //$("#modal-1").modal('show');
            $('#noticia').show();
            $('#vistaNoticia').hide();
            $('#edit').summernote('destroy');
            $('#edit').html('');
            $('#edit').summernote({height: 100});


        }

        $('#guardar').on('click', function () {
            console.log('evento');
            cargar(true,this);
            $.ajax({
                url: '/crear-noticia',
                type: 'POST',
                data: {
                    titulo: $('#titulo').val().toUpperCase(),
                    tipo: $('#tipoNoticia').val(),
                    contenido: $('#edit').summernote('code')
                    ,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                //console.log(response);
                if (!response.success) {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    cargar(false,'#guardarUsuario');
                    cancelar();
                    notify('Noticia Registrada', 'success');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false,'#guardarUsuario');


            });
        });

        $('#actualizar').on('click', function () {
            console.log("actualizando..");
            cargar(true,this);
            $.ajax({
                    url: '/actualizar-noticia',
                    type: 'POST',
                    data: {
                        id: $('#idNoticia').val(),
                        titulo: $('#titulo').val().toUpperCase(),
                        tipo: $('#tipoNoticia').val(),
                        estado: $('#estado').val(),
                        contenido: $('#edit').summernote('code'),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                }
            ).done(function (response) {
                //console.log(response);
                if (!response.success) {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Noticia Actualizada', 'success');
                    cargar(false,'#guardarUsuario');
                    cancelar();
                    //$('#modal-1').modal('hide');
                    $("#guardarUsuario")[0].reset();
                    $("#error").hide();
                }

                //return response;
            }).fail(function (error) {

                //console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false,'#guardarUsuario');
                });
            });
        });

        function cancelar() {
            $('#noticia').hide();
            $('#vistaNoticia').show();
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargar(state,button){
            var d = $(button);
            if (state){
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else{
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

        TABLA.on('click', '.editar', function () {
            evento = 'actualizar';
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            console.log(data);
            $("#error").hide();

            //$("#modal-1").modal('show');
            $('#guardarUsuario')[0].reset();

            $('#edit').summernote('destroy');

            $('#actualizar').show();
            $('#guardar').hide();

            $('#noticia').show();
            $('#vistaNoticia').hide();

            $("#idNoticia").val(data.Id);
            $("#titulo").val(data.Titulo);
            $("#tipoNoticia").val(data.Tipo);
            $("#edit").val(data.Contenido);
            $("#estado").val(data.estado);

            $('#edit').summernote({
                focus: true, height: 300, dialogsInBody: true
            });


        });

        TABLA.on('click', '.eliminar', function () {
            evento = 'Eliminar';
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            swal({
                title: "Estas seguro?",
                text: "Realmente desea eliminar esta Noticia?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.post(
                            "/eliminar-noticia", {id: data.Id, _token: $('meta[name="csrf-token"]').attr('content')}
                        ).done(function (data) {
                            TABLA.ajax.reload();
                            notify('Noticia eliminada con exito','success');
                        });
                    } else {

                    }
                });
        });
    </script>


@endsection
