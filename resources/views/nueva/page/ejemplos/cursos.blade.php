@extends('nueva.layout.Dashboard')
@section('page')
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Cursos</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                                class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Cursos Registrados</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- [ sample-page ] start -->
                <div class="col-sm-12">
                    <div class="card animated fadeIn" id="vistaCursos">
                        <div class="card-header">
                            <h5>Cursos</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    @if(auth()->user()->rol_id==1)
                                        <button type="button" class="btn btn btn-primary" id="nuevoCurso">
                                            <i class="feather mr-2 icon-thumbs-up"></i>
                                            Nuevo
                                        </button>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="btn-group mb-2 mr-2">
                                <button class="btn  btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                </button>

                                <ul class="dropdown-menu">
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="0">Cod
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="1">Nombre
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="2">Nivel
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="3">Cupos
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="4">Incripciones
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="5">Fecha Inicio
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="6">Entrenador
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="7">Supervisor
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="8">Ciudad
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="9">Estado
                                        </label>
                                    </li>
                                    <li class="dropdown-item">
                                        <label>
                                            <input autocomplete="off" type="checkbox" checked value="10">Acciones
                                        </label>
                                    </li>
                                </ul>
                            </div>

                            <div class="dt-responsive table-responsive">
                                <table class="table table-striped table-bordered nowrap" id="tablaCursos" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Nivel</th>
                                        <th>Cupos</th>
                                        <th>Incripciones</th>
                                        <th>Fecha Inicio</th>
                                        <th>Entrenador</th>
                                        <th>Supervisor</th>
                                        <th>Ciudad</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaRegistroCurso" style="display: none">
                        <div class="card-header">
                            <h5>Registro De Curso</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarCurso">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombre Curso</label>
                                            <input type="text" id="nombre" class="form-control" placeholder="Nombres">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nivel</label>
                                            <input type="text" id="nivel" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Intensidad Horaria</label>
                                            <input type="number" id="intensidad" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Cupos</label>
                                            <input type="number" id="cupos" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Inicio Inscripcion</label>
                                            <input type="date" id="inscripcionFechaInicio" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Fin Inscripcion</label>
                                            <input type="date" id="inscripcionFechaFin" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Inicio curso</label>
                                            <input type="date" id="fechaInicio" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Fin curso</label>
                                            <input type="date" id="fechaFin" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Entrenador</label>
                                            <select class="col-sm-12" id="select-entrenador" name="state"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ciudad</label>
                                            <select class="form-control" id="ciudad">
                                                <option value="Monteria">Montería</option>
                                                <option value="Sincelejo">Sincelejo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Dias</label>
                                            <input type="text" id="dias" class="form-control" placeholder="1,2,3,4">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Supervisor</label>
                                            <input type="text" id="supervisor" class="form-control" placeholder="Nombre Supervisor">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group fill">
                                            <label>Certificado(PDF)</label>
                                            <input type="file" class="form-control" id="certificado" name="certificado"
                                                   accept="application/pdf">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Estado</label>
                                            <select class="form-control" id="estado">
                                                <option value="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display:none">

                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaActualizarCurso" style="display: none">
                        <div class="card-header">
                            <h5>Actualizacion De Curso</h5>
                        </div>
                        <div class="card-body">
                            <form id="actualizarCurso">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nombre Curso</label>
                                            <input type="text" id="Anombre" class="form-control" placeholder="Nombres">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Nivel</label>
                                            <input type="text" id="Anivel" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Intensidad Horaria</label>
                                            <input type="number" id="Aintensidad" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Cupos</label>
                                            <input type="number" id="Acupos" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Inicio Inscripcion</label>
                                            <input type="date" id="AinscripcionFechaInicio" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Fin Inscripcion</label>
                                            <input type="date" id="AinscripcionFechaFin" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Inicio curso</label>
                                            <input type="date" id="AfechaInicio" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Fecha Fin curso</label>
                                            <input type="date" id="AfechaFin" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Entrenador</label>
                                            <select class="col-sm-12" id="Aselect-entrenador" name="state"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Ciudad</label>
                                            <select class="form-control" id="Aciudad">
                                                <option value="Monteria">Montería</option>
                                                <option value="Sincelejo">Sincelejo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Dias</label>
                                            <input type="text" id="Adias" class="form-control" placeholder="1,2,3,4">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group fill">
                                            <label>Certificado(PDF)</label>
                                            <input type="file" class="form-control" id="Acertificado" name="certificado"
                                                   accept="application/pdf">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group fill">
                                            <label>Supervisor</label>
                                            <input type="text" id="Asupervisor" class="form-control" placeholder="Nombre Supervisor">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="Aerror" style="display:none">

                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
                <!-- [ sample-page ] end -->
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>


    <!-- [ Main Content ] end @ include('modals.curso')-->
@endsection

@section('js')

    <script>
        var IDCURSO = 0;
        var USUARIOS = [];
        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Entrenador'}
            ).done(function (data) {
                //console.log(data);

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].persona.id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-entrenador').select2({
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Entrenador",
                    data: USUARIOS
                });
                $('#Aselect-entrenador').select2({
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    placeholder: "Bucar Entrenador",
                    data: USUARIOS
                });
                $("#select-entrenador").val("").trigger('change');
                $("#Aselect-entrenador").val("").trigger('change');

            });

            function diasEntreFechas(desde, hasta) {
                var dia_actual = moment(desde);
                var fechas = [];
                while (dia_actual.isSameOrBefore(moment(hasta))) {
                    fechas.push(dia_actual.format('DD'));
                    dia_actual.add(1, 'days');
                }
                return fechas;
            }

            $("#fechaInicio").change(function () {
                if ($("#fechaInicio").val() != null && $("#fechaFin").val() != null) {
                    var results = diasEntreFechas($("#fechaInicio").val(), $("#fechaFin").val());
                    $('#dias').val(results.join(', '));
                }
            });
            $("#fechaFin").change(function () {
                if ($("#fechaInicio").val() != null && $("#fechaFin").val() != null) {
                    var results = diasEntreFechas($("#fechaInicio").val(), $("#fechaFin").val());
                    $('#dias').val(results.join(', '));
                }
            });

            $("#AfechaInicio").change(function () {
                if ($("#AfechaInicio").val() != null && $("#AfechaFin").val() != null) {
                    var results = diasEntreFechas($("#AfechaInicio").val(), $("#AfechaFin").val());
                    $('#Adias').val(results.join(', '));
                }
            });
            $("#AfechaFin").change(function () {
                if ($("#AfechaInicio").val() != null && $("#AfechaFin").val() != null) {
                    var results = diasEntreFechas($("#AfechaInicio").val(), $("#AfechaFin").val());
                    $('#Adias').val(results.join(', '));
                }
            });

        });
        var TABLA = $('#tablaCursos').DataTable({
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            paging: false,
            "ajax": {
                "url": "/get-cursos",
                "type": "GET",
                "dataSrc": function (data) {
                    console.log(data);
                    var json = [];
                    for (var item in data.msg) {
                        var estado = (data.msg[item].estado == 0 ? '<span class="badge badge-danger">Inactivo</span>' : '<span class="badge badge-success">Activo</span>');
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].nombre,
                            Nivel: data.msg[item].nivel,
                            Incripciones: data.msg[item].fecha_inicio_inscripciones + "<br>" + data.msg[item].fecha_fin_inscripciones,
                            Fechas: data.msg[item].fecha_inicio_curso + "<br>" + data.msg[item].fecha_fin_curso,
                            Cupos: data.msg[item].matriculados + "/" + data.msg[item].cupos,
                            Entrenador: data.msg[item].entrenador.nombres,
                            Supervisor: data.msg[item].supervisor,
                            Ciudad: data.msg[item].ciudad,
                            Estado: estado,
                            Opciones: opciones()
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            autoWidth: true,
            order: [[0, "desc"]],
            columns: [
                {data: "Id"},
                {data: "Nombre"},
                {data: "Nivel"},
                {data: "Cupos"},
                {data: "Incripciones"},
                {data: "Fechas"},
                {data: "Entrenador"},
                {data: "Supervisor"},
                {data: "Ciudad"},
                {data: "Estado"},
                {data: "Opciones"}
            ]
        });

        $(document).on('change', 'input[type="checkbox"]', function (e) {
            TABLA.column(this.value).visible(this.checked);

        });

        function opciones() {
            var opciones = '';
            if ('{!! auth()->user()->rol->nombre !!}' == "Administrador") {
                opciones += '<button type="button" class="btn btn-sm btn-primary editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit">' +
                '           <i class="feather icon-edit"></i>\n' +
                ' </button>';

            }
            opciones += '<button type="button" class="btn btn-sm btn-success verCurso" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver curso" data-original-title="Edit">' +
                '           <i class="feather icon-eye"></i>\n' +
                ' </button>';

            return opciones;
        }

        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            IDCURSO=data.Id;
            cargar(true,'#actualizarCurso');
            $("#actualizarCurso")[0].reset();

            $('#vistaActualizarCurso').show();
            $('#vistaRegistroCurso').hide();
            $('#vistaCursos').hide();
            $.ajax({
                    url: '/buscar-curso',
                    type: 'POST',
                    data: {
                        id: IDCURSO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#Anombre').val(response.msg.nombre);
                $('#Anivel').val(response.msg.nivel);
                $('#Aintensidad').val(response.msg.intensidad);
                $('#Acupos').val(response.msg.cupos);
                $('#AinscripcionFechaInicio').val(response.msg.fecha_inicio_inscripciones);
                $('#AinscripcionFechaFin').val(response.msg.fecha_fin_inscripciones);
                $('#AfechaInicio').val(response.msg.fecha_inicio_curso);
                $('#AfechaFin').val(response.msg.fecha_fin_curso);
                $('#Aselect-entrenador').val(response.msg.entrenador.id).trigger('change');
                $('#estado').val(response.msg.estado);
                $('#Adias').val(response.msg.dias);
                $('#Aciudad').val(response.msg.ciudad);
                $('#Asupervisor').val(response.msg.supervisor);

                //$('#modal-actualizar').modal();
                cargar(false,'#actualizarCurso');

                return response;

            }).fail(function (error) {

                console.log(error);
                cargar(false,'#actualizarCurso');

            });
        });

        TABLA.on('click', '.verCurso', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            window.location.href = "/dashboard/curso/" + data.Id;
            //alert("Perfil");
        });

        $('#nuevoCurso').on('click',function () {
            $('#vistaCursos').hide();
            $('#vistaRegistroCurso').show();
        });

        function cancelar() {
            $('#vistaCursos').show();
            $('#vistaRegistroCurso').hide();
            $('#vistaActualizarCurso').hide();
        }

        $('#guardar').on('click',function () {
           //alert('guardar');
            cargar(true,this);
            var data = new FormData();
            jQuery.each(jQuery('#certificado')[0].files, function (i, file) {
                data.append('certificado', file);
            });
            data.append('nombre', $('#nombre').val().toUpperCase());
            data.append('nivel', $('#nivel').val().toUpperCase());
            data.append('supervisor', $('#supervisor').val().toUpperCase());
            data.append('intensidad', $('#intensidad').val().toUpperCase());
            data.append('cupos', $('#cupos').val());
            data.append('fecha_inicio_inscripciones', $('#inscripcionFechaInicio').val());
            data.append('fecha_fin_inscripciones', $('#inscripcionFechaFin').val());
            data.append('fecha_inicio_curso', $('#fechaInicio').val());
            data.append('fecha_fin_curso', $('#fechaFin').val());
            data.append('entrenador_id', $('#select-entrenador').val());
            data.append('dias', $('#dias').val());
            data.append('ciudad', $('#ciudad').val());
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                url: '/crear-curso',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    TABLA.ajax.reload();
                    //$('#modal-1').modal('hide');
                    notify('Curso creado con exito','success');
                    cancelar();
                    $("#guardarCurso")[0].reset();
                    $("#error").hide();
                }
                cargar(false,this);

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false,'#guardarCurso');
            });
        });

        $('#actualizar').on('click',function () {
            cargar(true,this);
            var data = new FormData();
            jQuery.each(jQuery('#Acertificado')[0].files, function (i, file) {
                data.append('Acertificado', file);
            });
            data.append('id', IDCURSO);
            data.append('nombre', $('#Anombre').val().toUpperCase());
            data.append('nivel', $('#Anivel').val().toUpperCase());
            data.append('supervisor', $('#Asupervisor').val().toUpperCase());
            data.append('intensidad', $('#Aintensidad').val().toUpperCase());
            data.append('cupos', $('#Acupos').val());
            data.append('fecha_inicio_inscripciones', $('#AinscripcionFechaInicio').val());
            data.append('fecha_fin_inscripciones', $('#AinscripcionFechaFin').val());
            data.append('fecha_inicio_curso', $('#AfechaInicio').val());
            data.append('fecha_fin_curso', $('#AfechaFin').val());
            data.append('entrenador_id', $('#Aselect-entrenador').val());
            data.append('estado', $('#estado').val());
            data.append('dias', $('#Adias').val());
            data.append('ciudad', $('#Aciudad').val());
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));

            $.ajax({
                    url: '/actualizar-curso',
                    type: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false
                }
            ).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#Aerror").html(response.msg);
                    $("#Aerror").show();
                } else {
                    TABLA.ajax.reload();
                    notify('Curso actualizado con exito','success');
                    cancelar();
                    //$('#modal-actualizar').modal('hide');
                    $("#actualizarCurso")[0].reset();
                    $("#Aerror").hide();
                }
                cargar(false,this);

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                    $("#Aerror").show();
                });
                cargar(false,'#actualizarCurso');

            });
        });

        function cargar(state, button) {
            var d = $(button);
            if (state) {
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else {
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

    </script>


@endsection
