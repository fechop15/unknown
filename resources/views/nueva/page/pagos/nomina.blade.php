@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Nomina</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a> Pagos</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!"> Nomina (Mensual)</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Pagos Nomina</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="abrirModal()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaPagoNomina">
                                    <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Mes Pagado</th>
                                        <th>Fecha Pago</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                    <div class="row m-b-30 dashboard-header">
                        <div class="col-md-12">
                            <div class="card quater-card">
                                <div class="card-body">
                                    <h5 class="mb-3">Produccion</h5>
                                    <h6 class="text-muted m-b-15">Recaudado Efectivo del mes</h6>
                                    <h4 id="producido">$0</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                                <div class="card-block">
                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                        <li class="nav-item active">
                                            <a class="nav-link active" data-toggle="tab" href="#profile3" role="tab"
                                               id="select_mes">Mes</a>
                                            <div class="slide"></div>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.pagoNomina')

@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var TOTALAPAGAR = 0;
        var DEUDA = 0;
        var REGISTROS = [];
        var USUARIOS = [];
        var FECHA = null;
        var TABLA = null;

        function abrirModal() {
            $('#modal-6').modal('show');
            $('#error').hide();
            $('#form-pagoNomina')[0].reset();
            $("#guardarPagoNomina").show();
            $('#totalAPagar').html(0);
            $("#select-usuarios").val("").trigger('change');
        }

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaMesNomina').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
                //gerRegistrosMes($("#fechaMes").data("date"));
            });

            $('#fechaMesNomina').on('dp.change', function (e) {
                console.log($("#fechaMesNomina").data("date"));
                console.log($("#select-usuarios").val());
                getToatalPagoMes($("#fechaMesNomina").data("date"), $("#select-usuarios").val());
            });

            $('#select-usuarios').on('change', function (e) {
                console.log($("#fechaMesNomina").data("date"));
                console.log($("#select-usuarios").val());
                getToatalPagoMes($("#fechaMesNomina").data("date"), $("#select-usuarios").val());
                estadoCredito($("#select-usuarios").val());
            });

            $('#pagarDeuda').on('change', function () {
                console.log($('#pagarDeuda').prop('checked'));
                if ($('#pagarDeuda').prop('checked')) {
                    var calculo = TOTALAPAGAR - DEUDA;
                    $('#totalAPagar').html(calculo.toLocaleString());
                    disponibilidad2(calculo);
                } else {
                    $('#totalAPagar').html(TOTALAPAGAR.toLocaleString());
                    disponibilidad2(TOTALAPAGAR)
                }

            });

            $.post(
                "/get-usuarios-by-rol-pago", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    rol: 'Empleado',
                    pago: "Mensual"
                }
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal-6"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });
                $("#select-usuarios").val("").trigger('change');

            });

            FECHA = $("#fechaMes").data("date");

            TABLA = $('#tablaPagoNomina').DataTable({
                "ajax": {
                    "url": "/get-nomina",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        REGISTROS = [];
                        total = 0;
                        console.log(data);
                        for (var item in data.msg) {
                            REGISTROS.push(data.msg[item]);
                            var itemJson = {
                                Id: data.msg[item].id,
                                Empleado: data.msg[item].empleado,
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Fecha: data.msg[item].fecha,
                                FechaPago: data.msg[item].fechaPago,
                            };
                            total += data.msg[item].valor;

                            json.push(itemJson)
                        }
                        getProduccion();
                        //calcularToral();
                        //$('#total').html(total.toLocaleString());
                        return json;
                    }
                },
                columns: [
                    {data: "Empleado"},
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Fecha"},
                    {data: "FechaPago"},
                ],
            });

            function calcularToralAPagar(total) {
                var valor = parseInt(total.sueldoFijo) + parseInt(total.totalGanancia);
                TOTALAPAGAR = valor;
                $('#totalAPagar').html(valor.toLocaleString());
                if (TOTALAPAGAR > 0 && TOTALAPAGAR < TOTALDISPONIBLE) {
                    $("#guardarPagoNomina").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    if (TOTALAPAGAR > TOTALDISPONIBLE) {
                        $("#guardarPagoNomina").hide();
                        $("#error").show();
                        $("#error").html("No Hay Fondos para pagos");
                    } else {
                        $("#guardarPagoNomina").hide();
                        $("#error").show();
                        $("#error").html("No Hay Pagos Para Este Doctor");
                    }

                }

            }

            function getToatalPagoMes(mes, empleado) {
                if (empleado == null) {
                    return;
                }
                $.post(
                    "/get-total-pago-mes", {
                        fecha: mes,
                        empleado: empleado,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    calcularToralAPagar(data.msg);
                });
            }

            $("#select_mes").click(function () {
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });

            function estadoCredito(empleado) {
                if (empleado == null) {
                    return
                }
                $.post(
                    "/get-estado-credito-empleado", {
                        empleado: empleado,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    DEUDA = data.avance - data.abono - data.descuento;
                    if (DEUDA > 0) {
                        $("#nota").html("Actualmente tiene un avance de: $" + DEUDA.toLocaleString());
                        $("#deuda").show();

                    } else {
                        DEUDA = 0;
                        $("#nota").html("");
                        $("#deuda").hide();

                    }
                });
            }

        });

        function disponibilidad(total) {
            if (TOTALDISPONIBLE >= total.value) {
                $("#guardarPagoNomina").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardarPagoNomina").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }
        }

        function disponibilidad2(total) {
            if (TOTALDISPONIBLE >= total) {
                $("#guardarPagoNomina").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardarPagoNomina").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }
        }

        function calcularToral(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())

        }

        function getProduccion() {
            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });
        }

        $('#guardarPagoNomina').on('click', function () {
            cargar(true, '#guardarPagoNomina');
            $("#error").hide();
            $.ajax({
                url: '/crear-pago-nomina',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    fecha: $("#fechaMesNomina").data("date"),
                    valor: TOTALAPAGAR,
                    empleado: $('#select-usuarios').val(),
                    pagarDeuda: $('#pagarDeuda').prop('checked'),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                $('#modal-6').modal('hide');
                //getProduccion();
                cargar(false, '#guardarPagoNomina');
                notify('Pago Registrado con Exito', 'success');
                $('#form-pagoNomina')[0].reset();
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false, '#guardarPagoNomina');
            });
        });

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        $("#guardarPagoNomina").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }


    </script>


@endsection
