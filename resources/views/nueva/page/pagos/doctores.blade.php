@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Pago Diarios A Doctores</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a>Pagos</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!"> Diario A Doctores</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Pago Diarios A Doctores</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="abrirModal()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                            <a style="float: right;padding-right: 30px">Total: $
                                <strong id="total">0</strong>
                            </a>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaPagoDoctores">
                                    <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Mes Pagado</th>
                                        <th>Fecha Pago</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="col-md-12">
                        <div class="card quater-card">
                            <div class="card-body">
                                <h5 class="mb-3">Produccion</h5>
                                <h6 class="text-muted m-b-15">Recaudado Efectivo del Mes</h6>
                                <h4 id="producido">$0</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                            <div class="card-block">
                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                           id="select_dia">Dia</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                           id="select_mes">Mes</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaDia"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="profile3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaMes"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.pagoDoctores')
@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var TOTALAPAGAR = 0;
        var REGISTROS = [];
        var USUARIOS = [];
        var TABLA;
        var FECHA;

        function abrirModal() {
            $('#modal-7').modal('show');
            $('#error').hide();
            $('#form-pagoDoctores')[0].reset();
            $("#guardarPagoDoctor").show();
            $('#totalAPagar').html(0);
        }

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });

            $('#fechaDiaNomina').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });


            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                //gerRegistrosMes($("#fechaDia").data("date"));
                FECHA = $("#fechaDia").data("date");
                TABLA.ajax.reload();
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                //gerRegistrosMes($("#fechaMes").data("date") + "-%");
                FECHA = $("#fechaMes").data("date") + "-%";
                TABLA.ajax.reload();
            });

            $('#fechaDiaNomina').on('dp.change', function (e) {
                console.log($("#fechaDiaNomina").data("date"));
                console.log($("#select-usuarios").val());
                getToatalPagoMes($("#fechaDiaNomina").data("date"), $("#select-usuarios").val());
            });

            $('#select-usuarios').on('change', function (e) {
                console.log($("#fechaDiaNomina").data("date"));
                console.log($("#select-usuarios").val());
                getToatalPagoMes($("#fechaDiaNomina").data("date"), $("#select-usuarios").val());

            });

            $.post(
                "/get-usuarios-by-rol-pago", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    rol: 'Empleado',
                    pago: "Diario"
                }
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal-7"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });
                $("#select-usuarios").val("").trigger('change');

            });

            FECHA = $("#fechaDia").data("date");

            TABLA = $('#tablaPagoDoctores').DataTable({
                "ajax": {
                    "url": "/get-nomina",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.tipo = "Diario";
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        REGISTROS = [];
                        total = 0;
                        console.log(data);
                        for (var item in data.msg) {
                            REGISTROS.push(data.msg[item]);
                            var itemJson = {
                                Id: data.msg[item].id,
                                Empleado: data.msg[item].empleado,
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Fecha: data.msg[item].fecha,
                                FechaPago: data.msg[item].fechaPago,
                            };
                            total += parseInt(data.msg[item].valor);

                            json.push(itemJson)
                        }
                        getProduccion();
                        calcularToral();
                        return json;
                    }
                },
                columns: [
                    {data: "Empleado"},
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Fecha"},
                    {data: "FechaPago"},
                ],
            });

            function calcularToralAPagar(total) {
                var valor = parseInt(total.totalGanancia);
                TOTALAPAGAR = valor;
                $('#totalAPagar').html(valor.toLocaleString());
                if (TOTALAPAGAR > 0) {
                    $("#guardarPagoDoctor").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    $("#guardarPagoDoctor").hide();
                    $("#error").show();
                    $("#error").html("No Hay Pagos Para Este Doctor Hoy");
                }
            }

            function getToatalPagoMes(mes, empleado) {
                if (empleado == null) {
                    return;
                }
                $.post(
                    "/get-total-pago-mes", {
                        fecha: mes,
                        empleado: empleado,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    calcularToralAPagar(data.msg);
                });
            }

            $("#select_dia").click(function () {
                //gerRegistrosMes($("#fechaDia").data("date"));
                FECHA = $("#fechaDia").data("date");
                TABLA.ajax.reload();
            });

            $("#select_mes").click(function () {
                //gerRegistrosMes($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });
        });

        function disponibilidad(total) {
            if (TOTALDISPONIBLE >= total.value && TOTALAPAGAR > 0) {
                $("#guardarPagoDoctor").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardarPagoDoctor").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }
        }

        function getProduccion() {
            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                var total = parseInt(data.msg.totalEfectivo) - parseInt(data.msg.totalTGastos);
                TOTALDISPONIBLE = total;
                $('#producido').html("$" + total.toLocaleString())
            });
        }

        $('#guardarPagoDoctor').on('click', function () {
            cargar(true, '#guardarPagoDoctor');
            $("#error").hide();
            $.ajax({
                url: '/crear-pago-nomina',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    fecha: $("#fechaDiaNomina").data("date"),
                    valor: TOTALAPAGAR,
                    empleado: $('#select-usuarios').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                $('#modal-7').modal('hide');
                //getProduccion();
                cargar(false, '#guardarPagoDoctor');
                notify('Pago Registrado con Exito', 'success');
                $('#form-pagoDoctores')[0].reset();
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
            });
        });

        function calcularToral() {
            var total = 0;

            for (var item in REGISTROS) {
                total += parseInt(REGISTROS[item].valor);
            }

            $('#total').html(parseInt(total).toLocaleString());
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        $("#guardarPagoDoctor").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

    </script>


@endsection
