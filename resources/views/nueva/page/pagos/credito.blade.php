@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Avances</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a> Pagos</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!"> Avanves de pagos</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Avances de pagos</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="abrirModal()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover" id="tablaAvances" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Tipo</th>
                                        <th>Fecha</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        <div class="card quater-card">
                            <div class="card-body">
                                <h5 class="mb-3">Produccion</h5>
                                <h6 class="text-muted m-b-15">Recaudado Efectivo del mes</h6>
                                <h4 id="producido">$0</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                            <div class="card-block">
                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link active" data-toggle="tab" href="#profile3" role="tab"
                                           id="select_mes">Mes</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaMes"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.credito')

@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var TOTALAPAGAR = 0;
        var REGISTROS = [];
        var USUARIOS = [];
        var FECHA = null;
        var TABLA = null;

        function abrirModal() {
            $('#modal').modal('show');
            $('#error').hide();
            $('#form-credito')[0].reset();
            $("#guardarCredito").show();
            $('#totalAPagar').html(0);
            $("#select-usuarios").val("").trigger('change');

        }

        $(document).ready(function () {
            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            FECHA = $("#fechaMes").data("date");

            TABLA = $('#tablaAvances').DataTable({
                "ajax": {
                    "url": "/get-credito-empleados",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Id: data.msg[item].id,
                                Empleado: data.msg[item].empleado,
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        getProduccion();

                        return json;
                    }
                },
                columns: [
                    {data: "Empleado"},
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Fecha"},
                ],
            });


            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });

            $('#select-usuarios').on('change', function (e) {
                console.log($("#select-usuarios").val());
                estadoCredito($("#select-usuarios").val());
            });

            $.post(
                "/get-usuarios-by-rol-pago", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    rol: 'Empleado',
                    pago: "Mensual"
                }
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });
                $("#select-usuarios").val("").trigger('change');

            });

            function estadoCredito(empleado) {
                if (empleado == null) {
                    return
                }
                $.post(
                    "/get-estado-credito-empleado", {
                        empleado: empleado,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    var valor = data.avance - data.abono - data.descuento;
                    if (valor > 0) {
                        $("#nota").html("Actualmente Debe: $" + valor.toLocaleString());
                    } else {
                        $("#nota").html("");
                    }
                });
            }

            function calcularToralAPagar(total) {
                var valor = parseInt(total.sueldoFijo) + parseInt(total.totalGanancia);
                TOTALAPAGAR = valor;
                $('#totalAPagar').html(valor.toLocaleString());

                if (TOTALAPAGAR > 0 && TOTALAPAGAR < TOTALDISPONIBLE) {
                    $("#guardar").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    if (TOTALAPAGAR > TOTALDISPONIBLE) {
                        $("#guardar").hide();
                        $("#error").show();
                        $("#error").html("No Hay Fondos para pagos");
                    } else {
                        $("#guardar").hide();
                        $("#error").show();
                        $("#error").html("No Hay Pagos Para Este Doctor");
                    }

                }

            }

        });

        function disponibilidad(total) {


            if (TOTALDISPONIBLE >= total.value || $("#tipo").val() == "Abono") {
                $("#guardar").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardar").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }

            var totalAvance = parseInt(total.value);
            $('#totalAPagar').html(totalAvance.toLocaleString())
        }

        function calcularToral(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())

        }

        function getProduccion() {
            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });
        }

        $('#guardarCredito').on('click', function () {
            cargar(true, '#guardarCredito');
            $("#error").hide();

            $.ajax({
                url: '/credito-empleado',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    valor: $('#valor').val(),
                    tipo: $('#tipo').val(),
                    empleado: $('#select-usuarios').val(),
                    _token: $('meta[name="csrf-token"]').attr('content'),
                },

            }).done(function (response) {

                console.log(response);
                TABLA.ajax.reload();
                $('#descripcion').val("");
                $('#valor').val("");
                $('#modal').modal('hide');
                cargar(false, '#guardarCredito');
                notify('Credito Registrado con Exito', 'success');

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false, '#guardarCredito');

            });
        });


        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        $("#guardarCredito").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }
    </script>


@endsection
