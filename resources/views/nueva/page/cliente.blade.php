@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>{{$cliente->nombre}}</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('clientes')}}">Paciente</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Perfil</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <!-- Nav tabs -->
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-pills bg-white" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active text-uppercase" data-toggle="tab" href="#personal"
                                       role="tab">Informacion
                                        Personal</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" data-toggle="tab" href="#project" role="tab">Historial</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" data-toggle="tab" href="#questions" role="tab">Anotaciones</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" data-toggle="tab" href="#members" role="tab">Saldo</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Sobre Mi</h5>
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombre Completo</th>
                                                                    <td>{{$cliente->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Identificacion</th>
                                                                    <td>{{$cliente->cedula}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Telefono</th>
                                                                    <td>{{$cliente->telefono}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Edad</th>
                                                                    <td>{{$cliente->edad}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Ciudad</th>
                                                                    <td>{{$cliente->ciudad}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Direccion</th>
                                                                    <td> {{$cliente->direccion}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Ocupacion</th>
                                                                    <td>{{$cliente->ocupacion}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Saldo</th>
                                                                    <td id="credito"></td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Historial Clinico</h5>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablaHistorial">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Servicio</th>
                                                        <th class="text-center txt-primary">Valor</th>
                                                        <th class="text-center txt-primary">Fecha</th>
                                                        <th class="text-center txt-primary">Metodo de pago</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                    @foreach($cliente->facturas->reverse() as $factura)
                                                        <tr>
                                                            <td>{{$factura->servicio}}</td>
                                                            <td>
                                                                ${{(number_format( $factura->precio , 0, '.', '.'))}}</td>
                                                            <td>{{date("d/m/Y h:i A", strtotime($factura->created_at))}}</td>
                                                            <td class="text-center">
                                                                @if($factura->metodo_pago=="Efectivo")
                                                                    <span
                                                                        class="badge badge-success">{{$factura->metodo_pago}}</span>
                                                                @elseif($factura->metodo_pago=="Tarjeta")
                                                                    <span
                                                                        class="badge badge-info">{{$factura->metodo_pago}}</span>
                                                                @else
                                                                    <span
                                                                        class="badge badge-warning">{{$factura->metodo_pago}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">
                            <section class="panels-wells">

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text">Notas del Paciente</h5>
                                        <button type="button"
                                                class="btn btn-primary waves-effect waves-light f-right"
                                                onclick="openModal()">
                                            + Nueva Nota
                                        </button>
                                    </div>
                                    <div class="card-block">
                                        <div class="row" id="notas">


                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Movimientos de Pagos</h5>
                                            <a style="float: right;padding-right: 30px">
                                                <strong style="padding-right: 30px" id="nota"></strong>
                                                @if(auth()->user()->rol_id==1)
                                                    <button type="button"
                                                            class="btn btn-primary waves-effect waves-light f-right"
                                                            onclick="openModalRecarga()">
                                                        Recargar
                                                    </button>
                                                @endif
                                            </a>

                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablapagos" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Descripcion</th>
                                                        <th>Valor</th>
                                                        <th>Tipo</th>
                                                        <th>Fecha</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMesPagos"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    @include('nueva.modal.notas')
    @include('nueva.modal.creditoCliente')

@endsection

@section('js')

    <script>
        var IDCLIENTE = '{!! $cliente->id !!}';
        var IDNOTA = 0;
        var TABLA = 0;
        var FECHA = null;
        estadoCredito();
        $('#tablaHistorial').DataTable();
        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-notas", {_token: $('meta[name="csrf-token"]').attr('content'), cliente: IDCLIENTE}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    nota(data.msg[item], 'agregar');
                }

            });
            $('#fechaMesPagos').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            FECHA = $("#fechaMesPagos").data("date");

            TABLA = $('#tablapagos').DataTable({
                "ajax": {
                    "url": "/get-estado-credito-cliente",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.cliente = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + data.msg[item].valor.toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Fecha"},
                ],
            });

            $('#fechaMesPagos').on('dp.change', function (e) {
                console.log($("#fechaMesPagos").data("date"));
                FECHA = $("#fechaMesPagos").data("date");
                TABLA.ajax.reload();
                //gerRegistrosMes($("#fechaMes").data("date"));
            });
        });

        @if(auth()->user()->rol_id==1)

        $('#guardarCreditoCliente').on('click', function () {
            cargar(true, "#guardarCreditoCliente");
            $("#errorRecarga").hide();
            $.ajax({
                url: '/credito-cliente',
                type: 'POST',
                data: {
                    valor: $('#valorRecarga').val().toUpperCase(),
                    tipo: 'Recarga',
                    cliente: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                //console.log(response);
                $('#modal-recarga').modal('hide');
                notify("Credito agregado con exito", 'success');
                TABLA.ajax.reload();
                cargar(false, "#guardarCreditoCliente");
                $('#form-recarga')[0].reset();
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#errorRecarga").html(value[0]);
                    $("#errorRecarga").show();
                });
                cargar(false, "#guardarCreditoCliente");

            });
        });


        function openModalRecarga() {
            $('#valorRecarga').val("");
            $('#totalARecargar').html("0");
            $('#modal-recarga').modal();
            $('#form-recarga')[0].reset();

        }

        @endif
        function disponibilidad(total) {
            var totalAvance = parseInt(total.value);
            $('#totalARecargar').html(totalAvance.toLocaleString())
        }

        /---------------------------------/

        $('#guardarNota').on('click', function () {
            cargar(true, "#guardarNota");
            $("#error").hide();

            $.ajax({
                url: '/crear-nota',
                type: 'POST',
                data: {
                    titulo: $('#titulo').val().toUpperCase(),
                    descripcion: $('#descripcion').val(),
                    cliente_id: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                //console.log(response);
                nota(response.msg, 'agregar');
                $('#modal-4').modal('hide');
                cargar(false, "#guardarNota");
                notify("Nota agregada con exito", 'success');
                $('#form-nota')[0].reset();
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false, "#guardarNota");

            });
        });

        $('#actualizarNota').on('click', function () {
            cargar(true, "#actualizarNota");
            $("#error").hide();
            $.ajax({
                    url: '/actualizar-nota',
                    type: 'POST',
                    data: {
                        id: IDNOTA,
                        titulo: $('#titulo').val().toUpperCase(),
                        descripcion: $('#descripcion').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                }
            ).done(function (response) {
                //console.log(response);
                nota(response.msg, 'remplazar');
                $('#modal-4').modal('hide');
                notify("Nota Actualizada con exito", 'success');
                cargar(false, "#actualizarNota");
                $('#form-nota')[0].reset();
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false, "#actualizarNota");
                });
                cargar(false, "#actualizarNota");
            });
        });

        function actualizarModal(id) {
            cargar(true, "#actualizarNota");
            $("#error").hide();

            $('#modal-4').modal();
            IDNOTA = id;
            $.ajax({
                    url: '/buscar-nota',
                    type: 'POST',
                    data: {
                        id: IDNOTA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                }
            ).done(function (response) {
                //console.log(response);
                $('#titulo').val(response.msg.titulo);
                $('#descripcion').val(response.msg.descripcion);
                $('#guardarNota').hide();
                $('#actualizarNota').show();
                cargar(false, "#actualizarNota");
                return response;

            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();

                });
                cargar(false, "#actualizarNota");

            });
        }

        function openModal() {
            $('#titulo').val("");
            $('#descripcion').val("");
            $('#modal-4').modal();
            $('#actualizarNota').hide();
            $('#guardarNota').show();
        }

        /---------------------------------/


        function nota(data, op) {
            var html = '<div class="col-xl-2 col-lg-4 col-md-4 col-sm-6" id="ser_' + data.id + '">' +
                '<div class="card text-white bg-primary">' +
                '<div class="card-header">' + data.titulo +
                '<a href="" data-toggle="modal" data-target="#only-icon-Modal" class="f-right" style="color: white;float: right;" onclick="actualizarModal(' + data.id + ')">\n' +
                '                                        <i class="fa fa-edit"></i>\n' +
                '                                    </a> </div>' +
                '<div class="card-body">' +
                '<p class="card-text">' + data.descripcion + '</p>' +
                '</div>' +
                '<div class="card-footer">' +
                '<small class="text-white">Fecha' + data.fecha + '</small>\n' +
                '</div>' +
                '</div>' +
                '</div>';
            if (op == "agregar") {
                $('#notas').append(html);
            } else if (op == "remplazar") {
                $('#ser_' + data.id).replaceWith(html);
            }

        }

        function estadoCredito() {
            $.post(
                "/get-estado-credito-cliente", {
                    cliente: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                var valor = data.recarga - data.pago;
                if (valor >= 0) {
                    $("#credito").html("$" + valor.toLocaleString());
                    $("#nota").html("Actualmente tiene: $" + valor.toLocaleString());
                } else {
                    $("#credito").html("Actualmente Debe: $0");
                    $("#nota").html("Actualmente Debe: $0");
                }
            });
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        $("#actualizarNota").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        $("#guardarNota").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        $("#guardarCreditoCliente").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

    </script>


@endsection
