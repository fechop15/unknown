@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Consignaciones</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!"> Consignaciones </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Movimientos Tarjeta</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="abrirModal()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover" id="tablaConsignaciones">
                                    <thead>
                                    <tr>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Tipo</th>
                                        <th>Fecha Creacion</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                    <div class="col-md-12">
                        <div class="card quater-card">
                            <div class="card-body">
                                <h5 class="mb-3">Produccion</h5>

                                <h6 class="text-muted m-b-15">Recaudado Virtual</h6>
                                <h4 id="producidoT">$0</h4>

                                <h6 class="text-muted m-b-15">Recaudado Efectivo del mes</h6>
                                <h4 id="producido">$0</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                            <div class="card-block">
                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                           id="select_dia">Dia</a>
                                        <div class="slide"></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                           id="select_mes">Mes</a>
                                        <div class="slide"></div>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="home3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaDia"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="profile3" role="tabpanel">
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaMes"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>
    @include('nueva.modal.consignaciones')

@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var TOTALDISPONIBLEE = 0;
        var opciones = [];
        var REGISTROS = [];
        var TABLA = 0;
        var FECHA;

        function abrirModal() {
            $('#modal-8').modal('show');
            $('#error').hide();
            $('#form-consignaciones')[0].reset();
            $("#guardarConsignaion").show();
            $('#totalConsignacion').html(0);
        }

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                FECHA = $("#fechaDia").data("date");
                TABLA.ajax.reload();
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });
            FECHA = $("#fechaDia").data("date");
            TABLA = $('#tablaConsignaciones').DataTable({
                "ajax": {
                    "url": "/get-gasto-mes",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.razon = "Consignacion";
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        REGISTROS = [];
                        total = 0;
                        console.log(data);
                        for (var item in data.msg) {
                            REGISTROS.push(data.msg[item]);
                            var itemJson = {
                                Id: data.msg[item].id,
                                Descripcion: data.msg[item].descripcion,
                                Tipo: data.msg[item].tipo,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Fecha: data.msg[item].fecha,
                            };
                            total += data.msg[item].valor;

                            json.push(itemJson)
                        }
                        getProduccion();
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Tipo"},
                    {data: "Valor"},
                    {data: "Fecha"},
                ],
            });

            $("#select_mes").click(function () {
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });

            $("#select_dia").click(function () {
                FECHA = $("#fechaDia").data("date");
                TABLA.ajax.reload();
            })

        });

        function disponibilidad(total) {
            var valor = parseInt(total.value == '' ? 0 : total.value);
            $('#totalConsignacion').html(valor.toLocaleString());

            if ($("#tipo").val() == 'Salida') {
                if (TOTALDISPONIBLE >= valor && valor > 0) {
                    $("#guardarConsignaion").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    $("#guardarConsignaion").hide();
                    $("#error").show();
                    $("#error").html("Fondos Insufucientes");
                }
            } else {

                if (TOTALDISPONIBLEE >= valor && valor > 0) {
                    $("#guardarConsignaion").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    $("#guardarConsignaion").hide();
                    $("#error").show();
                    $("#error").html("Fondos Insufucientes");
                }
            }

        }

        function verificar() {
            var valor = parseInt($("#valor").val() == '' ? 0 : $("#valor").val());
            $('#totalConsignacion').html(valor.toLocaleString());

            if ($("#tipo").val() == 'Salida') {
                if (TOTALDISPONIBLE >= valor && valor > 0) {
                    $("#guardarConsignaion").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    $("#guardarConsignaion").hide();
                    $("#error").show();
                    $("#error").html("Fondos Insufucientes");
                }
            } else {

                if (TOTALDISPONIBLEE >= valor && valor > 0) {
                    $("#guardarConsignaion").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    $("#guardarConsignaion").hide();
                    $("#error").show();
                    $("#error").html("Fondos Insufucientes");
                }
            }

        }

        function calcularToral(data) {
            var total = data.totalTarjeta + data.totalTarjetaEntrada - data.totalTarjetaSalida;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())

        }

        function calcularToralEfectivo(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLEE = total;
            $('#producidoE').html("$" + total.toLocaleString())
        }

        function getProduccion() {
            $.get(
                "/produccion-tarjeta"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });

            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToralEfectivo(data.msg);
            });
        }

        $('#guardarConsignaion').on('click', function () {
            cargar(true, '#guardarConsignaion');
            $("#error").hide();
            $.ajax({
                url: '/crear-consignacion',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    valor: $('#valor').val(),
                    razon: "Consignacion",
                    tipo: $('#tipo').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                $('#descripcion').val("");
                $('#valor').val("");
                $('#modal-8').modal('hide');
                cargar(false, '#guardarConsignaion');
                notify('Gasto Registrado con Exito', 'success');
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
                cargar(false, '#guardarConsignaion');

            });
        });


        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.children(".spinner-border").show();
                d.children(".spinner-grow").show();
                d.children(".load-text").show();
                d.children(".btn-text").hide();
                d.attr("disabled", "true");

            } else {
                d.children(".spinner-border").hide();
                d.children(".spinner-grow").hide();
                d.children(".load-text").hide();
                d.children(".btn-text").show();
                d.removeAttr("disabled");
            }
        }

        $("#guardarConsignaion").each(function () {
            $(this).children(".spinner-border").hide();
            $(this).children(".spinner-grow").hide();
            $(this).children(".load-text").hide()
        });

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }
    </script>


@endsection
