@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>Pacientes</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="/">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('clientes')}}">Pacientes</a>
                                </li>
                                <li class=" breadcrumb-item"><a href="#">Pacientes Registrados</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card animated fadeIn" id="vistaPacientes">
                        <div class="card-header">
                            <h5 class="card-header-text">Gestion de Pacientes</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn btn btn-primary" id="nuevoPaciente">
                                        <i class="feather mr-2 icon-thumbs-up"></i>
                                        Agregar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-hover" id="tablaClientes">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Cedula</th>
                                            <th>Telefono</th>
                                            <th>Ciudad</th>
                                            <th>Ocupacion</th>
                                            <th>Credito</th>
                                            <th>Opciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card animated fadeIn" id="vistaRegistroPaciente" style="display: none">
                        <div class="card-header">
                            <h5>Registro De Paciente</h5>
                        </div>
                        <div class="card-body">
                            <form id="guardarPasiente">
                                <input type="hidden" id="id">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-control-label">Nombre </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="nombre"
                                                   placeholder="Nombre Del Paciente">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Identificacion </label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="cedula"
                                                   placeholder="Cedula De Ciudadania">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Edad </label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="edad"
                                                   placeholder="Años de edad">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Telefono </label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="telefono"
                                                   placeholder="Numero telefonico">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Direccion </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="direccion"
                                                   placeholder="Direccion de residencia">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Ocupación</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="ocupacion"
                                                   placeholder="Ocupación">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Ciudad</label>
                                        <div class="input-group">
                                            <input type="text" id="ciudad" class="form-control" placeholder="Ciudad">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label class="form-control-label">Estado Civil </label>
                                        <div class="input-group">
                                            <select id="estado_civil" class="form-control">
                                                <option value="Casado(a)">Casado(a)</option>
                                                <option value="Soltero(a)">Soltero(a)</option>
                                                <option value="Union Libre">Union Libre</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert" id="error" style="display: none">
                                        </div>
                                    </div>

                                </div>

                                <div style="float: right;padding-top: 10px;">
                                    <button type="button" class="btn btn-default waves-effect" onclick="cancelar()">
                                        Cerrar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="guardar">Guardar
                                    </button>
                                    <button type="button" class="btn btn-primary waves-effect"
                                            id="actualizar">Actualizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

@endsection
@section('js')

    <script>
        var TABLA = null;
        TABLA = $('#tablaClientes').DataTable({
            "ajax": {
                "url": "/get-clientes",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: '<a href="/dashboard/perfil/' + data.msg[item].id + '" >' + data.msg[item].nombre + '</a>',
                            Cedula: data.msg[item].cedula,
                            Telefono: data.msg[item].telefono,
                            Ciudad: data.msg[item].ciudad,
                            Ocupacion: data.msg[item].ocupacion,
                            Credito: "$" + data.msg[item].credito.toLocaleString(),
                            Opciones: opciones(data.msg[item].id),
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Cedula"},
                {data: "Telefono"},
                {data: "Ciudad"},
                {data: "Ocupacion"},
                {data: "Credito"},
                {data: "Opciones"},
            ],
        });

        function opciones(id) {
            var opciones = '' +
                '<button type="button" class="btn btn-primary waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit">' +
                '           <i class="fa fa-edit"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-success waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver perfil" data-original-title="Edit"' +
                '           onclick="verPerfil(' + id + ')">' +
                '           <i class="fa fa-eye"></i>' +
                '</button>';
            if ('{!! auth()->user()->rol_id !!}' == '1') {
                // opciones += '<button type="button" class="btn btn-info waves-effect waves-light" ' +
                //   '           data-toggle="tooltip" data-placement="top" title="Agregar Saldo" data-original-title="Edit"' +
                // '           onclick="saldoModal(' + id + ')">' +
                // '           <i class="icofont icofont-cur-dollar"></i>' +
                // '</button>';
            }


            return opciones;
        }

        TABLA.on('click', '.actualizar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $('#id').val(data.Id);
            $("#guardarPasiente")[0].reset();

            $('#vistaPacientes').hide();
            $('#vistaRegistroPaciente').show();
            $('#actualizar').show();
            $('#guardar').hide();
            cargar(true, '#guardarPasiente');
            $.ajax({
                    url: '/buscar-cliente',
                    type: 'POST',
                    data: {
                        id: data.Id,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                console.log(response);

                $('#nombre').val(response.msg.nombre);
                $('#cedula').val(response.msg.cedula);
                $('#edad').val(response.msg.edad);
                $('#telefono').val(response.msg.telefono);
                $('#direccion').val(response.msg.direccion);
                $('#ocupacion').val(response.msg.ocupacion);
                $('#ciudad').val(response.msg.ciudad);
                $('#estado_civil').val(response.msg.estado_civil);

                cargar(false, '#guardarPasiente');
                return response;

            }).fail(function (error) {
                console.log(error);
                cargar(false, '#guardarPasiente');
            });
        });

        $('#guardar').on('click', function () {
            cargar(true, '#guardarPasiente');
            $("#error").hide();
            $.ajax({
                url: '/crear-cliente',
                type: 'POST',
                data: {
                    nombre: $('#nombre').val().toUpperCase(),
                    cedula: $('#cedula').val(),
                    edad: $('#edad').val(),
                    telefono: $('#telefono').val(),
                    direccion: $('#direccion').val().toUpperCase(),
                    ocupacion: $('#ocupacion').val().toUpperCase(),
                    ciudad: $('#ciudad').val().toUpperCase(),
                    estado_civil: $('#estado_civil').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                notify('Paciente Registrado con Exito', 'success');
                cargar(false, '#guardarPasiente');
                $("#guardarPasiente")[0].reset();
                cancelar();
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false, '#guardarPasiente');
                    return;
                });
                cargar(false, '#guardarPasiente');

            });

        });

        $('#actualizar').on('click', function () {
            cargar(true, '#guardarPasiente');
            $("#error").hide();
            $.ajax({
                    url: '/actualizar-cliente',
                    type: 'POST',
                    data: {
                        id: $('#id').val(),
                        nombre: $('#nombre').val().toUpperCase(),
                        cedula: $('#cedula').val(),
                        telefono: $('#telefono').val(),
                        edad: $('#edad').val(),
                        direccion: $('#direccion').val().toUpperCase(),
                        ocupacion: $('#ocupacion').val().toUpperCase(),
                        ciudad: $('#ciudad').val().toUpperCase(),
                        estado_civil: $('#estado_civil').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                notify('Paciente Actualizado con Exito', 'success');
                cargar(false, '#guardarPasiente');
                $("#guardarPasiente")[0].reset();
                cancelar();
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                    cargar(false, '#guardarPasiente');
                    return;
                });
                cargar(false, '#guardarPasiente');
            });
        });

        function verPerfil(id) {
            window.location.href = "/dashboard/perfil/" + id;
        }

        $('#nuevoPaciente').on('click', function () {
            $('#vistaPacientes').hide();
            $('#actualizar').hide();
            $('#vistaRegistroPaciente').show();
            $('#guardar').show();
        });

        function cancelar() {
            $('#vistaPacientes').show();
            $('#vistaRegistroPaciente').hide();
            $('#vistaActualizarPaciente').hide();
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        function cargar(state, form) {
            var d = $(form);
            if (state) {
                d.parents(".card").addClass("card-load");
                d.parents(".card").append('<div class="card-loader"><i class="pct-loader1 anim-rotate"></div>');
            } else {
                d.parents(".card").children(".card-loader").remove();
                d.parents(".card").removeClass("card-load");
            }
        }
    </script>


@endsection
