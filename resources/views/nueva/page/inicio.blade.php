@extends('nueva.layout.Dashboard')
@section('page')
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Dashboard</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i
                                            class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#!">Informacion General</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- page statustic card start -->
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-yellow">{{$usuarios->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Usuarios</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-users f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-yellow">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">En El Sistema</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-green">{{$clientes->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Pacientes</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-user-graduate f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-green">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">Registrados</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-red">{{auth()->user()->servicios->count()}}</h4>
                                            <h6 class="text-muted m-b-0">Atendidos</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-book f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-red">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">Usuarios Atendidos</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-blue">0</h4>
                                            <h6 class="text-muted m-b-0">Certificados</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="fas fa-certificate f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-blue">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">Generados</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- page statustic card end -->
                </div>
                <!-- prject ,team member start -->
                <div class="col-xl-7 col-md-12">
                    <div class="card table-card">
                        <div class="card-header">
                            <h5>Usuarios</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                </div>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Ultimo Ingreso</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($usuarios->sortByDesc('last_login')->take(5) as $usuario)
                                        @if($usuario->last_login!=null)
                                            <tr>
                                                <td>
                                                    <div class="d-inline-block align-middle">
                                                        <img src="/images/perfil/{{$usuario->imagen}}" alt="user image"
                                                             class="img-radius wid-40 hei-40 align-top m-r-15">
                                                        <div class="d-inline-block">
                                                            <h6>{{$usuario->nombre}}</h6>
                                                            <p class="text-muted m-b-0">
                                                                Rol: {{$usuario->rol->nombre}}</p>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($usuario->last_login)->diffForHumans()}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-md-12">
                    <div class="card user-card2">
                        <div class="card-body text-center">
                            <h6 class="m-b-15">Bienvenido</h6>
                            <img class="img-radius img-fluid wid-150 hei-150"
                                 src="/images/perfil/{{auth()->user()->imagen}}"
                                 alt="User image">
                            <h6 class="m-b-10 m-t-10">{{auth()->user()->nombre}}</h6>
                            <a href="{{route('perfil')}}"
                               class="text-c-green b-b-success">{{auth()->user()->rol->nombre}}</a>
                            <div class="row justify-content-center m-t-10 b-t-default m-l-0 m-r-0">
                                <div class="col m-t-15">
                                    <h6 class="text-muted">Sueldo</h6>
                                    <h6>$ {{auth()->user()->sueldo}}</h6>
                                </div>
                                <div class="col m-t-15">
                                    <h6 class="text-muted">Porcentaje de Ganancia</h6>
                                    <h6>{{auth()->user()->ganancia}} %</h6>
                                </div>
                                <div class="col m-t-15">
                                    <h6 class="text-muted">Forma de Pago</h6>
                                    <h6>{{auth()->user()->tipoPago}}</h6>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-success btn-block" href="{{route('perfil')}}">Ver mi perfil</a>
                    </div>
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>

    <!-- @ include('nueva.modal.inicio') -->
@endsection
@section('js')

    <!-- custom-chart js -->
    <script>

    </script>
@endsection
