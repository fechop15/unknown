<!DOCTYPE html>
<html lang="es">

<head>

    <title>Mi Negocio</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content=""/>
    <meta name="keywords" content="">
    <meta name="author" content="GatewayTI"/>
    <!-- Favicon icon -->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="/assets/css/style.css">


</head>
<body>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper align-items-stretch aut-bg-img">
    <div class="flex-grow-1">
        <div class="h-100 d-md-flex align-items-center auth-side-img">
            <div class="col-sm-10 auth-content w-auto">
                <img src="assets/images/auth/auth-logo.png" alt="" class="img-fluid">
                <h1 class="text-white my-4">Bienvenido A la Plataforma <strong>Mi Negocio!</strong></h1>
                <h4 class="text-white font-weight-normal">Signin to your account and get explore the Able pro Dashboard
                    Template.<br/>Do not forget to play with live customizer</h4>
            </div>
        </div>
        <div class="auth-side-form">
            <div class=" auth-content">
                <img src="assets/images/auth/auth-logo-dark.png" alt=""
                     class="img-fluid mb-4 d-block d-xl-none d-lg-none">
                <h3 class="mb-4 f-w-400">Entrar</h3>
                <form class="md-float-material" action="{{route('login')}}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group mb-3">
                        <label class="floating-label" for="Email">Correo Electronico</label>
                        <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" id="Email"
                               value="{{ old('email') }}">
                    </div>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <div class="form-group mb-4">
                        <label class="floating-label" for="Password">Contraseña</label>
                        <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password" id="Password" placeholder="">
                    </div>
                    {!! $errors -> first('email','
                            <div class="alert alert-danger" role="alert">
							:message
						    </div>') !!}

                    {!! $errors -> first('password','
                            <div class="alert alert-danger" role="alert">
							:message
						    </div>') !!}

                    {!! $errors -> first('loginError','
                            <div class="alert alert-danger" role="alert">
							:message
						    </div>') !!}
                    <div class="custom-control custom-checkbox text-left mb-4 mt-2">
                        <input type="checkbox" class="custom-control-input" id="customCheck1"
                               name="remember">
                        <label class="custom-control-label" for="customCheck1">Recordar datos.</label>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary mb-4">Entrar</button>
                </form>

                <div class="text-center">
                    <p class="mb-2 mt-4 text-muted">Forgot password? <a href="auth-reset-password-img-side.html"
                                                                        class="f-w-400">Reset</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="/assets/js/vendor-all.min.js"></script>
<script src="/assets/js/plugins/bootstrap.min.js"></script>
<script src="/assets/js/ripple.js"></script>
<script src="/assets/js/pcoded.min.js"></script>


</body>

</html>
