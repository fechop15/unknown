@extends('nueva.layout.Dashboard')
@section('page')


    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5>{{auth()->user()->empresa->nombre}}</h5>
                            </div>
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="feather icon-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="#">Empresa</a></li>
                                <li class="breadcrumb-item"><a href="#!">Perfil</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-pills bg-white" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active text-uppercase" id="user1-tab" data-toggle="tab"
                                       href="#personal" role="tab" aria-controls="user1"
                                       aria-selected="true">Informacion General</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user2-tab" data-toggle="tab" href="#project"
                                       role="tab" aria-controls="user2" aria-selected="false">Historial</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user3-tab" data-toggle="tab"
                                       href="#questions"
                                       role="tab" aria-controls="user3" aria-selected="false">Pagos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="user3-tab" data-toggle="tab" href="#members"
                                       role="tab" aria-controls="user3" aria-selected="false">Cuenta</a>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header"><h5 class="card-header-text">Sobre Mi</h5>
                                    {{--<button id="edit-btn" type="button"--}}
                                    {{--class="btn btn-primary waves-effect waves-light f-right">--}}
                                    {{--<i class="icofont icofont-edit"></i>--}}
                                    {{--</button>--}}
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombre de la Empresa</th>
                                                                    <td>{{ auth()->user()->empresa->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Representante</th>
                                                                    <td>{{ auth()->user()->empresa->representante}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nit</th>
                                                                    <td>{{ auth()->user()->empresa->nit}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Contacto</th>
                                                                    <td>{{ auth()->user()->empresa->telefono}}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Cuota</th>
                                                                    <td>{{auth()->user()->empresa->cuota}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Estado</th>
                                                                    <td> {{auth()->user()->empresa->estado}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">*****</th>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">*****</th>
                                                                    <td>*****</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                    <!-- end of view-info -->
                                    <!-- end of view-info -->
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Produccion</h5>
                                            <a style="float: right;padding-right: 30px">Total Produccion: $
                                                <strong id="total">0</strong>
                                            </a>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablaHistorial"
                                                       style="width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Nombre Paciente</th>
                                                        <th>Servicio</th>
                                                        <th>Precio</th>
                                                        <th>Ganancia</th>
                                                        <th>Fecha Registro</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_dia">Dia</a>
                                                    <div class="slide"></div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#profile3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaDia"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMes"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                    <div class="container">
                                                        <br>
                                                        <div class='col-md-12'>
                                                            <div class="form-group">
                                                                <label for="">Desde</label>
                                                                <div class="input-group date input-group-date-custom">
                                                                    <input type="text" class="form-control">
                                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-12'>
                                                            <div class="form-group">
                                                                <label for="">Hasta</label>
                                                                <div class="input-group date input-group-date-custom">
                                                                    <input type="text" class="form-control">
                                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block waves-effect"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title=".btn-success .btn-block">Buscar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Pagos</h5>
                                            <a style="float: right;padding-right: 30px">
                                                <strong id="nota"></strong>
                                            </a>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablapagos" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Descripcion</th>
                                                        <th>Valor</th>
                                                        <th>Tipo</th>
                                                        <th>Mes Pagado</th>
                                                        <th>Fecha</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMesPagos"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end of row -->
                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                            <!-- end of row -->
                            <section class="panels-wells">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-header-text">Color Representativo</h5>
                                            </div>
                                            <!-- end of card-header  -->
                                            <div class="card-block">
                                                <h6>Color</h6>
                                                <input type="text" id="hue-demo" class="form-control demo"
                                                       data-control="hue"
                                                       value="{{auth()->user()->empresa->color}}">
                                                <button type="button"
                                                        class="btn btn-primary waves-effect waves-light"
                                                        onclick="cambiarColor()">Confirmar
                                                </button>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5 class="card-header-text">Imagen de perfil</h5>
                                            </div>
                                            <!-- end of card-header  -->
                                            <div class="card-block" style="background: black">
                                                <div class="change-profile text-center">
                                                    <div class="dropdown w-auto d-inline-block">
                                                        <div class="profile-dp">
                                                            <div class="position-relative d-inline-block">
                                                                <img class="img-fluid"
                                                                     src="/images/empresas/{{auth()->user()->empresa->logo}}"
                                                                     alt="User image" id="perfil"
                                                                     style="height: 125px;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="text-center">
                                                    <button type="button"
                                                            class="btn btn-primary waves-effect waves-light"
                                                            id="editar">Cambiar
                                                    </button>
                                                </div>
                                                <form id="subirImagen">
                                                    <input type="file" id="avatar" name="avatar"
                                                           style="display: none" accept="image/*">
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </section>
                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>
            <!-- [ Main Content ] end -->
        </div>
    </div>


@endsection
@section('js')

    <script>
        var IDCLIENTE = '{!! auth()->user()->id !!}';
        var FECHA = null;
        var FECHAH = null;
        var TABLA = null;
        var TABLA2 = null;
        var COMISION = '{!! auth()->user()->ganancia !!}';
        estadoCredito();

        $("#editar").click(function () {
            $("#avatar").click();
        });

        $('#avatar').change(function () {
            var data = new FormData();
            jQuery.each(jQuery('#avatar')[0].files, function (i, file) {
                data.append('logo', file);
            });
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: '/cambir-empresa-logo',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/empresas/' + response.imagen);
                    $("#logoEmpresa").attr("src", '/images/empresas/' + response.imagen);
                    $("#logoEmpresaThumb").attr("src", '/images/empresas/' + response.imagen);
                    notify(response.msg, 'success');

                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        function cambiarPass() {
            if ($("#pass").val() != $("#pass2").val()) {
                $("#Perror").html("Las Contraseña No Son Iguales");
                $("#Perror").show();
                return;
            }
            $("#Perror").hide();

            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    passOld: $("#passOld").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {
                    notify('Contraseña Actualizada', 'success');
                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function notify(message, type) {
            $.notify({
                message: message,
                icon: 'feather icon-bell',
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaMesPagos').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });
            //rango//
            $('#datetimepicker6').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true
            });
            $('#datetimepicker7').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                FECHAH = $("#fechaDia").data("date");
                TABLA2.ajax.reload();

                //gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHAH = $("#fechaMes").data("date");
                TABLA2.ajax.reload();

                //gerRegistrosMes($("#fechaMes").data("date"));
            });

            $('#fechaMesPagos').on('dp.change', function (e) {
                FECHA = $("#fechaMesPagos").data("date");
                TABLA.ajax.reload();
                console.log($("#fechaMesPagos").data("date"));

            });
            FECHA = $("#fechaMesPagos").data("date");
            FECHAH = $("#fechaDia").data("date");

            TABLA = $('#tablapagos').DataTable({
                "ajax": {
                    "url": "/get-pagos-empleado",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.empleado = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + data.msg[item].valor.toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Mes: data.msg[item].mes,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Mes"},
                    {data: "Fecha"},
                ],
            });

            TABLA2 = $('#tablaHistorial').DataTable({
                "ajax": {
                    "url": "/get-produccion-mes",
                    "data": function (d) {
                        d.fecha = FECHAH;
                        d.id = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "GET",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        var total = 0;
                        for (var item in data.msg) {
                            var ganancia = data.msg[item].precio * (COMISION / 100);
                            total += ganancia;
                            var itemJson = {
                                nombre: data.msg[item].nombre,
                                servicio: data.msg[item].servicio,
                                precio: '$' + data.msg[item].precio.toLocaleString(),
                                ganancia: '$' + ganancia.toLocaleString(),
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson);
                        }
                        $('#total').html(total.toLocaleString())

                        return json;
                    }
                },
                columns: [
                    {data: "nombre"},
                    {data: "servicio"},
                    {data: "precio"},
                    {data: "ganancia"},
                    {data: "Fecha"},
                ],
            });


            function calcularToral() {
                var total = 0;

                for (var item in REGISTROS) {
                    total += parseInt(REGISTROS[item].precio);
                }

                total = total * (COMISION / 100);
                $('#total').html(total.toLocaleString())

            }

            $("#select_dia").click(function () {
                //gerRegistros($("#fechaDia").data("date"));
                FECHAH = $("#fechaDia").data("date");
                TABLA2.ajax.reload();
                console.log($("#fechaMesPagos").data("date"));
                // calcularToral();

            });

            $("#select_mes").click(function () {
                //gerRegistrosMes($("#fechaMes").data("date"));
                FECHAH = $("#fechaMes").data("date");
                TABLA2.ajax.reload();
                console.log($("#fechaMes").data("date"));
                //calcularToral();

            });
        });

        function estadoCredito() {
            $.post(
                "/get-estado-credito-empleado", {
                    empleado: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                var valor = data.avance - data.abono - data.descuento;
                if (valor > 0) {
                    $("#nota").html("Actualmente Debe: $" + valor.toLocaleString());
                } else {
                    $("#nota").html("Actualmente Debe: $0");
                }
            });
        }

        'use strict';
        $(document).ready(function () {

            $('.demo').each(function () {
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    defaultValue: $(this).attr('data-defaultValue') || '',
                    format: $(this).attr('data-format') || 'hex',
                    keywords: $(this).attr('data-keywords') || '',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: $(this).attr('data-letterCase') || 'lowercase',
                    opacity: $(this).attr('data-opacity'),
                    position: $(this).attr('data-position') || 'bottom',
                    swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                    change: function (value, opacity) {
                        if (!value) return;
                        if (opacity) value += ', ' + opacity;
                        if (typeof console === 'object') {
                        }
                    },
                    theme: 'bootstrap'
                });
            });
        });
    </script>


@endsection

