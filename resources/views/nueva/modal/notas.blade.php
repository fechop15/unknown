<div class="modal fade bd-example-modal-lg" id="modal-4" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-purple">
                <h5 class="modal-title h4" id="myLargeModalLabel" style="color: white;">Nueva Nota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-nota">
                    <div class="row">

                        <div class="col-md-12">
                            <label class="text-c-purple">Titulo:</label>
                            <input type="text" class="form-control" id="titulo" placeholder="Titulo De la nota">
                        </div>

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Descripcion:</label>
                            <textarea id='descripcion' class="form-control" style="margin-top: 30px;"
                                      placeholder="Descripcion"></textarea>
                        </div>

                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert" id="error" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>

                <button class="btn btn-primary m-2" type="button" id="guardarNota">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Guardando...</span>
                    <span class="btn-text">Guardar</span>
                </button>

                <button class="btn btn-primary m-2" type="button" id="actualizarNota">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Actualizando...</span>
                    <span class="btn-text">Actualizar</span>
                </button>

            </div>
        </div>
    </div>
</div>
