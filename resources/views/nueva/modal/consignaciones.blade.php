<div class="modal fade bd-example-modal-lg" id="modal-8" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-purple">
                <h5 class="modal-title h4" id="myLargeModalLabel" style="color: white;">Registrar Movimientos
                    Tarjeta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-consignaciones">
                    <div class="row">

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Valor </label>
                            <input type="number" class="form-control" id="valor" placeholder="valor"
                                   onkeyup="disponibilidad(this)">
                        </div>

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Descripcion:</label>
                            <textarea id='descripcion' class="form-control" style="margin-top: 30px;"
                                      placeholder="Descripcion"></textarea>
                            <label class="form-control-label" id="nota"></label>
                        </div>

                        <div class="col-md-12" style="padding-bottom: 15px;">
                            <label class="text-c-purple">Tipo de Movimiento</label>
                            <select id="tipo" class="form-control" onchange="verificar()">
                                <option value="Salida">Salida</option>
                                <option value="Entrada">Entrada</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert" id="error" style="display: none">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <h4 class="modal-title text-center">Total: $<strong id="totalConsignacion">0</strong></h4>

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-primary m-2" type="button" id="guardarConsignaion">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Guardando...</span>
                    <span class="btn-text">Guardar</span>
                </button>
            </div>
        </div>
    </div>
</div>
