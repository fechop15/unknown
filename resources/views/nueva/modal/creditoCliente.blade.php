<div class="modal fade bd-example-modal-lg" id="modal-recarga" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-purple">
                <h5 class="modal-title h4" id="myLargeModalLabel" style="color: white;">Recarga a cuenta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-recarga">
                    <div class="row">

                        <div class="col-md-12">
                            <label class="form-control-label">Valor </label>
                            <input type="number" class="form-control" id="valorRecarga" placeholder="valor"
                                   onkeyup="disponibilidad(this)">
                        </div>

                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert" id="errorRecarga" style="display: none">
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <h4 class="modal-title text-center">Total: $<strong id="totalARecargar">0</strong></h4>

                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>

                <button class="btn btn-primary m-2" type="button" id="guardarCreditoCliente">
                    <span class="spinner-border spinner-border-sm" role="status"></span>
                    <span class="load-text">Guardando...</span>
                    <span class="btn-text">Guardar</span>
                </button>

            </div>
        </div>
    </div>
</div>
