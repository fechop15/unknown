<div class="modal fade bd-example-modal-lg" id="modal-empresas" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-purple">
                <h5 class="modal-title h4" id="myLargeModalLabel" style="color: white;">Empresas Contratadas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-produccion">
                    <input type="hidden" id="id">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="tablaEmpresasContratadas" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center txt-primary">Nombre</th>
                                        <th class="text-center txt-primary">Pago</th>
                                    </tr>
                                    </thead>
                                </table>
                                <!-- end of table -->
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
