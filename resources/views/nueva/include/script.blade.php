<script src="/assets/js/vendor-all.min.js"></script>
<script src="/assets/js/plugins/bootstrap.min.js"></script>
<script src="/assets/js/ripple.js"></script>
<script src="/assets/js/pcoded.min.js"></script>

<!-- Apex Chart <script src="/assets/js/menu-setting.min.js"></script>-->
<script src="/assets/js/plugins/apexcharts.min.js"></script>
<script src="/assets/js/plugins/bootstrap-notify.min.js"></script>

<!-- datatable Js -->
<script src="/assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="/assets/js/plugins/dataTables.bootstrap4.min.js"></script>
<script src="/assets/js/plugins/dataTables.fixedColumns.min.js"></script>

<!-- sweet alert Js -->
<script src="/assets/js/plugins/sweetalert.min.js"></script>

<!-- select2 Js -->
<script src="/assets/js/plugins/select2.full.min.js"></script>
<!-- Date picker.js -->
<script src="/plugins/datepicker/js/moment-with-locales.min.js"></script>
<script src="/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>--}}
<!-- Bootstrap Datepicker js -->
<script type="text/javascript" src="/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- minicolors Js -->
<script src="/assets/js/plugins/jquery.minicolors.min.js"></script>
@if(auth()->user()->id==1)
    <script>
        var TABLA = $('#tablaEmpresasContratadas').DataTable({
            "ajax": {
                "url": "/get-empresas",
                "data": function (d) {
                    d._token = $('meta[name="csrf-token"]').attr('content');
                },
                "type": "POST",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: "<a href='/dashboard/empresa/" + data.msg[item].id + "'>" + data.msg[item].nombre + "</a>",
                            Pago: "",
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Pago"},
            ],
        });


        function estadoCredito() {
            $.post(
                "/get-estado-credito-empleado", {
                    empleado: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
            });
        }
    </script>
@endif
