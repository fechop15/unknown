<nav class="pcoded-navbar menu-light menupos-fixed">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div ">

            <div class="">
                <div class="main-menu-header">
                    <img class="img-radius hei-60" src="/images/perfil/{{auth()->user()->imagen}}"
                         alt="User-Profile-Image">
                    <div class="user-details">
                        <div id="more-details" style="font-size:10px;">
                            {{ auth()->user()->nombre }}
                            <i class="fa fa-caret-down"></i></div>
                    </div>
                </div>
                <div class="collapse" id="nav-user-link">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="{{route('perfil')}}" data-toggle="tooltip"
                                                        title="Ver Perfil"><i class="feather icon-user"></i></a></li>
                        <li class="list-inline-item"><a href="{{route('logout')}}" data-toggle="tooltip" title="Salir"
                                                        class="text-danger"><i class="feather icon-power"></i></a></li>
                    </ul>
                </div>
            </div>

            <ul class="nav pcoded-inner-navbar ">
                <li class="nav-item pcoded-menu-caption">
                    <label>Navegacion</label>
                </li>

                <li class="nav-item {{ (request()->is('dashboard')) ? 'active' : '' }}">
                    <a href="{{route('dashboard')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="fas fa-home"></i>
                            </span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>
                @if(auth()->user()->rol_id == 2)
                    <li class="nav-item {{ (request()->is('producido/*')) ? 'active' : '' }}">
                        <a href="{{route('producido')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-aperture"></i>
                            </span>
                            <span class="pcoded-mtext">Producido</span>
                        </a>
                    </li>
                @endif

                <li class="nav-item {{ request()->route()->getName() === 'clientes'|| request()->route()->getName() === 'perfilCliente' ? ' active' : '' }}">
                    <a href="{{route('clientes')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="fas fa-user-clock"></i>
                            </span>
                        <span class="pcoded-mtext">Pacientes</span>
                    </a>
                </li>
                @if(auth()->user()->rol_id == 1)
                    <li class="nav-item pcoded-menu-caption">
                        <label>Administracion</label>
                    </li>

                    <li class="nav-item {{ request()->route()->getName() === 'empleados'}}">
                        <a href="{{route('empleados')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="fas fa-user-md"></i>
                            </span>
                            <span class="pcoded-mtext">Empleados</span>
                        </a>
                    </li>

                    <li class="nav-item {{ request()->route()->getName() === 'historico'}}">
                        <a href="{{route('historico')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="fas fa-history"></i>
                            </span>
                            <span class="pcoded-mtext">Historico</span>
                        </a>
                    </li>

                    <li class="nav-item pcoded-hasmenu {{
                request()->route()->getName() ==='fijos'||
                request()->route()->getName() ==='insumos'||
                request()->route()->getName() ==='otros'||
                request()->route()->getName() ==='diarios' ||
                request()->route()->getName() ==='nomina' ||
                request()->route()->getName() ==='credito' ||
                request()->route()->getName() === 'consignaciones' ? ' active' : '' }}">
                        <a href="#" class="nav-link "><span class="pcoded-micon"><i
                                    class="feather icon-menu"></i></span><span
                                class="pcoded-mtext">Financiero</span></a>
                        <ul class="pcoded-submenu">

                            <li class="pcoded-hasmenu {{
                request()->route()->getName() ==='fijos'||
                request()->route()->getName() ==='insumos'||
                request()->route()->getName() ==='otros'  ? ' active' : '' }} ">
                                <a href="#">Gastos</a>
                                <ul class="pcoded-submenu">
                                    <li class="{{ request()->route()->getName() === 'fijos' ? ' active' : '' }}"><a
                                            href="{{route('fijos')}}">Fijos</a></li>
                                    <li class="{{ request()->route()->getName() === 'insumos' ? ' active' : '' }}">
                                        <a
                                            href="{{route('insumos')}}">Insumo</a></li>
                                    <li class="{{ request()->route()->getName() === 'otros' ? ' active' : '' }}"><a
                                            href="{{route('otros')}}">Otros</a></li>
                                </ul>
                            </li>
                            <li class="pcoded-hasmenu {{ request()->route()->getName() ==='diarios' || request()->route()->getName() ==='nomina'|| request()->route()->getName() ==='credito' ? ' active' : '' }}">
                                <a href="#">Pagos</a>
                                <ul class="pcoded-submenu">
                                    <li class="{{ request()->route()->getName() === 'diarios' ? ' active' : '' }}">
                                        <a
                                            href="{{route('diarios')}}
                                                ">Doctores</a></li>
                                    <li class="{{ request()->route()->getName() === 'nomina' ? ' active' : '' }}"><a
                                            href="{{route('nomina')}}">Nomina</a></li>
                                    <li class="{{ request()->route()->getName() === 'credito' ? ' active' : '' }}">
                                        <a
                                            href="{{route('credito')}}">Credito</a></li>
                                </ul>
                            </li>
                            <li class="{{ request()->route()->getName() === 'consignaciones' ? ' active' : '' }}"><a
                                    href="{{route('consignaciones')}}">Consignaciones</a></li>
                        </ul>
                    </li>
                @endif
                @if(auth()->user()->id==1)
                    <li class="nav-item pcoded-menu-caption">
                        <label>Empresas</label>
                    </li>
                    <li class="nav-item {{ request()->route()->getName() === 'empresas'? ' active' : '' }}">
                        <a href="{{route('empresas')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-aperture"></i>
                            </span>
                            <span class="pcoded-mtext">Empresas</span>
                        </a>
                    </li>

                    <li class="nav-item {{ request()->route()->getName() === 'empresa'? ' active' : '' }}">
                        <a href="{{route('empresa')}}" class="nav-link ">
                            <span class="pcoded-micon">
                                <i class="feather icon-aperture"></i>
                            </span>
                            <span class="pcoded-mtext">Perfil</span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>


