<header class="navbar pcoded-header navbar-expand-lg navbar-light header-purple">


    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="#!" class="b-brand">
            <!-- ========   change your logo hear   ============ -->
            <img src="/images/empresas/{{auth()->user()->empresa->logo}}" alt="" class="logo" style="width: 125px;"
                 id="logoEmpresa">
            <img src="/images/empresas/{{auth()->user()->empresa->logo}}" alt="" class="logo-thumb"
                 style="width: 150px;" id="logoEmpresaThumb">
        </a>
        <a href="#!" class="mob-toggler">
            <i class="feather icon-more-vertical"></i>
        </a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            @if(auth()->user()->id==1)
                <li>
                    <a data-toggle="modal"
                       data-target="#modal-empresas" href="#"><i
                            class="icon feather icon-bell"></i></a>
                </li>
            @endif
            <li>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i
                            class="icon feather icon-bell"></i></a>
                    <div class="dropdown-menu dropdown-menu-right notification">
                        <div class="noti-head">
                            <h6 class="d-inline-block m-b-0">Notificaciones</h6>
                            <div class="float-right">
                                <a href="#!" class="m-r-10">Marcar leidos</a>
                                <a href="#!">Limpiar todo</a>
                            </div>
                        </div>
                        <div class="noti-footer">
                            <a href="#!">Ver todo</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown drp-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="feather icon-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-notification">
                        <div class="pro-head">
                            <img src="/images/perfil/{{auth()->user()->imagen}}" class="img-radius hei-40"
                                 alt="User-Profile-Image">
                            <span style="font-size:10px;">
                                 {{ auth()->user()->nombre }}
                            </span>
                            <a href="{{route('logout')}}" class="dud-logout" title="Logout">
                                <i class="feather icon-log-out"></i>
                            </a>
                        </div>
                        <ul class="pro-body">
                            <li><a href="{{route('perfil')}}" class="dropdown-item"><i class="feather icon-user"></i>
                                    Perfil</a></li>
                            {{--                            <li><a href="email_inbox.html" class="dropdown-item"><i class="feather icon-mail"></i> My Messages</a></li>--}}
                            <li><a href="{{route('logout')}}" class="dropdown-item"><i class="feather icon-lock"></i>
                                    Logout</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</header>

@if(auth()->user()->id==1)
    @include("nueva.modal.empresas")
@endif
