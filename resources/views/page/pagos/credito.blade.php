@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Avances</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a> Pagos</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!"> Avanves de pagos</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Avances de pagos</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    data-toggle="modal"
                                    data-target="#modal">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover" id="tablaAvances" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Tipo</th>
                                        <th>Fecha</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="row m-b-30 dashboard-header">
                        <div class="col-lg3 col-sm-6-">
                            <div class="col-sm-11 card dashboard-product">
                                <span>Produccion</span>
                                <h2 class="dashboard-total-products counter" id="producido">$0</h2>
                                <span class="label label-success">Mes</span> Recaudado Efectivo
                                <div class="side-box bg-success">
                                    <i class="icon-social-soundcloud"></i>
                                </div>
                            </div>
                            <div class="col-sm-11 card">
                                <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                                <div class="card-block">
                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                        <li class="nav-item active">
                                            <a class="nav-link active" data-toggle="tab" href="#profile3" role="tab"
                                               id="select_mes">Mes</a>
                                            <div class="slide"></div>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                        {{--<div class="slide"></div>--}}
                                        {{--</li>--}}
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages3" role="tabpanel">
                                            <div class="container">
                                                <br>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Desde</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Hasta</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-block waves-effect"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title=".btn-success .btn-block">Buscar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

    @include('modals.credito')

@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var TOTALAPAGAR = 0;
        var REGISTROS = [];
        var USUARIOS = [];
        var FECHA = null;
        var TABLA = null;

        $(document).ready(function () {
            getProduccion();
            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });
            FECHA = $("#fechaMes").data("date");

            TABLA = $('#tablaAvances').DataTable({
                "ajax": {
                    "url": "/get-credito-empleados",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Id: data.msg[item].id,
                                Empleado: data.msg[item].empleado,
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Empleado"},
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Fecha"},
                ],
            });


            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHA = $("#fechaMes").data("date");
                TABLA.ajax.reload();
            });

            $('#select-usuarios').on('change', function (e) {
                console.log($("#select-usuarios").val());
                estadoCredito($("#select-usuarios").val());
            });

            $.post(
                "/get-usuarios-by-rol-pago", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    rol: 'Empleado',
                    pago: "Mensual"
                }
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });
                $("#select-usuarios").val("").trigger('change');

            });

            function estadoCredito(empleado) {
                $.post(
                    "/get-estado-credito-empleado", {
                        empleado: empleado,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    var valor = data.avance - data.abono - data.descuento;
                    if (valor > 0) {
                        $("#nota").html("Actualmente Debe: $" + valor.toLocaleString());
                    } else {
                        $("#nota").html("");
                    }
                });
            }

            function calcularToralAPagar(total) {
                var valor = parseInt(total.sueldoFijo) + parseInt(total.totalGanancia);
                TOTALAPAGAR = valor;
                $('#totalAPagar').html(valor.toLocaleString());

                if (TOTALAPAGAR > 0 && TOTALAPAGAR < TOTALDISPONIBLE ) {
                    $("#guardar").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    if (TOTALAPAGAR > TOTALDISPONIBLE) {
                        $("#guardar").hide();
                        $("#error").show();
                        $("#error").html("No Hay Fondos para pagos");
                    } else {
                        $("#guardar").hide();
                        $("#error").show();
                        $("#error").html("No Hay Pagos Para Este Doctor");
                    }

                }

            }

        });

        function disponibilidad(total) {


            if (TOTALDISPONIBLE >= total.value || $("#tipo").val()=="Abono") {
                $("#guardar").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardar").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }

            var totalAvance = parseInt(total.value);
            $('#totalAPagar').html(totalAvance.toLocaleString())
        }

        function calcularToral(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())

        }

        function getProduccion() {
            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });
        }

        function guardar() {

            $.ajax({
                url: '/credito-empleado',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    valor: $('#valor').val(),
                    tipo: $('#tipo').val(),
                    empleado: $('#select-usuarios').val(),
                    _token: $('meta[name="csrf-token"]').attr('content'),
                },

            }).done(function (response) {

                console.log(response);
                TABLA.ajax.reload();
                getProduccion();
                $('#descripcion').val("");
                $('#valor').val("");
                $('#modal').modal('hide');

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });

            });

        }

    </script>


@endsection
