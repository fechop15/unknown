@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Pago Diarios A Doctores</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a>Pagos</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!"> Diario A Doctores</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Pago Diarios A Doctores</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    data-toggle="modal"
                                    data-target="#modal-7">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                            <a style="float: right;padding-right: 30px">Total: $
                                <strong id="total">0</strong>
                            </a>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Mes Pagado</th>
                                        <th>Fecha Pago</th>
                                    </tr>
                                    </thead>
                                    <tbody id="nomina">

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="row m-b-30 dashboard-header">
                        <div class="col-lg3 col-sm-6-">
                            <div class="col-sm-11 card dashboard-product">
                                <span>Produccion</span>
                                <h2 class="dashboard-total-products counter" id="producido">$0</h2>
                                <span class="label label-success">Hoy</span> Recaudado Efectivo
                                <div class="side-box bg-success">
                                    <i class="icon-social-soundcloud"></i>
                                </div>
                            </div>
                            <div class="col-sm-11 card">
                                <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                                <div class="card-block">
                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                               id="select_dia">Dia</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                               id="select_mes">Mes</a>
                                            <div class="slide"></div>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                        {{--<div class="slide"></div>--}}
                                        {{--</li>--}}
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaDia"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages3" role="tabpanel">
                                            <div class="container">
                                                <br>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Desde</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Hasta</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-block waves-effect"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title=".btn-success .btn-block">Buscar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>
    @include('modals.modal7')
@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var TOTALAPAGAR = 0;
        var REGISTROS = [];
        var USUARIOS = [];

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });

            $('#fechaDiaNomina').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });


            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                gerRegistrosMes($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                gerRegistrosMes($("#fechaMes").data("date")+"-%");
            });

            $('#fechaDiaNomina').on('dp.change', function (e) {
                console.log($("#fechaDiaNomina").data("date"));
                console.log($("#select-usuarios").val());
                getToatalPagoMes($("#fechaDiaNomina").data("date"), $("#select-usuarios").val());
            });

            $('#select-usuarios').on('change', function (e) {
                console.log($("#fechaDiaNomina").data("date"));
                console.log($("#select-usuarios").val());
                getToatalPagoMes($("#fechaDiaNomina").data("date"), $("#select-usuarios").val());

            });

            $.post(
                "/get-usuarios-by-rol-pago", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    rol: 'Empleado',
                    pago: "Diario"
                }
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal-7"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });
                $("#select-usuarios").val("").trigger('change');

            });

            gerRegistrosMes($("#fechaDia").data("date"));

            function calcularToralAPagar(total) {
                var valor = parseInt(total.totalGanancia);
                TOTALAPAGAR = valor;
                $('#totalAPagar').html(valor.toLocaleString());
                if (TOTALAPAGAR > 0) {
                    $("#guardar").show();
                    $("#error").hide();
                    $("#error").html("");

                } else {
                    $("#guardar").hide();
                    $("#error").show();
                    $("#error").html("No Hay Pagos Para Este Doctor Hoy");
                }
            }

            function getToatalPagoMes(mes, empleado) {
                if (empleado == null) {
                    return;
                }
                $.post(
                    "/get-total-pago-mes", {
                        fecha: mes,
                        empleado: empleado,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    calcularToralAPagar(data.msg);
                });
            }

            function gerRegistrosMes(fecha) {
                $('#nomina').html("");
                $('#total').html("0")
                REGISTROS = [];
                $.post(
                    "/get-nomina", {
                        fecha: fecha,
                        razon: "Gasto Insumo",
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#nomina').html("");
                    for (var item in data.msg) {
                        nomina(data.msg[item], 'agregar');
                    }
                    getProduccion();
                    calcularToral();
                    //console.log(opciones)
                });
            }


            $("#select_dia").click(function () {
                gerRegistrosMes($("#fechaDia").data("date"));
            });

            $("#select_mes").click(function () {
                gerRegistrosMes($("#fechaMes").data("date"));
            });
        });

        function disponibilidad(total) {
            if (TOTALDISPONIBLE >= total.value && TOTALAPAGAR > 0) {
                $("#guardar").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardar").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }
        }

        function calcularToral(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())
        }

        function getProduccion() {
            $.get(
                "/produccion-dia"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });
        }

        function guardar() {

            $.ajax({
                url: '/crear-pago-nomina',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    fecha: $("#fechaDiaNomina").data("date"),
                    valor: TOTALAPAGAR,
                    empleado: $('#select-usuarios').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                nomina(response.msg, 'agregar');
                $('#descripcion').val("");
                $('#valor').val("");
                $('#modal-7').modal('hide');
                getProduccion();
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });

            });

        }

        function nomina(data, op) {
            var html = ' <tr id="prod_' + data.id + '">' +
                '        <td class="text-nowrap">' + data.empleado + '</td>' +
                '        <td class="text-nowrap">' + data.descripcion + '</td>' +
                '        <td class="text-nowrap">$' + parseInt(data.valor).toLocaleString() + '</td>' +
                '        <td class="text-nowrap">' + data.fecha + '</td>' +
                '        <td class="text-nowrap">' + data.fechaPago + '</td>' +
                '        </tr>';

            if (op == "agregar") {
                $('#nomina').prepend(html);
                REGISTROS.push(data);
            } else if (op == "remplazar") {
                $('#prod_' + data.id).replaceWith(html);
            }

        }

        function calcularToral() {
            var total = 0;

            for (var item in REGISTROS) {
                total += parseInt(REGISTROS[item].valor);
            }

            $('#total').html(total.toLocaleString())
        }

    </script>


@endsection
