@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Historial</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Historico</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Produccion</h5>
                            <button type="button" class="btn btn-primary" style="float: right" data-toggle="modal"
                                    data-target="#modal-2-produccion">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                            <a style="float: right;padding-right: 30px">Efectivo: $
                                <strong id="totalE">0</strong>
                            </a>
                            <a style="float: right;padding-right: 30px">Tarjeta: $
                                <strong id="totalT">0</strong>
                            </a>
                            <a style="float: right;padding-right: 30px">Mi Saldo: $
                                <strong id="totalM">0</strong>
                            </a>
                            <a style="float: right;padding-right: 30px">Total: $
                                <strong id="total">0</strong>
                            </a>
                            <a style="float: right;padding-right: 30px">Total con Mi Saldo: $
                                <strong id="totalMM">0</strong>
                            </a>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Servicio</th>
                                        <th>Atendido Por</th>
                                        <th>Precio</th>
                                        <th>Metodo De Pago</th>
                                        <th>Fecha Creacion</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="produccion">

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="row m-b-30 dashboard-header">
                        <div class="col-sm-11 card dashboard-product">
                            <span>Produccion</span>
                            <h2 class="dashboard-total-products counter" id="producidoT">$0</h2>
                            <span class="label label-success"></span> Recaudado Virtual
                            <h2 class="dashboard-total-products counter" id="producido">$0</h2>
                            <span class="label label-success">Mes</span> Recaudado Efectivo
                            <h2 class="dashboard-total-products counter" id="totalSC">$0</h2>
                            Saldo Cliente
                            <div class="side-box bg-success">
                                <i class="icon-social-soundcloud"></i>
                            </div>
                        </div>

                        <div class="col-lg3 col-sm-6-">
                            <div class="col-sm-11 card">
                                <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                                <div class="card-block">
                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                               id="select_dia">Dia</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                               id="select_mes">Mes</a>
                                            <div class="slide"></div>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                        {{--<div class="slide"></div>--}}
                                        {{--</li>--}}
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaDia"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages3" role="tabpanel">
                                            <div class="container">
                                                <br>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Desde</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Hasta</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-block waves-effect"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title=".btn-success .btn-block">Buscar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>
    @include('modals.modal2')

@endsection

@section('js')

    <script>

        var opciones = [];
        var REGISTROS = [];
        var USUARIOS = [];
        var CLIENTES = [];

        $(document).ready(function () {
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Empleado'}
            ).done(function (data) {
                //console.log(data);

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal-2-produccion"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });

            });
            $.get(
                "/get-clientes"
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre + " - " + data.msg[item].cedula
                    };
                    CLIENTES.push(itemSelect2)
                }
                $('#select-cliente').select2({
                    dropdownParent: $("#modal-2-produccion"),
                    minimumResultsForSearch: 5,
                    placeholder: "Bucar Cliente",
                    allowClear: true,
                    data: CLIENTES
                });

            });

            $.get(
                "/get-produccion"
            ).done(function (data) {
                console.log(data);
                REGISTROS = [];
                for (var item in data.msg) {
                    produccion(data.msg[item], 'agregar');
                }
                console.log(opciones)

            });

            $('#select-servicios').on('select2:select', function (e) {
                calcularToral($('#select-servicios').select2("val"));
            });

            $('#select-cliente').on('change', function (e) {
                consultarSaldoCuenta($('#select-cliente').val());
            });
            getProduccion();

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });
            //rango//
            $('#datetimepicker6').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true
            });

            $('#datetimepicker7').datetimepicker({
                format: 'DD/MM/YYYY',
                inline: true,
                sideBySide: true,
                useCurrent: false //Important! See issue #1075
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                gerRegistrosMes($("#fechaMes").data("date"));
            });

            gerRegistros($("#fechaDia").data("date"));

            function consultarSaldoCuenta(IDCLIENTE) {
                $.post(
                    "/get-estado-credito-cliente", {
                        cliente: IDCLIENTE,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    var valor = data.recarga - data.pago;
                    SALDO = valor;
                    if (valor >= 0) {
                        $("#notaSaldo").html("Saldo Actual: $" + valor.toLocaleString());
                    } else {
                        $("#notaSaldo").html("Actualmente Debe: $0");
                    }
                });
            }

            function gerRegistros(fecha) {
                $('#produccion').html("");
                $('#total').html("0")
                REGISTROS = [];
                $.get(
                    "/get-produccion-fecha", {
                        fecha: fecha,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#produccion').html("");
                    REGISTROS = [];
                    for (var item in data.msg) {
                        produccion(data.msg[item], 'agregar');
                    }
                    calcularToral2();

                });

                creditosCliente(fecha);

            }

            function gerRegistrosMes(fecha) {
                $('#produccion').html("");
                $('#total').html("0");
                REGISTROS = [];

                $.get(
                    "/get-produccion-mes", {
                        fecha: fecha,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#produccion').html("");
                    REGISTROS = [];
                    for (var item in data.msg) {
                        produccion(data.msg[item], 'agregar');
                    }
                    calcularToral2();
                    //console.log(opciones)
                });

                creditosCliente(fecha);
            }

            function creditosCliente(fecha) {
                $.get(
                    "/total-creditos-clientes", {
                        fecha: fecha,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $("#totalSC").html(parseInt(data.msg).toLocaleString());
                });
            }

            function calcularToral2() {
                var total = 0;
                var totalE = 0;
                var totalT = 0;
                var totalM = 0;
                var totalMM = 0;

                for (var item in REGISTROS) {
                    totalMM += parseInt(REGISTROS[item].precio);

                    if (REGISTROS[item].metodoP == "Efectivo") {
                        totalE += parseInt(REGISTROS[item].precio);
                        total += parseInt(REGISTROS[item].precio);
                    } else if (REGISTROS[item].metodoP == "Tarjeta") {
                        totalT += parseInt(REGISTROS[item].precio);
                        total += parseInt(REGISTROS[item].precio);
                    } else {
                        totalM += parseInt(REGISTROS[item].precio);
                        console.log(REGISTROS[item].precio)
                    }
                }

                $('#total').html(total.toLocaleString());
                $('#totalE').html(totalE.toLocaleString());
                $('#totalT').html(totalT.toLocaleString());
                $('#totalM').html(totalM.toLocaleString());
                $('#totalMM').html(totalMM.toLocaleString());
            }

            $("#select_dia").click(function () {
                gerRegistros($("#fechaDia").data("date"));
            });

            $("#select_mes").click(function () {
                gerRegistrosMes($("#fechaMes").data("date"));
            });
        });

        function produccion(data, op) {
            var pago = '';
            if (data.metodoP == 'Efectivo') {
                pago = '<span class="label label-success">Efectivo</span>';
            } else if (data.metodoP == 'Tarjeta') {
                pago = '<span class="label label-info">Tarjeta</span>';
            } else {
                pago = '<span class="label label-warning">Mi Saldo</span>';
            }
            var precio = parseInt(data.precio);
            var html = ' <tr id="prod_' + data.id + '">' +
                '        <td class="text-nowrap">' + data.nombre + '</td>' +
                '        <td class="text-nowrap">' + data.servicio + '</td>' +
                '        <td class="text-nowrap">' + data.atendido + '</td>' +
                '        <td>$' + precio.toLocaleString() + '</td>' +
                '        <td class="text-nowrap">' + pago + '</td>' +
                '        <td class="text-nowrap">' + data.fecha + '</td>' +
                '        </tr>';

            if (op == "agregar") {
                $('#produccion').prepend(html);
                REGISTROS.push(data);
            } else if (op == "remplazar") {
                $('#prod_' + data.id).replaceWith(html);
            }

        }

        function calcularToral(total) {
            var valor = parseInt(total.value);
            $('#totalRegistroProduccion').html(valor.toLocaleString());
        }

        function guardarProduccion() {
            $("#produccion-error").html("");

            $.ajax({
                url: '/crear-produccion',
                type: 'POST',
                data: {
                    cliente_id: $('#select-cliente').val(),
                    servicio: $('#Pservicio').val(),
                    empleado_id: $('#select-usuarios').val(),
                    precio: $('#Pprecio').val(),
                    metodo_pago: $('#PmetodoPago').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                console.log("respuesta" + response);
                if (response.status == 'error') {
                    $("#produccion-error").html(response.msg);
                    return;
                }
                //produccion(response.msg, 'agregar');
                getProduccion();
                location.reload();
                $('#modal-2-produccion').modal('hide');
                $("#notaSaldo").html("");
                $("#produccion-error").html("");
                $('#form-produccion')[0].reset();
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#produccion-error").html(value[0]);
                });
            });

        }

        function getProduccion() {
            $.get(
                "/produccion-tarjeta"
            ).done(function (data) {
                console.log(data);
                calcularToralTarjeta(data.msg);
            });

            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToralEfectivo(data.msg);
            });
        }

        function calcularToralEfectivo(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())
        }

        function calcularToralTarjeta(data) {
            var total = data.totalTarjeta + data.totalTarjetaEntrada - data.totalTarjetaSalida;
            TOTALDISPONIBLE = total;
            $('#producidoT').html("$" + total.toLocaleString())
        }
    </script>


@endsection