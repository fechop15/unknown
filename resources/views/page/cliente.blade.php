@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>{{$cliente->nombre}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('clientes')}}">Clientes</a>
                        </li>
                        <li class="breadcrumb-item">Perfil
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Header end -->
            <div class="row">
                <!-- start col-lg-9 -->
                <div class="col-xl-12 col-lg-12">
                    <!-- Nav tabs -->
                    <div class="tab-header">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Informacion
                                    Personal</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#project" role="tab">Historial</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#questions" role="tab">Anotaciones</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#members" role="tab">Saldo</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Sobre Mi</h5>
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombre Completo</th>
                                                                    <td>{{$cliente->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Identificacion</th>
                                                                    <td>{{$cliente->cedula}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Telefono</th>
                                                                    <td>{{$cliente->telefono}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Edad</th>
                                                                    <td>{{$cliente->edad}}</td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Ciudad</th>
                                                                    <td>{{$cliente->ciudad}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Direccion</th>
                                                                    <td> {{$cliente->direccion}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">Ocupacion</th>
                                                                    <td>{{$cliente->ocupacion}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Saldo</th>
                                                                    <td id="credito"></td>
                                                                </tr>

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Historial Clinico</h5>
                                </div>
                                <!-- end of card-header  -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="project-table">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>

                                                    <tr>
                                                        <th class="text-center txt-primary pro-pic">Servicio</th>
                                                        <th class="text-center txt-primary">Valor</th>
                                                        <th class="text-center txt-primary">Fecha</th>
                                                        <th class="text-center txt-primary">Metodo de pago</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                    @foreach($cliente->facturas->reverse() as $factura)
                                                        <tr>
                                                            <td>{{$factura->servicio}}</td>
                                                            <td>${{(number_format( $factura->precio , 0, '.', '.'))}}</td>
                                                            <td>{{date("d/m/Y h:i A", strtotime($factura->created_at))}}</td>
                                                            <td class="text-center">
                                                                @if($factura->metodo_pago=="Efectivo")
                                                                    <span class="label label-success m-t-20">{{$factura->metodo_pago}}</span>
                                                                @else
                                                                    <span class="label label-info m-t-20">{{$factura->metodo_pago}}</span>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                <!-- end of table -->
                                            </div>
                                            <!-- end of table responsive -->
                                        </div>
                                        <!-- end of project table -->
                                    </div>
                                    <!-- end of col-lg-12 -->
                                </div>
                                <!-- end of row -->
                            </div>
                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">
                            <section class="panels-wells">

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text">Notas del Paciente</h5>
                                        <button type="button"
                                                class="btn btn-primary waves-effect waves-light f-right"
                                                onclick="openModal()">
                                            + Nueva Nota
                                        </button>
                                    </div>
                                    <div class="card-block">
                                        <div class="row" id="notas">


                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Movimientos de Pagos</h5>
                                            <a style="float: right;padding-right: 30px">
                                                @if(auth()->user()->rol_id==1)
                                                <button type="button"
                                                        class="btn btn-primary waves-effect waves-light f-right"
                                                        onclick="openModalRecarga()">
                                                    Recargar
                                                </button>
                                                @endif
                                                <strong style="padding-right: 30px" id="nota"></strong>
                                            </a>

                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablapagos" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Descripcion</th>
                                                        <th>Valor</th>
                                                        <th>Tipo</th>
                                                        <th>Fecha</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMesPagos"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>

        </div>
        <!-- Container-fluid ends -->
    </div>


    @include('modals.modal4')
    @include('modals.creditoCliente')

@endsection

@section('js')

    <script>
        var IDCLIENTE = '{!! $cliente->id !!}';
        var IDNOTA = 0;
        var TABLA = 0;
        var FECHA = null;
        estadoCredito();
        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-notas", {_token: $('meta[name="csrf-token"]').attr('content'), cliente: IDCLIENTE}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    nota(data.msg[item], 'agregar');
                }

            });
            $('#fechaMesPagos').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });
            FECHA = $("#fechaMesPagos").data("date");

            TABLA = $('#tablapagos').DataTable({
                "ajax": {
                    "url": "/get-estado-credito-cliente",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.cliente = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + data.msg[item].valor.toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Fecha"},
                ],
            });

        });

        function guardarCreditoCliente() {
            $.ajax({
                url: '/credito-cliente',
                type: 'POST',
                data: {
                    valor: $('#valorRecarga').val().toUpperCase(),
                    tipo: 'Recarga',
                    cliente: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                //console.log(response);
                $('#modal-recarga').modal('hide');
                TABLA.ajax.reload();
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#errorRecarga").html(value[0]);
                });
            });
        }

        function openModalRecarga() {
            $('#valorRecarga').val("");
            $('#totalARecargar').html("0");
            $('#modal-recarga').modal();

        }

        function disponibilidad(total){
            var totalAvance = parseInt(total.value);
            $('#totalARecargar').html(totalAvance.toLocaleString())
        }
        /---------------------------------/

        function guardar() {
            $.ajax({
                url: '/crear-nota',
                type: 'POST',
                data: {
                    titulo: $('#titulo').val().toUpperCase(),
                    descripcion: $('#descripcion').val(),
                    cliente_id: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                //console.log(response);
                nota(response.msg, 'agregar');
                $('#modal-4').modal('hide');
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        function actualizar() {
            $.ajax({
                    url: '/actualizar-nota',
                    type: 'POST',
                    data: {
                        id: IDNOTA,
                        titulo: $('#Atitulo').val().toUpperCase(),
                        descripcion: $('#Adescripcion').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                nota(response.msg, 'remplazar');
                $('#modal-actualizar').modal('hide');
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        function actualizarModal(id) {

            IDNOTA = id;
            $.ajax({
                    url: '/buscar-nota',
                    type: 'POST',
                    data: {
                        id: IDNOTA,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                $('#Atitulo').val(response.msg.titulo);
                $('#Adescripcion').val(response.msg.descripcion);
                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Aerror").html(value[0]);
                });
            });
        }

        function openModal() {
            $('#titulo').val("");
            $('#descripcion').val("");
            $('#modal-4').modal();
        }

        /---------------------------------/


        function nota(data, op) {
            var html = '  <div class="col-xl-2 col-lg-4 col-md-4 col-sm-6" id="ser_' + data.id + '">' +
                '       <div class="panel panel-info">' +
                '           <div class="panel-heading bg-info">' + data.titulo +
                '<a href="" data-toggle="modal" data-target="#only-icon-Modal" class="f-right" style="color: white;" onclick="actualizarModal(' + data.id + ')">\n' +
                '                                        <i class="icofont icofont-edit"></i>\n' +
                '                                    </a>' +
                '           </div>' +
                '           <div class="panel-body">' +
                '               <p>' + data.descripcion + '</p>' +
                '           </div>' +
                '           <div class="panel-footer">' +
                '               Fecha:' + data.fecha +
                '           </div>' +
                '       </div>';
            if (op == "agregar") {
                $('#notas').append(html);
            } else if (op == "remplazar") {
                $('#ser_' + data.id).replaceWith(html);
            }

        }

        function estadoCredito() {
            $.post(
                "/get-estado-credito-cliente", {
                    cliente: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                var valor = data.recarga-data.pago;
                if (valor >= 0) {
                    $("#credito").html("$"+valor.toLocaleString());
                    $("#nota").html("Actualmente tiene: $" + valor.toLocaleString());
                } else {
                    $("#credito").html("Actualmente Debe: $0");
                    $("#nota").html("Actualmente Debe: $0");
                }
            });
        }

    </script>


@endsection
