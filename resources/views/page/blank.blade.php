@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Tooltip</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">UI Elements</a>
                            </li>
                            <li class="breadcrumb-item"><a href="tooltips.html">Tooltip</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"><h5 class="card-header-text">Tooltips On Button</h5></div>
                        <div class="card-block tooltip-btn button-list">
                            <button type="button" class="btn btn-default waves-effect" data-toggle="tooltip"
                                    data-placement="top" title="tooltip on top">Top
                            </button>
                            <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="tooltip"
                                    data-placement="left" title="tooltip on left">Left
                            </button>
                            <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="tooltip"
                                    data-placement="right" title="tooltip on right">right
                            </button>
                            <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="tooltip"
                                    data-placement="bottom" title="tooltip on bottom">bottom
                            </button>
                            <button type="button" class="btn btn-info waves-effect waves-light" data-toggle="tooltip"
                                    data-html="true" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">Html Tooltip
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

@endsection

