@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">

        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Dashboard</h4>
                </div>
            </div>
            <!-- 4-blocks row start -->
{{--
            <div class="row m-b-30 dashboard-header">

                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Products</span>
                        <h2 class="dashboard-total-products counter">4500</h2>
                        <span class="label label-warning">Orders</span>New Orders
                        <div class="side-box bg-warning">
                            <i class="icon-social-soundcloud"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Products</span>
                        <h2 class="dashboard-total-products counter">37,500</h2>
                        <span class="label label-primary">Sales</span>Last Week Sales
                        <div class="side-box bg-primary">
                            <i class="icon-social-soundcloud"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Products</span>
                        <h2 class="dashboard-total-products">$<span class="counter">30,780</span></h2>
                        <span class="label label-success">Sales</span>Total Sales
                        <div class="side-box bg-success">
                            <i class="icon-bubbles"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-sm-12 card dashboard-product">
                        <span>Products</span>
                        <h2 class="dashboard-total-products">$<span class="counter">30,780</span></h2>
                        <span class="label label-danger">Views</span>Views Today
                        <div class="side-box bg-danger">
                            <i class="icon-bubbles"></i>
                        </div>
                    </div>
                </div>

            </div>
--}}
            <!-- 4-blocks row end -->

            <!-- 2-1 block start -->
            <div class="row">
{{--
                <div class="col-xl-8 col-lg-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table m-b-0 photo-table">
                                    <thead>
                                    <tr class="text-uppercase">
                                        <th>Photo</th>
                                        <th>Project</th>
                                        <th>Completed</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/avatar-2.png"
                                                 alt="User">
                                        </th>
                                        <td>Appestia Project
                                            <p><i class="icofont icofont-clock-time"></i>Created 14.9.2016</p>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">226,134</span>
                                            <svg class="peity" height="30" width="30">
                                                <path d="M 15.000000000000002 0 A 15 15 0 1 1 4.209902994920235 25.41987555688496 L 15 15"
                                                      fill="#2196F3"></path>
                                                <path d="M 4.209902994920235 25.41987555688496 A 15 15 0 0 1 14.999999999999996 0 L 15 15"
                                                      fill="#ccc"></path>
                                            </svg>
                                        </td>
                                        <td>50%</td>
                                        <td>October 21, 2015</td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/avatar-4.png"
                                                 alt="User">
                                        </th>
                                        <td>Contract with belife Company
                                            <p><i class="icofont icofont-clock-time"></i>Created 20.10.2016</p>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">0.52/1.561</span>
                                            <svg class="peity" height="30" width="30">
                                                <path d="M 15.000000000000002 0 A 15 15 0 0 1 28.00043211809656 22.482564048691025 L 15 15"
                                                      fill="#2196F3"></path>
                                                <path d="M 28.00043211809656 22.482564048691025 A 15 15 0 1 1 14.999999999999996 0 L 15 15"
                                                      fill="#ccc"></path>
                                            </svg>
                                        </td>
                                        <td>70%</td>
                                        <td>November 21, 2015</td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/avatar-1.png"
                                                 alt="User">
                                        </th>
                                        <td>Web Consultancy project
                                            <p><i class="icofont icofont-clock-time"></i>Created 20.10.2016</p>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">1,4</span>
                                            <svg class="peity" height="30" width="30">
                                                <path d="M 15.000000000000002 0 A 15 15 0 0 1 29.265847744427305 10.36474508437579 L 15 15"
                                                      fill="#2196F3"></path>
                                                <path d="M 29.265847744427305 10.36474508437579 A 15 15 0 1 1 14.999999999999996 0 L 15 15"
                                                      fill="#ccc"></path>
                                            </svg>
                                        </td>
                                        <td>40%</td>
                                        <td>September 21, 2015</td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/avatar-3.png"
                                                 alt="User">
                                        </th>
                                        <td>Contract with belife Company
                                            <p><i class="icofont icofont-clock-time"></i>Created 20.10.2016</p>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">0.52/1.561</span>
                                            <svg class="peity" height="30" width="30">
                                                <path d="M 15.000000000000002 0 A 15 15 0 0 1 28.00043211809656 22.482564048691025 L 15 15"
                                                      fill="#2196F3"></path>
                                                <path d="M 28.00043211809656 22.482564048691025 A 15 15 0 1 1 14.999999999999996 0 L 15 15"
                                                      fill="#ccc"></path>
                                            </svg>
                                        </td>
                                        <td>70%</td>
                                        <td>November 21, 2015</td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/avatar-1.png"
                                                 alt="User">
                                        </th>
                                        <td>Contract with belife Company
                                            <p><i class="icofont icofont-clock-time"></i>Created 20.10.2016</p>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">0.52/1.561</span>
                                            <svg class="peity" height="30" width="30">
                                                <path d="M 15.000000000000002 0 A 15 15 0 0 1 28.00043211809656 22.482564048691025 L 15 15"
                                                      fill="#2196F3"></path>
                                                <path d="M 28.00043211809656 22.482564048691025 A 15 15 0 1 1 14.999999999999996 0 L 15 15"
                                                      fill="#ccc"></path>
                                            </svg>
                                        </td>
                                        <td>70%</td>
                                        <td>November 21, 2015</td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <img class="img-fluid img-circle" src="/images/avatar-2.png"
                                                 alt="User">
                                        </th>
                                        <td>Contract with belife Company
                                            <p><i class="icofont icofont-clock-time"></i>Created 20.10.2016</p>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">0.52/1.561</span>
                                            <svg class="peity" height="30" width="30">
                                                <path d="M 15.000000000000002 0 A 15 15 0 0 1 28.00043211809656 22.482564048691025 L 15 15"
                                                      fill="#2196F3"></path>
                                                <path d="M 28.00043211809656 22.482564048691025 A 15 15 0 1 1 14.999999999999996 0 L 15 15"
                                                      fill="#ccc"></path>
                                            </svg>
                                        </td>
                                        <td>70%</td>
                                        <td>November 21, 2015</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
--}}
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="user-block-2">
                            <img class="img-fluid" src="/images/widget/user-1.png" alt="user-header">
                            <h5>{{auth()->user()->nombre}}</h5>
                            <h6>{{auth()->user()->rol->nombre}}</h6>
                        </div>
                        <div class="card-block">
                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-clock-time"></i> Ganancia
                                    <label class="label label-primary">{{auth()->user()->ganancia}} %</label>
                                </div>
                            </div>
                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-users"></i> Sueldo
                                    <label class="label label-primary">$ {{auth()->user()->sueldo}}</label>
                                </div>
                            </div>

                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-ui-user"></i> TipoPago
                                    <label class="label label-primary">{{auth()->user()->tipoPago}}</label>
                                </div>

                            </div>
                            <div class="text-center">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="text-center">
                                Bienvenido a Dental Stetic
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 2-1 block end -->
        </div>

    </div>

@endsection