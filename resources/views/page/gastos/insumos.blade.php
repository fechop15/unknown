@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Gastos Insumos</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a> Gastos</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!"> Insumos (Diario)</a>
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Gastos Insumos</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    onclick="abrirModal()">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                            <a style="float: right;padding-right: 30px">Total: $
                                <strong id="total">0</strong>
                            </a>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Descripcion</th>
                                        <th>Valor</th>
                                        <th>Fecha Creacion</th>
                                    </tr>
                                    </thead>
                                    <tbody id="gasto">

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="row m-b-30 dashboard-header">
                        <div class="col-sm-11 card dashboard-product">
                            <span>Produccion</span>
                            <h2 class="dashboard-total-products counter" id="producidoT">$0</h2>
                            <span class="label label-success"></span> Recaudado Virtual
                            <h2 class="dashboard-total-products counter" id="producido">$0</h2>
                            <span class="label label-success">Mes</span> Recaudado Efectivo
                            <div class="side-box bg-success">
                                <i class="icon-social-soundcloud"></i>
                            </div>
                        </div>

                        <div class="col-lg3 col-sm-6-">
                            <div class="col-sm-11 card">
                                <div class="card-header"><h5 class="card-header-text">Filtros De Busqueda</h5></div>
                                <div class="card-block">
                                    <ul class="nav nav-tabs md-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"
                                               id="select_dia">Dia</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"
                                               id="select_mes">Mes</a>
                                            <div class="slide"></div>
                                        </li>
                                        {{--<li class="nav-item">--}}
                                        {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                        {{--<div class="slide"></div>--}}
                                        {{--</li>--}}
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaDia"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages3" role="tabpanel">
                                            <div class="container">
                                                <br>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Desde</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="">Hasta</label>
                                                        <div class="input-group date input-group-date-custom">
                                                            <input type="text" class="form-control">
                                                            <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-block waves-effect"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title=".btn-success .btn-block">Buscar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>
    @include('modals.modal5')

@endsection

@section('js')

    <script>

        var TOTALDISPONIBLE = 0;
        var opciones = [];
        var REGISTROS = [];
        var total = 0;

        function abrirModal() {
            $("#tituloModal").html("Registrar Gasto Insumo");
            $('#modal-5').modal('show');
        }

        $(document).ready(function () {

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                gerRegistrosMes($("#fechaMes").data("date"));
            });

            gerRegistros($("#fechaDia").data("date"));

            function gerRegistros(fecha) {
                $('#produccion').html("");
                $('#total').html("0");
                REGISTROS = [];
                $.post(
                    "/get-gasto-dia", {
                        fecha: fecha,
                        razon: "Gasto Insumo",
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#gasto').html("");
                    total = 0
                    for (var item in data.msg) {
                        gasto(data.msg[item], 'agregar');
                    }
                    getProduccion();

                });
            }

            function gerRegistrosMes(fecha) {
                $('#gasto').html("");
                $('#total').html("0")
                REGISTROS = [];
                $.post(
                    "/get-gasto-mes", {
                        fecha: fecha,
                        razon: "Gasto Insumo",
                        _token: $('meta[name="csrf-token"]').attr('content')
                    }
                ).done(function (data) {
                    console.log(data);
                    $('#gasto').html("");
                    total = 0
                    for (var item in data.msg) {
                        gasto(data.msg[item], 'agregar');
                    }
                    getProduccion();
                    //console.log(opciones)
                });
            }

            $("#select_dia").click(function () {
                gerRegistros($("#fechaDia").data("date"));
            });

            $("#select_mes").click(function () {
                gerRegistrosMes($("#fechaMes").data("date"));
            });
        });

        function disponibilidad(total) {
            var valor = parseInt(total.value == '' ? 0 : total.value);

            $('#totalAPagar').html(valor.toLocaleString());
            if (TOTALDISPONIBLE >= total.value) {
                $("#guardar").show();
                $("#error").hide();
                $("#error").html("");

            } else {
                $("#guardar").hide();
                $("#error").show();
                $("#error").html("Fondos Insufucientes");
            }
        }

        function calcularToral(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())
        }

        function getProduccion() {
            $.get(
                "/produccion-tarjeta"
            ).done(function (data) {
                console.log(data);
                calcularToralTarjeta(data.msg);
            });

            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToral(data.msg);
            });
        }

        function calcularToralTarjeta(data) {
            var total = data.totalTarjeta + data.totalTarjetaEntrada - data.totalTarjetaSalida;
            TOTALDISPONIBLE = total;
            $('#producidoT').html("$" + total.toLocaleString())
        }

        function guardar() {

            $.ajax({
                url: '/crear-gasto',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    valor: $('#valor').val(),
                    razon: "Gasto Insumo",
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                gasto(response.msg, 'agregar');
                $('#descripcion').val("");
                $('#valor').val("");
                $('#modal-5').modal('hide');
                getProduccion();
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });

            });

        }

        function gasto(data, op) {
            total += data.valor;

            var html = ' <tr id="prod_' + data.id + '">' +
                '        <td class="text-nowrap">' + data.descripcion + '</td>' +
                '        <td class="text-nowrap">$' + parseInt(data.valor).toLocaleString() + '</td>' +
                '        <td class="text-nowrap">' + data.fecha + '</td>' +
                '        </tr>';

            if (op == "agregar") {
                $('#gasto').prepend(html);
                REGISTROS.push(data);
                $('#total').html(total.toLocaleString());

            } else if (op == "remplazar") {
                $('#prod_' + data.id).replaceWith(html);
            }

        }

    </script>


@endsection
