@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>{{$empleado->nombre}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{route('empleados')}}">Empleado</a>
                        </li>
                        <li class="breadcrumb-item">Perfil
                        </li>
                    </ol>
                </div>
            </div>
            <!-- Header end -->
            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="card faq-left">
                        <div class="social-profile">
                            <img id="perfil" class="img-fluid" src="/images/perfil/{{$empleado->imagen}}" alt="">
                            {{--<div class="profile-hvr m-t-15">--}}
                            {{--<i class="icofont icofont-ui-edit p-r-10"--}}
                            {{--id="editar"></i>--}}
                            {{--<i class="icofont icofont-ui-delete" id="eliminar"></i>--}}
                            {{--</div>--}}
                        </div>
                        <div class="card-block">
                            <h4 class="f-18 f-normal m-b-10 txt-primary">{{ $empleado->nombre }}</h4>
                            <h5 class="f-14">{{ $empleado->rol->nombre }}</h5>
                            <p class="m-b-15">
                            <form id="subirImagen">
                                <input type="file" id="avatar" name="avatar"
                                       style="display: none" accept="image/*">
                            </form>
                            </p>
                            <ul>
                                {{--<li class="faq-contact-card">--}}
                                {{--<i class="icofont icofont-telephone"></i>--}}
                                {{--+(1234) - 5678910--}}
                                {{--</li>--}}
                                {{--<li class="faq-contact-card">--}}
                                {{--<i class="icofont icofont-world"></i>--}}
                                {{--<a href="http://phoenixcoded.com">www.phoenixcoded.com</a>--}}
                                {{--</li>--}}
                                {{--<li class="faq-contact-card">--}}
                                {{--<i class="icofont icofont-email"></i>--}}
                                {{--<a href="mailto:joe@example.com">demo@phoenixcoded.com</a>--}}
                                {{--</li>--}}
                            </ul>
                            <div class="faq-profile-btn">
                                {{--<button type="button" class="btn btn-primary waves-effect waves-light">Follows--}}
                                {{--</button>--}}
                                {{--<button type="button" class="btn btn-success waves-effect waves-light">Message--}}
                                {{--</button>--}}
                            </div>

                        </div>
                    </div>

                </div>
                <!-- end of col-lg-3 -->

                <!-- start col-lg-9 -->
                <div class="col-xl-9 col-lg-8">
                    <!-- Nav tabs -->
                    <div class="tab-header">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">
                                    Informacion Personal
                                </a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#project" role="tab">Historial</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#questions" role="tab">Pagos</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#members" role="tab">Cuenta</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                    </div>
                    <!-- end of tab-header -->

                    <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                            <div class="card">
                                <div class="card-header"><h5 class="card-header-text">Sobre Mi</h5>
                                    {{--<button id="edit-btn" type="button"--}}
                                    {{--class="btn btn-primary waves-effect waves-light f-right">--}}
                                    {{--<i class="icofont icofont-edit"></i>--}}
                                    {{--</button>--}}
                                </div>
                                <div class="card-block">
                                    <div class="view-info">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="general-info">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table m-0">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Nombre Completo</th>
                                                                    <td>{{ $empleado->nombre}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Identificacion</th>
                                                                    <td>{{ $empleado->cedula}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Telefono</th>
                                                                    <td>{{ $empleado->telefono}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Correo</th>
                                                                    <td>{{ $empleado->email}}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->

                                                        <div class="col-lg-12 col-xl-6">
                                                            <table class="table">
                                                                <tbody>
                                                                <tr>
                                                                    <th scope="row">Ganancia</th>
                                                                    <td>{{$empleado->ganancia}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sueldo</th>
                                                                    <td> {{$empleado->sueldo}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <th scope="row">TipoPago</th>
                                                                    <td>{{$empleado->tipoPago}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Estado</th>
                                                                    <td>{{$empleado->estado?'Activo':'Inactivo'}}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end of table col-lg-6 -->
                                                    </div>
                                                    <!-- end of row -->
                                                </div>
                                                <!-- end of general info -->
                                            </div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>
                                    <!-- end of view-info -->

                                {{--<div class="edit-info">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-lg-12">--}}
                                {{--<div class="general-info">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-lg-6">--}}
                                {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-ui-user"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="text"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Full Name</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}

                                {{--<div class="form-radio">--}}
                                {{--<form>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-group-students"></i>--}}
                                {{--</span>--}}
                                {{--<div class="radio radiofill radio-inline">--}}
                                {{--<label>--}}
                                {{--<input type="radio"--}}
                                {{--name="radio"><i--}}
                                {{--class="helper"></i>--}}
                                {{--Male--}}
                                {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="radio radiofill radio-inline">--}}
                                {{--<label>--}}
                                {{--<input type="radio"--}}
                                {{--name="radio"><i--}}
                                {{--class="helper"></i>--}}
                                {{--Female--}}
                                {{--</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</form>--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-ui-calendar"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="text" id="date"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Birthday Date</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-users-alt-4"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<select class="md-form-control">--}}
                                {{--<option>Select Marital Status--}}
                                {{--</option>--}}
                                {{--<option>Married</option>--}}
                                {{--<option>Unmarried</option>--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-location-pin"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<textarea class="md-form-control"--}}
                                {{--cols="2" rows="4"></textarea>--}}
                                {{--<label>Address</label>--}}

                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}
                                {{--</div>--}}
                                {{--<!-- end of table col-lg-6 -->--}}

                                {{--<div class="col-lg-6">--}}
                                {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-email"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="email"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Email</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-mobile-phone"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="number"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Mobile Number</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-social-twitter"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="email"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Twitter Id</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}

                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-social-skype"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="email"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Skype Id</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                {{--<td>--}}
                                {{--<div class="md-group-add-on">--}}
                                {{--<span class="md-add-on">--}}
                                {{--<i class="icofont icofont-web"></i>--}}
                                {{--</span>--}}
                                {{--<div class="md-input-wrapper">--}}
                                {{--<input type="text"--}}
                                {{--class="md-form-control">--}}
                                {{--<label>Website</label>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</td>--}}
                                {{--</tr>--}}
                                {{--</tbody>--}}
                                {{--</table>--}}

                                {{--</div>--}}

                                {{--<!-- end of table col-lg-6 -->--}}
                                {{--</div>--}}
                                {{--<!-- end of row -->--}}
                                {{--<div class="text-center">--}}
                                {{--<a href="#!"--}}
                                {{--class="btn btn-primary waves-effect waves-light m-r-20">Save</a>--}}
                                {{--<a href="#!" id="edit-cancel"--}}
                                {{--class="btn btn-default waves-effect">Cancel</a>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- end of edit info -->--}}
                                {{--</div>--}}
                                {{--<!-- end of col-lg-12 -->--}}
                                {{--</div>--}}
                                {{--<!-- end of row -->--}}

                                {{--</div>--}}
                                <!-- end of view-info -->
                                </div>
                                <!-- end of card-block -->
                            </div>
                            <!-- end of card-->

                            <!-- end of row -->
                        </div>
                        <!-- end of tab-pane -->
                        <!-- end of about us tab-pane -->

                        <!-- start tab-pane of project tab -->
                        <div class="tab-pane" id="project" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Produccion</h5>
                                            <a style="float: right;padding-right: 30px">Total Produccion: $
                                                <strong id="total">0</strong>
                                            </a>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablaHistorial"
                                                       style="width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Nombre Paciente</th>
                                                        <th>Servicio</th>
                                                        <th>Precio</th>
                                                        <th>Ganancia</th>
                                                        <th>Fecha Registro</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_dia">Dia</a>
                                                    <div class="slide"></div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#profile3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                                {{--<li class="nav-item">--}}
                                                {{--<a class="nav-link" data-toggle="tab" href="#messages3" role="tab">Rango</a>--}}
                                                {{--<div class="slide"></div>--}}
                                                {{--</li>--}}
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaDia"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMes"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                    <div class="container">
                                                        <br>
                                                        <div class='col-md-12'>
                                                            <div class="form-group">
                                                                <label for="">Desde</label>
                                                                <div class="input-group date input-group-date-custom">
                                                                    <input type="text" class="form-control">
                                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-12'>
                                                            <div class="form-group">
                                                                <label for="">Hasta</label>
                                                                <div class="input-group date input-group-date-custom">
                                                                    <input type="text" class="form-control">
                                                                    <span class="input-group-addon bg-primary">
                                                        <i class="icofont icofont-calendar"></i>
                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button"
                                                            class="btn btn-primary btn-block waves-effect"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title=".btn-success .btn-block">Buscar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end of card-main -->
                        </div>
                        <!-- end of project pane -->

                        <!-- start a question pane  -->

                        <div class="tab-pane" id="questions" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-header-text">Movimientos de Pagos</h5>
                                            <a style="float: right;padding-right: 30px">
                                                <strong id="nota"></strong>
                                            </a>
                                        </div>
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tablapagos" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Descripcion</th>
                                                        <th>Valor</th>
                                                        <th>Tipo</th>
                                                        <th>Mes Pagado</th>
                                                        <th>Fecha</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="card">
                                        <div class="card-header"><h5 class="card-header-text">Filtros De
                                                Busqueda</h5></div>
                                        <div class="card-block">
                                            <ul class="nav nav-tabs md-tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#home3"
                                                       role="tab"
                                                       id="select_mes">Mes</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home3" role="tabpanel">
                                                    <div style="overflow:hidden;">
                                                        <div class="form-group">
                                                            <div id="fechaMesPagos"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="profile3" role="tabpanel">
                                                </div>
                                                <div class="tab-pane" id="messages3" role="tabpanel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of tab pane question -->

                        <!-- start memeber ship tab pane -->

                        <div class="tab-pane" id="members" role="tabpanel">
                            <!-- end of row -->
                            <section class="panels-wells">

                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-header-text">Cuenta</h5>
                                    </div>
                                    <!-- end of card-header  -->
                                    <div class="card-block">

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <!-- end of project table -->
                                            </div>
                                            <div class="col-sm-4">
                                                <!-- end of project table -->
                                            </div>
                                            <div class="col-sm-4"></div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                        <!-- end of row -->
                                    </div>

                                </div>

                            </section>
                        </div>
                        <!-- end of memebership tab pane -->

                    </div>
                    <!-- end of main tab content -->
                </div>
            </div>

        </div>
        <!-- Container-fluid ends -->
    </div>


@endsection
@section('js')

    <script>
        var IDCLIENTE = '{!! $empleado->id !!}';
        var FECHA = null;
        var FECHAH = null;
        var TABLA = null;
        var TABLA2 = null;
        var COMISION = '{!! $empleado->ganancia !!}';
        estadoCredito();

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-notas", {_token: $('meta[name="csrf-token"]').attr('content'), cliente: IDCLIENTE}
            ).done(function (data) {
                //console.log(data);
                for (var item in data.msg) {
                    nota(data.msg[item], 'agregar');
                }

            });

            $('#fechaMes').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaMesPagos').datetimepicker({
                format: 'YYYY-MM',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').datetimepicker({
                format: 'YYYY-MM-DD',
                inline: true,
                sideBySide: true
            });

            $('#fechaDia').on('dp.change', function (e) {
                console.log($("#fechaDia").data("date"));
                FECHAH = $("#fechaDia").data("date");
                TABLA2.ajax.reload();

                //gerRegistros($("#fechaDia").data("date"));
            });

            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                FECHAH = $("#fechaMes").data("date");
                TABLA2.ajax.reload();

                //gerRegistrosMes($("#fechaMes").data("date"));
            });

            $('#fechaMesPagos').on('dp.change', function (e) {
                FECHA = $("#fechaMesPagos").data("date");
                TABLA.ajax.reload();
                console.log($("#fechaMesPagos").data("date"));

            });

            FECHA = $("#fechaMesPagos").data("date");

            FECHAH = $("#fechaDia").data("date");

            TABLA = $('#tablapagos').DataTable({
                "ajax": {
                    "url": "/get-pagos-empleado",
                    "data": function (d) {
                        d.fecha = FECHA;
                        d.empleado = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "POST",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        for (var item in data.msg) {
                            var itemJson = {
                                Descripcion: data.msg[item].descripcion,
                                Valor: '$' + parseInt(data.msg[item].valor).toLocaleString(),
                                Tipo: data.msg[item].tipo,
                                Mes: data.msg[item].mes,
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Descripcion"},
                    {data: "Valor"},
                    {data: "Tipo"},
                    {data: "Mes"},
                    {data: "Fecha"},
                ],
            });

            TABLA2 = $('#tablaHistorial').DataTable({
                "ajax": {
                    "url": "/get-produccion-mes",
                    "data": function (d) {
                        d.fecha = FECHAH;
                        d.id = IDCLIENTE;
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "type": "GET",
                    "dataSrc": function (data) {
                        var json = [];
                        console.log(data);
                        var total = 0;
                        for (var item in data.msg) {
                            var ganancia = data.msg[item].precio * (COMISION / 100);
                            total += ganancia;
                            var itemJson = {
                                nombre: data.msg[item].nombre,
                                servicio: data.msg[item].servicio,
                                precio: '$' + parseInt(data.msg[item].precio).toLocaleString(),
                                ganancia: '$' + ganancia.toLocaleString(),
                                Fecha: data.msg[item].fecha,
                            };
                            json.push(itemJson);
                        }
                        $('#total').html(total.toLocaleString())

                        return json;
                    }
                },
                columns: [
                    {data: "nombre"},
                    {data: "servicio"},
                    {data: "precio"},
                    {data: "ganancia"},
                    {data: "Fecha"},
                ],
            });

            function calcularToral() {
                var total = 0;

                for (var item in REGISTROS) {
                    total += parseInt(REGISTROS[item].precio);
                }

                total = total * (COMISION / 100);
                $('#total').html(total.toLocaleString())

            }

            $("#select_dia").click(function () {
                //gerRegistros($("#fechaDia").data("date"));
                FECHAH = $("#fechaDia").data("date");
                TABLA2.ajax.reload();
                console.log($("#fechaMesPagos").data("date"));
                // calcularToral();

            });

            $("#select_mes").click(function () {
                //gerRegistrosMes($("#fechaMes").data("date"));
                FECHAH = $("#fechaMes").data("date");
                TABLA2.ajax.reload();
                console.log($("#fechaMes").data("date"));
                //calcularToral();

            });
        });

        function estadoCredito() {
            $.post(
                "/get-estado-credito-empleado", {
                    empleado: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                var valor = data.avance - data.abono - data.descuento;
                if (valor > 0) {
                    $("#nota").html("Actualmente Debe: $" + valor.toLocaleString());
                } else {
                    $("#nota").html("Actualmente Debe: $0");
                }
            });
        }
    </script>


@endsection
