@extends('layout.Login')
@section('page')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <form class="md-float-material" action="{{route('login')}}" method="POST">
                            {{ csrf_field() }}

                            <div class="text-center">
                                <img src="/images/logo-dental.png" alt="logo" style="width: 80%">
                            </div>
                            <h3 class="text-center txt-primary">
                                Ingresa a tu cuenta
                            </h3>
                            <div class="md-input-wrapper">
                                <input type="email" class="md-form-control md-static" name="email"
                                       placeholder="Ingresa Tu Email"
                                       value="{{ old('email') }}"
                                />
                            </div>
                            {!! $errors -> first('email','<div class="col-md-12 waves-effect waves-light">
                                    <div class="bg-danger p-10">:message</div>
                                </div><br>') !!}

                            <div class="md-input-wrapper">
                                <input type="password" class="md-form-control md-static" name="password"
                                       placeholder="Ingresa Tu Password "/>
                            </div>
                            {!! $errors -> first('password','<div class="col-md-12 waves-effect waves-light">
                                    <div class="bg-danger p-10">:message</div>
                                </div><br>') !!}
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    {{--<div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                        <label class="input-checkbox checkbox-primary">
                                            <input type="checkbox" id="checkbox">
                                            <span class="checkbox"></span>
                                        </label>
                                        <div class="captions">Remember Me</div>

                                    </div>--}}
                                </div>
                                <div class="col-sm-6 col-xs-12 forgot-phone text-right">
{{--
                                    <a href="forgot-password.html" class="text-right f-w-600"> Forget Password?</a>
--}}
                                </div>
                            </div>

                            {!! $errors -> first('loginError','<div class="col-md-12 waves-effect waves-light">
                                    <div class="bg-danger p-10">:message</div>
                                </div><br>') !!}

                            <div class="row">
                                <div class="col-xs-10 offset-xs-1">
                                    <button type="submit"
                                            class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                        LOGIN
                                    </button>
                                </div>
                            </div>
                            <!-- <div class="card-footer"> -->
                            <!-- </div> -->
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection