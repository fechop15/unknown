@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Header Starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Pacientes</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item">
                                <a href="/">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('clientes')}}">Pacientes</a>
                            </li>
                            <li class=" breadcrumb-item">Pacientes Registrados
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Header end -->

            <!-- Row start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Gestion de Clientes</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    data-toggle="modal"
                                    data-target="#modal-3">
                                <i class="fa fa-plus-circle"></i> &nbsp;Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-hover" id="tablaClientes">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Cedula</th>
                                            <th>Telefono</th>
                                            <th>Ciudad</th>
                                            <th>Ocupacion</th>
                                            <th>Credito</th>
                                            <th>Opciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row end -->
        </div>

    </div>
    @include('modals.modal3')
@endsection
@section('js')

    <script>
        var TABLA = null;
        var IDUSUARIO = 0;
        TABLA = $('#tablaClientes').DataTable({
            "ajax": {
                "url": "/get-clientes",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: '<a href="/dashboard/perfil/' + data.msg[item].id + '" >' + data.msg[item].nombre + '</a>',
                            Cedula: data.msg[item].cedula,
                            Telefono: data.msg[item].telefono,
                            Ciudad: data.msg[item].ciudad,
                            Ocupacion: data.msg[item].ocupacion,
                            Credito: "$" + data.msg[item].credito.toLocaleString(),
                            Opciones: opciones(data.msg[item].id),
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Cedula"},
                {data: "Telefono"},
                {data: "Ciudad"},
                {data: "Ocupacion"},
                {data: "Credito"},
                {data: "Opciones"},
            ],
        });

        function opciones(id) {
            var opciones = '' +
                '<button type="button" class="btn btn-primary waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           onclick="actualizarModal(' + id + ')">' +
                '           <i class="icofont icofont-ui-edit"></i>' +
                '</button>' +
                '<button type="button" class="btn btn-success waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver perfil" data-original-title="Edit"' +
                '           onclick="verPerfil(' + id + ')">' +
                '           <i class="icofont icofont-eye-alt"></i>' +
                '</button>';
            if ('{!! auth()->user()->rol_id !!}' == '1') {
                // opciones += '<button type="button" class="btn btn-info waves-effect waves-light" ' +
                //   '           data-toggle="tooltip" data-placement="top" title="Agregar Saldo" data-original-title="Edit"' +
                // '           onclick="saldoModal(' + id + ')">' +
                // '           <i class="icofont icofont-cur-dollar"></i>' +
                // '</button>';
            }


            return opciones;
        }

        function guardarPaciente() {

            $.ajax({
                url: '/crear-cliente',
                type: 'POST',
                data: {
                    nombre: $('#nombre').val().toUpperCase(),
                    cedula: $('#cedula').val(),
                    edad: $('#edad').val(),
                    telefono: $('#telefono').val(),
                    direccion: $('#direccion').val().toUpperCase(),
                    ocupacion: $('#ocupacion').val().toUpperCase(),
                    ciudad: $('#ciudad').val().toUpperCase(),
                    estado_civil: $('#estado_civil').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                $('#modal-3').modal('hide');

                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });

            });

        }

        function actualizar() {
            console.log(IDUSUARIO);
            $.ajax({
                    url: '/actualizar-cliente',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        nombre: $('#Anombre').val().toUpperCase(),
                        cedula: $('#Acedula').val(),
                        telefono: $('#Atelefono').val(),
                        edad: $('#Aedad').val(),
                        direccion: $('#Adireccion').val().toUpperCase(),
                        ocupacion: $('#Aocupacion').val().toUpperCase(),
                        ciudad: $('#Aciudad').val().toUpperCase(),
                        estado_civil: $('#Aestado_civil').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                TABLA.ajax.reload();
                $('#modal-actualizar').modal('hide');
                //return response;
            }).fail(function (error) {

                console.log(error)

            });
        }

        function actualizarModal(id) {

            IDUSUARIO = id;
            $.ajax({
                    url: '/buscar-cliente',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                }
            ).done(function (response) {
                console.log(response);
                $('#Anombre').val(response.msg.nombre);
                $('#Acedula').val(response.msg.cedula);
                $('#Aedad').val(response.msg.edad);
                $('#Atelefono').val(response.msg.telefono);
                $('#Adireccion').val(response.msg.direccion);
                $('#Aocupacion').val(response.msg.ocupacion);
                $('#Aciudad').val(response.msg.ciudad);
                $('#Aestado_civil').val(response.msg.estado_civil);
                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error)

            });
        }

        function saldoModal(id) {

            IDUSUARIO = id;
            $("#modal-pago").modal();
            $("#pago").val(0);

        }

        function guardar() {

            $.ajax({
                url: '/credito-cliente',
                type: 'POST',
                data: {
                    descripcion: $('#descripcion').val(),
                    valor: $('#valor').val(),
                    cliente: IDUSUARIO,
                    _token: $('meta[name="csrf-token"]').attr('content'),
                },
            }).done(function (response) {

                console.log(response);
                TABLA.ajax.reload();
                getProduccion();
                $('#descripcion').val("");
                $('#valor').val("");
                $('#modal-pago').modal('hide');
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });

            });

        }

        function verPerfil(id) {
            window.location.href = "/dashboard/perfil/" + id;
        }

    </script>


@endsection