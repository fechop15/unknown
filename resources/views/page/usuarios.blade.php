@extends('layout.Dashboard')
@section('page')
    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Header Starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Empleados</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item">
                                <a href="index.html">
                                    <i class="icofont icofont-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('empleados')}}">Empleado</a>
                            </li>
                            <li class=" breadcrumb-item">Empleados Del Sistema
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Header end -->

            <!-- Row start -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Gestion de Empleados</h5>
                            <button type="button" class="btn btn-primary waves-effect md-trigger" style="float: right"
                                    data-toggle="modal"
                                    data-target="#modal-1">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Cedula</th>
                                            <th>Correo</th>
                                            <th>Rol</th>
                                            <th>Telefono</th>
                                            <th>%Ganancia</th>
                                            <th>Sueldo</th>
                                            <th>Pago</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                        </thead>
                                        <tbody id="usuario">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row end -->
        </div>

    </div>
    @include('modals.modal1')
@endsection
@section('js')

    <script>
        var IDUSUARIO = 0;
        $.get(
            "/get-usuarios"
        ).done(function (data) {
            //alert("Data Loaded: " + data);
            console.log(data);
            for (var item in data.msg) {
                usuario(data.msg[item], 'agregar');
            }
        });

        function guardar() {

            $.ajax({
                url: '/crear-usuarios',
                type: 'POST',
                data: {
                    nombre: $('#nombre').val(),
                    email: $('#email').val(),
                    cedula: $('#cedula').val(),
                    telefono: $('#telefono').val(),
                    password: $('#password').val(),
                    ganancia: $('#ganancia').val(),
                    sueldo: $('#sueldo').val(),
                    tipoPago: $('#tipoPago').val(),
                    rol_id: $('#rol').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                console.log(response);
                usuario(response.msg, 'agregar');
                $('#modal-1').modal('hide');
                $('#empleados')[0].reset();
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });

            });

        }

        function actualizar() {
            console.log(IDUSUARIO);
            $.ajax({
                    url: '/actualizar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        nombre: $('#Anombre').val(),
                        email: $('#Aemail').val(),
                        cedula: $('#Acedula').val(),
                        telefono: $('#Atelefono').val(),
                        password: $('#Apassword').val(),
                        rol_id: $('#Arol').val(),
                        estado: $('#Aestado').val(),
                        ganancia: $('#Aganancia').val(),
                        sueldo: $('#Asueldo').val(),
                        tipoPago: $('#AtipoPago').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                usuario(response.msg, 'remplazar');

                $('#modal-actualizar').modal('hide');
                $('#empleados-actualizar')[0].reset();

                //return response;
            }).fail(function (error) {

                console.log(error)

            });
        }

        function actualizarModal(id) {

            IDUSUARIO = id;
            console.log(IDUSUARIO);

            $.ajax({
                    url: '/buscar-usuarios',
                    type: 'POST',
                    data: {
                        id: IDUSUARIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                console.log(response);
                $('#Anombre').val(response.msg.nombre);
                $('#Acedula').val(response.msg.cedula);
                $('#Aemail').val(response.msg.email);
                $('#Atelefono').val(response.msg.telefono);
                $('#Apassword').val(response.msg.password);
                $('#Arol').val(response.msg.rol_id);
                $('#Aganancia').val(response.msg.ganancia);
                $('#Aestado').val(response.msg.estado);
                $('#Asueldo').val(response.msg.sueldo);
                $('#AtipoPago').val(response.msg.tipoPago);

                $('#modal-actualizar').modal();
                return response;

            }).fail(function (error) {

                console.log(error)

            });
        }

        function passModal(id) {
            IDUSUARIO = id;
            $('#modal-pass').modal();
        }

        function cambiarPass() {
            $.post(
                "/cambiar-pass", {
                    pass: $("#pass").val(),
                    id: IDUSUARIO,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $('#modal-pass').modal('hide');
                    notify('Contraseña Actualizada', 'success');

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        function usuario(data, op) {
            var estado = (data.estado == 0 ? '<span class="label label-danger">Inactivo</span>' : '<span class="label label-success">Activo</span>');
            var html = ' <tr id="us_' + data.id + '">\n' +
                '        <td>' + data.id + '</td>\n' +
                '        <td class="text-nowrap"><a href="/dashboard/empleados/' + data.id + '" >' + data.nombre + '</a></td>\n' +
                '        <td>' + data.cedula + '</td>\n' +
                '        <td>' + data.email + '</td>\n' +
                '        <td> ' + data.rol + ' </td>' +
                '        <td>' + data.telefono + '</td>\n' +
                '        <td>' + data.ganancia + '%</td>\n' +
                '        <td>$' + data.sueldo.toLocaleString() + '</td>\n' +
                '        <td>' + data.tipoPago + '</td>\n' +
                '        <td>' + estado + '</td>\n' +
                '        <td class="faq-table-btn">\n' +
                '<button type="button" class="btn btn-primary waves-effect waves-light" ' +
                '           data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"' +
                '           onclick="actualizarModal(' + data.id + ')">\n' +
                '           <i class="icofont icofont-ui-edit"></i>\n' +
                '        </button>' +
                '<button type="button" class="btn btn-success waves-effect waves-light actualizar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Cambiar Contraseña" data-original-title="Edit"' +
                '           onclick="passModal(' + data.id + ')">' +
                '           <i class="icofont icofont-key"></i>' +
                ' </button>' +
                '        </td>' +
                '        </tr>';

            if (op == "agregar") {
                $('#usuario').append(html);
            } else if (op == "remplazar") {
                $('#us_' + data.id).replaceWith(html);
            }

        }

        function notify(message, type) {
            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'button',
                    align: 'right'
                },
                delay: 4000,
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight'
                },
                offset: {
                    x: 30,
                    y: 30
                }
            });
        }

    </script>


@endsection