@extends('layout.Dashboard')
@section('page')

    <div class="content-wrapper">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <!-- Main content starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header">
                        <h4>Servicios</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="/"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">Servicios</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Tooltip start -->
            <div class="row">
                <div class="col-sm-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Produccion Del Dia
                            </h5>
                            <button type="button" class="btn btn-primary" style="float: right" data-toggle="modal"
                                    data-target="#modal-2-produccion">
                                <i class="fa fa-plus-circle"></i> &nbsp; Agregar
                            </button>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Servicio</th>
                                        <th>Atendido Por</th>
                                        <th>Precio</th>
                                        <th>Metodo De Pago</th>
                                        <th>Fecha Creacion</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="produccion">

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-11 card dashboard-product">
                        <span>Produccion</span>
                        <h2 class="dashboard-total-products counter" id="producidoT">$0</h2>
                        <span class="label label-success"></span> Recaudado Virtual
                        <h2 class="dashboard-total-products counter" id="producido">$0</h2>
                        <span class="label label-success">Mes</span> Recaudado Efectivo
                        <div class="side-box bg-success">
                            <i class="icon-social-soundcloud"></i>
                        </div>
                    </div>
                </div>

                {{--<div class="col-lg-4">--}}
                {{--<div class="card">--}}
                {{--<div class="card-header">--}}
                {{--<h5 class="card-header-text">--}}
                {{--Servicios Ofrecidos--}}
                {{--</h5>--}}
                {{--<button type="button" class="btn btn-primary" style="float: right" data-toggle="modal"--}}
                {{--data-target="#modal-2">--}}
                {{--<i class="fa fa-plus-circle"></i> &nbsp; Agregar--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--<div class="card-block">--}}
                {{--<div class="table-responsive">--}}
                {{--<table class="table table-hover">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                {{--<th>Nombre</th>--}}
                {{--<th>Precio</th>--}}
                {{--<th>Opciones</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody id="servicios">--}}

                {{--</tbody>--}}

                {{--</table>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

            </div>
            <!-- Container-fluid ends -->
        </div>
    </div>

    @include('modals.modal2')

@endsection

@section('js')

    <script>
        var IDSERVICIO = 0;
        var SALDO = 0;
        var opciones = [];
        var USUARIOS = [];
        var CLIENTES = [];
        var SERVICIOS = [];
        $(document).ready(function () {
            // Instrucciones a ejecutar al terminar la carga
            //$('#select-servicios').select2();
            $.post(
                "/get-usuarios-by-rol", {_token: $('meta[name="csrf-token"]').attr('content'), rol: 'Empleado'}
            ).done(function (data) {
                //console.log(data);

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    USUARIOS.push(itemSelect2)
                }
                $('#select-usuarios').select2({
                    dropdownParent: $("#modal-2-produccion"),
                    minimumResultsForSearch: 5,
                    allowClear: true,
                    data: USUARIOS
                });

            });
            $.get(
                "/get-clientes"
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre + " - " + data.msg[item].cedula
                    };
                    CLIENTES.push(itemSelect2)
                }
                $('#select-cliente').select2({
                    dropdownParent: $("#modal-2-produccion"),
                    minimumResultsForSearch: 5,
                    placeholder: "Bucar Cliente",
                    allowClear: true,
                    data: CLIENTES
                });

            });
            // $.get(
            //     "/get-servicios"
            // ).done(function (data) {
            //     //console.log(data);
            //     SERVICIOS = data.msg;
            //     for (var item in data.msg) {
            //         servicio(data.msg[item], 'agregar');
            //         var itemSelect2 = {
            //             id: data.msg[item].id,
            //             text: data.msg[item].nombre
            //         };
            //         opciones.push(itemSelect2);
            //     }
            //     $('#select-servicios').select2({
            //         data: opciones
            //     });
            //
            // });

            $.get(
                "/get-produccion"
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    produccion(data.msg[item], 'agregar');
                }
                console.log(opciones)

            });

            $('#select-servicios').on('select2:select', function (e) {
                calcularToral($('#select-servicios').select2("val"));
            });

            $('#select-cliente').on('change', function (e) {
                consultarSaldoCuenta($('#select-cliente').val());
            });

        });

        function calcularToral(total) {
            var valor = parseInt(total.value);
            $('#totalRegistroProduccion').html(valor.toLocaleString());
        }

        function consultarSaldoCuenta(IDCLIENTE) {
            $.post(
                "/get-estado-credito-cliente", {
                    cliente: IDCLIENTE,
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                var valor = data.recarga - data.pago;
                SALDO = valor;
                if (valor >= 0) {
                    $("#notaSaldo").html("Saldo Actual: $" + valor.toLocaleString());
                } else {
                    $("#notaSaldo").html("Actualmente Debe: $0");
                }
            });
        }

        /------------------------/

        function guardarServicio() {
            $.ajax({
                url: '/crear-servicio',
                type: 'POST',
                data: {
                    nombre: $('#nombre').val(),
                    precio: $('#precio').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                //console.log(response);
                servicio(response.msg, 'agregar');
                $('#modal-2').modal('hide');
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        function actualizarServicio() {
            console.log(IDSERVICIO);
            $.ajax({
                    url: '/actualizar-servicio',
                    type: 'POST',
                    data: {
                        id: IDSERVICIO,
                        nombre: $('#Anombre').val(),
                        precio: $('#Aprecio').val(),
                        estado: $('#Aestado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                servicio(response.msg, 'remplazar');
                $('#modal-2-actualizar').modal('hide');
                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        function actualizarModal(id) {

            IDSERVICIO = id;
            $.ajax({
                    url: '/buscar-servicio',
                    type: 'POST',
                    data: {
                        id: IDSERVICIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                $('#Anombre').val(response.msg.nombre);
                $('#Aprecio').val(response.msg.precio);
                $('#Aestado').val(response.msg.estado);

                $('#modal-2-actualizar').modal();
                return response;

            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        /---------------------------------/

        function guardarProduccion() {
            $("#produccion-error").html("");

            $.ajax({
                url: '/crear-produccion',
                type: 'POST',
                data: {
                    cliente_id: $('#select-cliente').val(),
                    servicio: $('#Pservicio').val(),
                    empleado_id: $('#select-usuarios').val(),
                    precio: $('#Pprecio').val(),
                    metodo_pago: $('#PmetodoPago').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                console.log(response);
                if (response.status == 'error') {
                    $("#produccion-error").html(response.msg);
                    return;
                }
                produccion(response.msg, 'agregar');
                $('#modal-2-produccion').modal('hide');
                $("#notaSaldo").html("");
                $("#produccion-error").html("");
                $('#form-produccion')[0].reset();
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#produccion-error").html(value[0]);
                });
            });

        }

        function actualizarProduccion() {
            console.log(IDSERVICIO);
            $.ajax({
                    url: '/actualizar-servicio',
                    type: 'POST',
                    data: {
                        id: IDSERVICIO,
                        nombre: $('#Anombre').val(),
                        precio: $('#Aprecio').val(),
                        estado: $('#Aestado').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                servicio(response.msg, 'remplazar');
                $('#modal-2-actualizar-produccion').modal('hide');
                $("#error").html("");

                //return response;
            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        function actualizarModal2(id) {

            IDSERVICIO = id;
            $.ajax({
                    url: '/buscar-servicio',
                    type: 'POST',
                    data: {
                        id: IDSERVICIO,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },

                }
            ).done(function (response) {
                //console.log(response);
                $('#Anombre').val(response.msg.nombre);
                $('#Aprecio').val(response.msg.precio);
                $('#Aestado').val(response.msg.estado);

                $('#modal-2-actualizar-produccion').modal();
                return response;

            }).fail(function (error) {
                console.log(error)
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#error").html(value[0]);
                });
            });
        }

        /---------------------------------/

        function servicio(data, op) {

            var html = ' <tr id="ser_' + data.id + '">\n' +
                '        <td class="text-nowrap">' + data.nombre + '</td>\n' +
                '        <td>$' + data.precio.toLocaleString() + '</td>\n' +
                // '        <td>' + (data.estado == 0 ? "Inactivo" : "Activo") + '</td>\n' +
                '        <td>\n' +
                '        <button type="button" class="btn btn-secondary btn-sm" ' +
                '                onclick="actualizarModal(' + data.id + ')"><i\n' + ' ' +
                '                class="fa fa-edit"></i>\n' +
                '        </button>\n' +
                '        </td>\n' +
                '        </tr>';

            if (op == "agregar") {
                $('#servicios').append(html);
            } else if (op == "remplazar") {
                $('#ser_' + data.id).replaceWith(html);
            }

        }

        function produccion(data, op) {
            var pago = (data.metodoP == 'Efectivo' ? '<span class="label label-success">Efectivo</span>' : '<span class="label label-info">Tarjeta</span>');
            var precio = parseInt(data.precio);
            var html = ' <tr id="prod_' + data.id + '">' +
                '        <td class="text-nowrap">' + data.nombre + '</td>' +
                '        <td class="text-nowrap">' + data.servicio + '</td>' +
                '        <td class="text-nowrap">' + data.atendido + '</td>' +
                '        <td>$' + precio.toLocaleString() + '</td>' +
                '        <td class="text-nowrap">' + pago + '</td>' +
                '        <td class="text-nowrap">' + data.fecha + '</td>' +
                // '        <td>' +
                // '        <button type="button" class="btn btn-secondary btn-sm" ' +
                // '               ' +
                // '                onclick="actualizarModal(' + data.id + ')"><i\n' + ' ' +
                // '                class="fa fa-edit"></i>' +
                // '        </button>' +
                // '        </td>' +
                '        </tr>';

            if (op == "agregar") {
                $('#produccion').prepend(html);
            } else if (op == "remplazar") {
                $('#prod_' + data.id).replaceWith(html);
            }

        }

        /////
        getProduccion();

        function calcularToralEfectivo(data) {
            var total = data.totalEfectivo - data.totalTGastos;
            TOTALDISPONIBLE = total;
            $('#producido').html("$" + total.toLocaleString())
        }

        function getProduccion() {
            $.get(
                "/produccion-tarjeta"
            ).done(function (data) {
                console.log(data);
                calcularToralTarjeta(data.msg);
            });

            $.get(
                "/produccion-mes"
            ).done(function (data) {
                console.log(data);
                calcularToralEfectivo(data.msg);
            });
        }

        function calcularToralTarjeta(data) {
            var total = data.totalTarjeta + data.totalTarjetaEntrada - data.totalTarjetaSalida;
            TOTALDISPONIBLE = total;
            $('#producidoT').html("$" + total.toLocaleString())
        }
    </script>


@endsection