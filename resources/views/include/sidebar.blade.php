<aside class="main-sidebar hidden-print ">
    <section class="sidebar" id="sidebar-scroll">

        <div class="user-panel">
            <div class="f-left image"><img src="/images/perfil/{{auth()->user()->imagen}}" alt="User Image"
                                           class="img-circle">
            </div>
            <div class="f-left info">
                <p> {{ auth()->user()->nombre }} </p>
                <p class="designation">{{ auth()->user()->rol->nombre }} <i
                            class="icofont icofont-caret-down m-l-5"></i></p>
            </div>
        </div>
        <!-- sidebar profile Menu-->
        <ul class="nav sidebar-menu extra-profile-list">
            <li>
                <a class="waves-effect waves-dark" href="{{route('perfil')}}">
                    <i class="icon-user"></i>
                    <span class="menu-text">Ver Perfil</span>
                    <span class="selected"></span>
                </a>
            </li>
            {{--<li>
                <a class="waves-effect waves-dark" href="javascript:void(0)">
                    <i class="icon-settings"></i>
                    <span class="menu-text">Settings</span>
                    <span class="selected"></span>
                </a>
            </li>--}}
            <li>
                <a class="waves-effect waves-dark" href="{{route('logout')}}">
                    <i class="icon-logout"></i>
                    <span class="menu-text">Logout</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- Sidebar Menu-->
        <ul class="sidebar-menu">

            <li class="nav-level">Navegacion</li>
            <li class="{{ request()->route()->getName() === 'dashboard' ? ' active' : '' }} treeview">
                <a class="waves-effect waves-dark" href="{{route('dashboard')}}">
                    <i class="icon-speedometer"></i><span> Dashboard</span>
                </a>
            </li>
            <li class="{{ request()->route()->getName() === 'producido' ? ' active' : '' }} treeview">
                <a class="waves-effect waves-dark" href="{{route('producido')}}">
                    <i class="icon-calendar"></i><span> Producido </span>
                </a>
            </li>
            <li class="{{ request()->route()->getName() === 'clientes'|| request()->route()->getName() === 'perfilCliente' ? ' active' : '' }} treeview">
                <a class="waves-effect waves-dark" href="{{route('clientes')}}">
                    <i class="icon-people"></i><span> Pacientes</span>
                </a>
            </li>

            @if(auth()->user()->rol_id == 1)
                <li class="nav-level">Administrador</li>
                <li class="{{ request()->route()->getName() === 'empleados'? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('empleados')}}">
                        <i class="icon-people"></i><span> Empleados</span>
                    </a>
                </li>

                <li class="{{ request()->route()->getName() === 'historico' ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="{{route('historico')}}">
                        <i class="icon-home"></i><span> Historico </span>
                    </a>
                </li>
                <li class=" {{
                request()->route()->getName() ==='fijos'||
                request()->route()->getName() ==='insumos'||
                request()->route()->getName() ==='otros'||
                request()->route()->getName() ==='diarios' ||
                request()->route()->getName() ==='nomina' ||
                request()->route()->getName() ==='credito' ||
                request()->route()->getName() === 'consignaciones' ? ' active' : '' }} treeview">
                    <a class="waves-effect waves-dark" href="#!"><i class="icofont icofont-company"></i>
                        <span>Financiero</span>
                        <i class="icon-arrow-down"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class=" {{
                request()->route()->getName() ==='fijos'||
                request()->route()->getName() ==='insumos'||
                request()->route()->getName() ==='otros'  ? ' active' : '' }}   treeview">
                            <a class="waves-effect waves-dark" href="#!">
                                <i class="icon-arrow-right"></i>
                                <span>Gastos</span>
                                <i class="icon-arrow-down"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ request()->route()->getName() === 'fijos' ? ' active' : '' }}">
                                    <a class="waves-effect waves-dark" href="{{route('fijos')}}">
                                        <i class="icon-arrow-right"></i>
                                        Fijos
                                    </a>
                                </li>
                                <li class="{{ request()->route()->getName() === 'insumos' ? ' active' : '' }}">
                                    <a class="waves-effect waves-dark" href="{{route('insumos')}}">
                                        <i class="icon-arrow-right"></i>
                                        Insumo
                                    </a>
                                </li>
                                <li class="{{ request()->route()->getName() === 'otros' ? ' active' : '' }}">
                                    <a class="waves-effect waves-dark" href="{{route('otros')}}">
                                        <i class="icon-arrow-right"></i>
                                        Otros
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class=" {{ request()->route()->getName() ==='diarios' || request()->route()->getName() ==='nomina'|| request()->route()->getName() ==='credito' ? ' active' : '' }}  treeview">
                            <a class="waves-effect waves-dark" href="#!">
                                <i class="icon-arrow-right"></i>
                                <span>Pagos</span>
                                <i class="icon-arrow-down"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ request()->route()->getName() === 'diarios' ? ' active' : '' }}">
                                    <a class="waves-effect waves-dark" href="{{route('diarios')}}">
                                        <i class="icon-arrow-right"></i>
                                        Doctores
                                    </a>
                                </li>
                                <li class="{{ request()->route()->getName() === 'nomina' ? ' active' : '' }}">
                                    <a class="waves-effect waves-dark" href="{{route('nomina')}}">
                                        <i class="icon-arrow-right"></i>
                                        Nomina
                                    </a>
                                </li>
                                <li class="{{ request()->route()->getName() === 'credito' ? ' active' : '' }}">
                                    <a class="waves-effect waves-dark" href="{{route('credito')}}">
                                        <i class="icon-arrow-right"></i>
                                        Credito
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="{{ request()->route()->getName() === 'consignaciones' ? ' active' : '' }}">
                            <a class="waves-effect waves-dark" href="{{route('consignaciones')}}">
                                <i class="icon-arrow-right"></i>
                                Consignaciones
                            </a>
                        </li>
                    </ul>
                </li>

            @endif
        </ul>
    </section>
</aside>